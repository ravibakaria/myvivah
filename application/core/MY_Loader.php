<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Loader.php";

class MY_Loader extends MX_Loader {

    private $CI;
    private $roleId;
    private $roleName;
    
    function __construct() {
        parent::__construct();
        $this->CI = &get_instance();
    }
    public function template($template_name, $vars = array(), $return = FALSE)
    {   

        if($return):
            $content  = $this->view('Home/adminheader', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('Home/adminfooter', $vars, $return);

            return $content;
        else:
            $this->view('Home/adminheader', $vars);
            $this->view($template_name, $vars);
            $this->view('Home/adminfooter', $vars);
        endif;
        
    }
    public function userTemplate($template_name, $vars = array(), $return = FALSE)
    {   

        if($return):
            $content  = $this->view('Home/header', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('Home/footer', $vars, $return);

            return $content;
        else:
            $this->view('Home/header', $vars);
            $this->view($template_name, $vars);
            $this->view('Home/footer', $vars);
        endif;
        
    }
    public function Business_partner($template_name, $vars = array(), $return = FALSE)
    {   

        if($return):
            $content  = $this->view('Business_partner/header', $vars, $return);
            $content .= $this->view($template_name, $vars, $return);
            $content .= $this->view('Business_partner/footer', $vars, $return);

            return $content;
        else:
            $this->view('Business_partner/header', $vars);
            $this->view($template_name, $vars);
            $this->view('Business_partner/footer', $vars);
        endif;
        
    }
}