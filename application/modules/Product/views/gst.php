<!doctype html> 
<html> 
<head> 
    <title>DataTables and Codeigniter</title> 
    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> -->


<!--Data Table-->


<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
    <style> 
        body { 
            padding: 15px; 
        } 
    </style> 
</head> 
<body> 
    <div class="row" style="margin-bottom: 10px"> 
        <div class="col-md-4"> 
            <h2 style="margin-top:0px">Activation Charge And Margin List</h2> 
        </div> 
        <div class="col-md-4 text-center"> 
            <div style="margin-top: 4px" id="message"> 
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?> 
            </div> 
        </div> 
        <!-- <div class="col-md-4 text-right"> 
            <?php //echo anchor(base_url('Product/create_gst'), 'Create', 'class="btn btn-primary"'); ?> 
        </div>  -->
    </div> 
    <div class="row" style="margin-bottom: 10px"> 
    <div class="col-md-8">
    <table class="table table-bordered table-striped"> 
        <thead> 
            <tr> 
                <th width="80px">No</th> 
                <th>Margin</th> 
                 
            </tr> 
        </thead> 
        <tbody> 
            <?php 
            $i = 0; 
            foreach ($margins as $Margin) 
            { 
                ?> 
                <tr> 
                    <td> 
                        <?php echo ++$i ?> 
                    </td> 
                    <td> 
                       <?php echo $Margin->margin ?> % 
                    </td>  
                </tr> 
                <?php 
            } 
            ?> 
        </tbody> 
    </table>

    </div>
    </div>
    <div class="row" style="margin-bottom: 10px"> 
    <div class="col-md-12">
    <table class="table table-bordered table-striped" id="mytable"> 
        <thead> 
            <tr> 
                <th width="80px">No</th> 
                <th>Activation Charge</th> 
                <th>Discription</th> 
               
                <th>Action</th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php 
            $start = 0; 
            foreach ($gst_data as $service) 
            { 
                ?> 
                <tr> 
                    <td> 
                        <?php echo ++$start ?> 
                    </td> 
                    <td> 
                       <?php echo $service->gst ?> 
                    </td>  
                    <td> 
                       <?php echo $service->discription ?> 
                    </td>  
                    
                    <td style="text-align:center" width="200px"> 
                        <?php  
            echo anchor(base_url('Product/update_gst/'.$service->id),'Update');  
            echo ' | ';  
            echo anchor(base_url('Product/delete_gst/'.$service->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"');  
            ?> 
                    </td> 
                </tr> 
                <?php 
            } 
            ?> 
        </tbody> 
    </table>
        </div> 
        </div> 
    <script type="text/javascript"> 
        $(document).ready(function() { 
            $("#mytable").dataTable(); 
        }); 
    </script> 
</body> 
</html>