
<?php // echo $product->service ;print_r($product);die;?>
<div class="row">
	<div class ="col-md-8 card mx-auto">
		<div class=" card-body">
			<form method="post" action="<?php echo base_url('Product/update_product/'.$product->id);?>" enctype="multipart/form-data">
				<h3>Update Product </h3>
					<div class="form-group">
						<label for="email">Name:</label>
						<input type="text" class="form-control" id="name" value="<?= $product->name;?>" name="name">
						<?php echo form_error('name'); ?>
					</div>
					<div class="form-row">
						<div class="col form-group">
							<label>Actual Price:</label>
							<input type="text" class="form-control" id="pries" value="<?= $product->pries;?>" name="pries">
					<?php echo form_error('pries'); ?>
						</div> 
						<div class="col form-group">
							<label>Your Share [Price - (4% + GST)]</label>
							<input type="text" class="form-control" id="share"  name="share"  value="<?= $product->share;?>" readonly>
						</div> 
            		</div> 
					
					<div class="form-row">
						<div class="col form-group">
							<label>Show Price:</label>
							<input type="text" class="form-control" id="showpries" placeholder="Enter Show Price" name="showpries" value="<?php echo $product->showpries ?>">
						</div> 
						<div class="col form-group">
							<label>Quantity:</label>
							<input type="text" class="form-control" id="pries" placeholder="Enter pries" name="quantity" value="<?= $product->quantity;?>">
								<?php echo form_error('quantity'); ?>
						</div> 
					</div> 
					<div class="form-row">
						<div class="col form-group">
							<label>Available In Size:</label>
							<input type="text" class="form-control" id="size" placeholder="XXL Or 9-10 Years" name="size" value="<?php echo $product->size; ?>">
						</div> 
						<div class="col form-group">
							<label>Available In Colour:</label>
							<input type="text" class="form-control" id="color" placeholder="Enter Colour" name="color" value="<?php echo $product->color; ?>">
						</div> 
					</div> 
					<div class="form-group">
						<label for="pwd">Description:</label>
						<textarea class="form-control" id="description"  name="description" cols="30" rows="5"><?= $product->description;?></textarea>
						<?php echo form_error('description'); ?>
					</div>
					<div class="form-row">
						<div class="col form-group">
							<label>Delivery :</label>
							<input type="text" class="form-control" id="delivery" placeholder="Enter Show Delivery" name="delivery" value="<?php echo $product->delivery; ?>">
						</div> 
						<div class="col form-group">
							<label>Delivery In:</label>
							<select name="deliveryin" class="form-control deliveryin" style = "height: 34px"> 
								<option value="">Select</option>
								<option <?php if($product->deliveryin =='Minutes'){echo "selected='selected'"; }?>>Minutes</option>";
								<option <?php if($product->deliveryin =='Hours'){echo "selected='selected'"; }?>>Hours</option>";
								<option <?php if($product->deliveryin =='Days'){echo "selected='selected'"; }?>>Days</option>";
							</select> 
						</div> 
					</div> 
					<div class="form-group"> 
						<label for="varchar">Service</label>  
						<?php echo form_error('service') ?> 
						<select name="service" class="form-control service" style = "height: 34px"> 
							<option value="">Select</option>
							<?php
								foreach($services as $value){
									if($product->service == $value->id ){
										echo "<option selected='selected' value='".$value->id."'>".$value->name."</option>";
									}else{
                                        echo "<option  value='".$value->id."'>".$value->name."</option>";
                                    }
								}
							?>
						</select> 
					</div> 

					<?php $images = $this->db->get_where('images',['product_id'=>$product->id])->result();
						if(count($images)>=5){

						}else{?>
						<div class="form-group">
							<label for="pwd">Image:</label> 
								<input type="file" class="form-control" id="fname" name="fname[]"> 
							<div class="input_fields_wrap">

							</div>
							<button class="add_field_button">Add More</button>

						</div>
					<?php } ?>
				<div class="row">

					<?php foreach ($images as $key => $image) {?>
						<div class="col-md-3">
							<button type="button" class="close" value="<?= $image->id?>">&times;</button>
							<img src="<?= base_url().'assets/images/thumbnails/'.$image->img_name?>">
						</div>
					<?php } ?>
				</div>



				<div class="form-group">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">


$(document).ready(function() {
var max_fields      = <?php echo 5 - count($images);?>; //maximum input boxes allowed
var wrapper       = $(".input_fields_wrap"); //Fields wrapper
var add_button      = $(".add_field_button"); //Add button ID

var x = 1; //initlal text box count
$(add_button).click(function(e){ //on add input button click
e.preventDefault();
if(x < max_fields){ //max input box allowed
x++; //text box increment
$(wrapper).append('<div><input type="file" class="form-control" id="fname" name="fname[]"> <a href="#" class="remove_field">   X </a></div>'); //add input box
}
});

$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
e.preventDefault(); $(this).parent('div').remove(); x--;
})
});


$(document).ready(function(){
  $(".close").click(function(){
    if(confirm("Are you sure you want to delete this?")){
        
    }
    else{
        return false;
    }
    var id = $(this).val();
    //alert(id);
    $.ajax({ 
        url: "<?= base_url('Product/image_delete')?>",
        data: {'id':id},
        type: 'post',
        success: function(data){
            //alert(data);
            location.reload();
          }
    });
  });
});

$("#pries").keyup(function(){
var p = $("#pries").val();
var m = 4/100;
var g = 18/100;
var tatal = p * m;
var tatal1 = tatal *g;
var main = tatal1 +tatal;
var main1 = (p - main).toFixed(2);

$("#share").val(main1);
});
</script>