
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>

<h2>List Of Order</h2>
<!-- <a href = "<?= base_url('Admin/download_excel')?>" class="btn btn-info">Export to excel</a> -->

<?php if($this->session->flashdata('message')){?>
  	<div class="alert alert-success">
		<strong>Success!</strong> <?php echo $this->session->flashdata('message_r');?>.
	</div>
	<?php } ?>
    <?php if($this->session->flashdata('message_r')){?>
    <div class="alert alert-warning">
        <strong>Error!</strong> <?php echo $this->session->flashdata('message_r');?>.
    </div>
    <?php } ?>
<?php 
//print_r($products);?>
  <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Sr. No</th>
                <th>Coustmer Name </th>
                <th>Invoice No </th>
                <th>Total amt</th>
                <th>Order Date</th>
                <th>Status</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Sr. No</th>
                <th>Coustmer Name </th>
                <th>Invoice No </th>
                <th>Total amt</th>
                <th>Order Date</th>
                <th>Status</th>
            </tr>
        </tfoot>
    </table>
<script>
$(document).ready(function(){
    $('#example').DataTable({
        
        "processing": true,
        
        "serverSide": true,
        "order": [[ 2, "desc" ]],
      
        // "order": [],
      
        "ajax": {
            "url": "<?php echo base_url('Product/getLists/'); ?>",
            "type": "POST"
        },
        "aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [0, 5 ] }
       ],
        // "columnDefs": [{ 
        //     "targets": [0],
        //     "orderable": false  
        // }],
        serverSide: true,
        // dom: 'Lfrtip',   
        // buttons: [
        //     'pageLength',
        //     {extend: 'excelHtml5', title: 'Data export' },
        //     {extend: 'csvHtml5', title: 'Data export' },
        //     {extend: 'pdfHtml5', title: 'Data export' }
        // ]
    });
    table.buttons().container()
        .appendTo( '#example .col-sm-6:eq(0)' );

});

// $(document).ready(function() {
//     $('#example').DataTable( {
//         dom: 'Bfrtip',
//         buttons: [
//             { extend: 'copyHtml5', footer: true },
//             { extend: 'excelHtml5', footer: true },
//             { extend: 'csvHtml5', footer: true },
//             { extend: 'pdfHtml5', footer: true }
//         ]
//     } );
// } );
</script>
