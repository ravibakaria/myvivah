
<section id="promo">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div id="promo-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <img src="<?php echo base_url('assets/img/slide_1.jpg')?>">
              </div>
              <div class="item">
                <img src="<?php echo base_url('assets/img/slide_2.jpg')?>">
              </div>
              <div class="item">
                <img src="<?php echo base_url('assets/img/slide_3.jpg')?>">
              </div>
              <div class="item">
                <img src="<?php echo  base_url('assets/img/slide_4.jpg')?>">
              </div>
              <div class="item">
                <img src="<?php echo base_url('assets/img/slide_5.jpg')?>">
              </div>
              <div class="item">
                <img src="<?php echo base_url('assets/img/slide_6.jpg')?>">
              </div>
              <div class="item">
                <img src="<?php echo base_url('assets/img/slide_7.jpg')?>">
              </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#promo-carousel" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#promo-carousel" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="products">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-3">
            <div class="col text-center" id="welcome">
              <h2>Welcome</h2>
              <p>Sign in for the best experience</p>
              <button class="btn"><a href="index1.php">Sign in securely</a></button>
              <div class="footer">
                New to Amazon? <a href="#">Start here</a>
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="col" id="amazon-basics">
              <h2>Explore QuestsBasics</h2>
              <a href="#" class="thumbnail text-center">
                <img src="<?php echo base_url('assets/img/product_1.jpg')?>" alt="Product 1">
                <span class="caption">Home</span>
              </a>
              <a href="#" class="thumbnail text-center">
                <img src="<?php echo base_url('assets/img/product_2.jpg')?>" alt="Product 2">
                <span class="caption">Bluetooth</span>
              </a>
              <a href="#" class="thumbnail text-center">
                <img src="<?php echo base_url('assets/img/product_3.jpg')?>" alt="Product 3">
                <span class="caption">Pets</span>
              </a>
              <a href="#" class="thumbnail text-center">
                <img src="<?php echo base_url('assets/img/product_4.jpg')?>" alt="Product 4">
                <span class="caption">Computer</span>
              </a>
              <div class="footer">
                <a href="#">Shop all AmazonBasics</a>
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="col" id="dress">
              <h2>The dress shop</h2>
              <div class="row text-center">
                <a href="#"><img src="<?php echo base_url('assets/img/dress.png')?>"></a>
              </div>

              <div class="footer">
                <a href="#">Shop women's dresses</a>
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div>
              <a href="#"><img src="<?php echo base_url('assets/img/product_5.jpg')?>" alt="Product 5"></a>
            </div>
            <div class="col" id="deal-day">
              <h2>Deal of the day</h2>
              <h3>$163.99</h3>
              <small>List: <s>$425.00</s> (47% off)</small>
              <a href="#"><img src="<?php echo base_url('assets/img/product_6.jpg')?>" alt="Product 6"></a>
              <div class="footer">
                <a href="#">Shop all deals</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

