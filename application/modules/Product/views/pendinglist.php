

<h2>List Of Pending Products</h2>


<?php if($this->session->flashdata('message')){?>
  	<div class="alert alert-success">
		<strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
	</div>
	<?php } ?>
<?php 
//print_r($products);?>
<table class="table table-striped">
    <thead>
      <tr>
        <th>Sr. No</th>
        <th>Name</th>
        <th>Price</th>
        <th>Quantity</th>
        <th>Business Type</th>
        <th>Image</th>
        <th>Status</th>
        <th>Remark</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php $i = 0;
    	foreach ($products as $key => $value) { $i++;?>
    	<tr>
    	<td><?= $i;?></td>
    	<td><?= $value->name;?></td>
    	<td><?= $value->pries;?></td>
    	<td><?= $value->quantity;?></td>
    	<td> <?php $business = $this->db->get_where('service',['id'=>$value->service])->row(); ?>
      <?= $business->name;?></td>
        <?php $img = $this->db->get_where('images',['product_id'=>$value->id])->row(); 
        if(!empty($img)){?>
        <td><img src="<?= base_url().'/assets/images/thumbnails/'.$img->img_name;?>" width="100" height="70"></td>

    <?php
        }else{
            ?>
             <td></td>
            <?php
        }
        ?>

        <td><?php if($value->status == 1){
            echo "Active";
        }else{
            echo "Inactive";
        }?></td>
         <td><?= $value->remark;?></td>
    	<td>
        <a href="<?= base_url().'Product/view/'.$value->id?>" class="btn btn-primary">View</a> <a href="<?= base_url().'Product/edit_product/'.$value->id?>" class="btn btn-primary">Edit</a>
    	<a href="<?= base_url().'Product/delete_product/'.$value->id?>" class="btn btn-danger">Delete</a></td>    
    	</tr>    
    <?php
    	}
    ?>
    </tbody>
  </table>


</body>
</html>