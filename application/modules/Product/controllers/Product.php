<?php

class Product extends MX_Controller
{
    function __construct()
    {
        $this->load->library('form_validation');
        $this->load->model('Product_md'); 
        parent::__construct();
    }

public  function index()
    {
       // echo "WELOME TO HERE HOME";
        //$this->load->view('test');
        $this->load->userTemplate('index');
        //$this->load->view('welcome_message');
        // if($this->session->userdata('user')){
        //     $this->load->userTemplate('test');
        // }else{
        //     redirect (base_url());
        // }
    }

    public function list_product(){
		
			$data['products'] = $this->Product_md->get_all_active('products');
			$this->load->template('listproduct.php',$data);
		
	}
	
    public function list_pending_product(){
		
			$data['products'] = $this->Product_md->get_all_pending('products');
			$this->load->template('pendinglist',$data);
		
    }
    
    public function edit_product($id){
		
		$product = $this->Product_md->get_by_id('products',$id);
		$service = $this->Product_md->get_all('service');
		$gsts = $this->Product_md->get_all('gst');
			$data = array( 
				'services' =>$service,
				'product'  =>$product,
				
            );
		if(empty($data['product'])){
				echo "404";
		}else{
            $this->load->template('product_edit',$data);
		}

	}

    public function add_product(){
		
			$service = $this->Product_md->get_all('service');
			$gsts = $this->Product_md->get_all('gst');
			$data = array( 
                'services' =>$service,
                'gsts' =>$gsts,
            );
            $this->load->template('add_product',$data);
		
		
    }
    public function add_product_action(){
		if($this->session->userdata('user')){
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('pries', 'Pries', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('service', 'Service', 'required');
			$count = count($_FILES['fname']['size']);
				for($s=0; $s<=$count-1; $s++) {
					if (empty($_FILES['fname']['name'][$s]))
					{
					    $this->form_validation->set_rules('fname', 'Image', 'required');
					} 
			}
	        if ($this->form_validation->run() == FALSE)
	        {
	            $this->load->template('add_product'); 

	        }
	        else
	        {	
                $uid =$this->session->userdata('user')->id;
	        	$userData = array(
	                'name' => $this->input->post('name'),
	                'pries' => $this->input->post('pries'),
	                'service' => $this->input->post('service'),
	               
	                'description' => $this->input->post('description'),
					
					'quantity' => $this->input->post('quantity'),
	                'created_by' => $uid,
	                'status' => 1,
            		);
            	$insertUserData = $this->Product_md->insert('products',$userData);
            	$last_id = $this->db->insert_id();
	        	if(!empty($_FILES['fname']['name'])){
	                
					$count = count($_FILES['fname']['size']);
				        foreach($_FILES as $key=>$value){
					        for($s=0; $s<=$count-1; $s++) {
						        $_FILES['fname']['name']     = $value['name'][$s];
						        $_FILES['fname']['type']     = $value['type'][$s];
						        $_FILES['fname']['tmp_name'] = $value['tmp_name'][$s];
						        $_FILES['fname']['error']    = $value['error'][$s];
						        $_FILES['fname']['size']     = $value['size'][$s]; 
								$image_path = realpath(APPPATH . '../assets/
								/');
                                //$config['upload_path'] = $image_path; 
                                $config['upload_path'] = 'assets/images/'; 
						        $config['allowed_types']        = 'gif|jpg|png|jpeg';
				                $config['max_size']             = 4800;
				                // $config['max_width']            = 1024;
				                // $config['max_height']           = 768;
						        $this->load->library('upload', $config);
						        if($this->upload->do_upload('fname')){
				                    // Uploaded file data
				                    $fileData = $this->upload->data();
				                    $name = $fileData['file_name'];
				                  
				                    $upload_data = $this->upload->data();
						            $data['filename'] = $upload_data['file_name'];
						            //print_r($data['filename']);exit();
						            $this->resize_image($data['filename']);
						           
				                    $userData = array(
							                'img_name' => $name,
							                'product_id' =>$last_id
						            		);
				                    $this->Product_md->insert('images',$userData);
			                	} else{
			                		 $error = array('error' => $this->upload->display_errors());
			                		print_r($error);
			                	}
						       
					        }
					    }
				        //$picture= implode(',', $name_array);
            	}else{
               	 $picture = '';
            	}
            
            	$this->session->set_flashdata('message', 'product added successfully');
            	redirect(base_url('Product/add_product'));
            
        	}
        }	

    }
    
    function resize_image($filename)
    {	
    	$image_path = realpath(APPPATH . '../assets/images/');
        $img_source = $image_path .'/'. $filename;
        $img_target = $image_path .'/thumbnails/'.$filename;
        echo $img_source."<br>";
        echo $img_target."<br>";
        // image lib settings
        $this->load->library('image_lib');
        $config['image_library']    = "gd2";      
	    $config['source_image']     = $img_source;
	    $config['new_image']        = $img_target;
	    //$config['create_thumb']     = TRUE;      
	    $config['maintain_ratio']   = TRUE;      
	    $config['width'] = "128";      
	    $config['height'] = "128";
      
        $this->image_lib->initialize($config);
        if(!$this->image_lib->resize()){
            echo $this->image_lib->display_errors();
        }
        $this->image_lib->clear();
    }

    public function image_delete(){
		$id = $this->input->post('id');
		//echo "ravi";
		$this->Product_md->delete('images', $id);
	}

    public function delete_product($id){
		if($this->session->userdata('user')){
			$this->db->where('id', $id);
	        $this->db->delete('products');
	        $this->db->where('product_id',$id);
	        $this->db->delete('images');
	        redirect(base_url('Product/list_product'));
    	}
	}

	public function getsubservice(){
		$id = $this->input->post('service');
		$sub_services = $this->Product_md->sub_get_by_id('sub_service',$id);
		echo json_encode($sub_services);
	}

	public function update_product($id){
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('pries', 'Pries', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('service', 'Service', 'required');
			
			
	        if ($this->form_validation->run() == FALSE){
	            $this->edit_product($id);
				//  $product['product'] = $this->db->get_where('product', array('id' => $id))->row();
				//  $this->load->template('add_product');
				// $this->load->view('admin/product_edit',$product); 
	        }
	        else{
	        	$userData = array(
					'name' => $this->input->post('name'),
					'pries' => $this->input->post('pries'),
					'service' => $this->input->post('service'),
					'description' => $this->input->post('description'),
					'quantity' => $this->input->post('quantity'),
					'share' => $this->input->post('share'),
					'showpries' => $this->input->post('showpries'),
					'delivery' => $this->input->post('delivery'),
					'deliveryin' => $this->input->post('deliveryin'), 
					'color'=>$this->input->post('color'),
                    'size'=> $this->input->post('size'),
					);
            			 $insertUserData = $this->Product_md->update('products',$id,$userData);

	        	if(!empty($_FILES['fname']['name'])){
					$count = count($_FILES['fname']['size']);
				        foreach($_FILES as $key=>$value){
					        for($s=0; $s<=$count-1; $s++) {
						        $_FILES['fname']['name']     = $value['name'][$s];
						        $_FILES['fname']['type']     = $value['type'][$s];
						        $_FILES['fname']['tmp_name'] = $value['tmp_name'][$s];
						        $_FILES['fname']['error']    = $value['error'][$s];
						        $_FILES['fname']['size']     = $value['size'][$s]; 
						        $image_path = realpath(APPPATH . '../assets/images/');
                                //$config['upload_path'] = $image_path; 
                                $config['upload_path'] = 'assets/images/'; 
						        $config['allowed_types']        = 'gif|jpg|png|jpeg';
				                $config['max_size']             = 4800;
				                // $config['max_width']            = 1024;
				                // $config['max_height']           = 768;
						        $this->load->library('upload', $config);
						        if($this->upload->do_upload('fname')){
				                    // Uploaded file data
				                    $fileData = $this->upload->data();
				                    $name = $fileData['file_name'];
				                  
				                    $upload_data = $this->upload->data();
						            $data['filename'] = $upload_data['file_name'];
						            //print_r($data['filename']);exit();
						            $this->resize_image($data['filename']);
						           
				                    $userData = array(
							                'img_name' => $name,
							                'product_id' =>$id
						            		);
				                    $this->Product_md->insert('images',$userData);
			                	} else{
			                		 $error = array('error' => $this->upload->display_errors());
			                		print_r($error);
			                	}
						       
					        }
					    }
					    $this->session->set_flashdata('message', 'product Update successfully');
            			redirect(base_url('Product/list_product'));
            		
	            }
	             $this->session->set_flashdata('message', 'product Update successfully');
				 redirect(base_url('Product/list_product'));
	           
	            
	        }     
	}

	public function view_invoice($invoice){
		
		 $invoices['invoice'] = $this->db->get_where('orders_items', array('invoice_no' => $invoice))->result_array();
		
		 if(empty($invoices['invoice'])){
		 	$this->session->set_flashdata('message_r', 'invoice not found');
		 	redirect(base_url('Product/order_list'));
		 }else{
			  //$this->load->view('admin/products/invoice',$invoices);
			  $this->load->template('invoice',$invoices);
		 }
		 
	}

	public function order_list(){
		$sql = "SELECT orders.u_id, orders.invoice_id,orders.date, orders.total_amt, users.name,invoices.invoice_no FROM `orders` LEFT JOIN users ON users.id = orders.u_id LEFT JOIN invoices ON invoices.id = orders.invoice_id";
	
		$data = [];
		// $this->load->view('admin/products/orderlist',$data);
		$this->load->template('orderlist',$data);
	}

	public function getLists(){
        $data = $row = array();
       // print_r($_POST);exit();
        $memData = $this->Product_md->getRows($_POST);
        
        $i = $_POST['start'];
        foreach($memData as $member){
            $i++;
            $status = '<a href="'.base_url().'Product/view_invoice/'.$member->invoice_no.'"'.' class="btn btn-success">View Invoice</a>';
            $created = date( 'jS M Y', strtotime($member->date));
            $data[] = array($i, $member->name, $member->invoice_no, $member->total_amt, $created,$status);
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Product_md->countAll(),
            "recordsFiltered" => $this->Product_md->countFiltered($_POST),
            "data" => $data,
        );
        
        // Output to JSON format
        echo json_encode($output);
	}
	function readMoreHelper($story_desc, $chars = 100) {
		$story_desc = substr($story_desc,0,$chars);  
		$story_desc = substr($story_desc,0,strrpos($story_desc,' '));  
		$story_desc = $story_desc."......";  
		return $story_desc;  
	}
	public function prodgetLists(){
        $data = $row = array();
       // print_r($_POST);exit();
        $memData = $this->Product_md->getRows1($_POST);
        
        $i = $_POST['start'];
        foreach($memData as $member){
			$i++;
			$st ='';
			if($member->status == 1){
				$st ='Active';
			}else{
				$st ='InActive';
			}
           
            $status = $st;
		   $img = "<img src='".base_url()."/assets/images/thumbnails/".$member->img_name."' width='100' height='70'>";
		   $action = "<a href='".base_url()."Product/edit_product/".$member->id."' class='btn btn-primary'>Edit</a>
		   <a href='".base_url()."Product/delete_product/".$member->id."' class='btn btn-danger'>Delete</a>";
			
            $data[] = array($i, $member->name, $member->pries,$this->readMoreHelper($member->description),$member->servicename, $img,$member->remark,$status, $action);
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Product_md->countAll1(),
            "recordsFiltered" => $this->Product_md->countFiltered1($_POST),
            "data" => $data,
        );
        
        // Output to JSON format
        echo json_encode($output);
	}
	
	public function add_gst(){
		$this->form_validation->set_rules('gst', 'GST', 'required');
		$this->form_validation->set_rules('discription', 'Discription', 'required');
		if ($this->form_validation->run() == FALSE) 
			{ 
			$this->create_gst(); 
			} 
		else 
			{ 
			$data = array( 
				'gst' => $this->input->post('gst', TRUE) ,  
				'discription' => $this->input->post('discription', TRUE) ,  
				
			); 
			$this->Product_md->insert('gst',$data); 
			$this->session->set_flashdata('message', 'Create Record Success'); 
			redirect(base_url('Product/gst')); 
			} 
	}

	public  function gst()
        {
           // echo "WELOME TO HERE HOME";
           $gst = $this->Product_md->get_all('gst'); 
           $margins = $this->Product_md->get_all('margin'); 
           $data = array( 
			   'gst_data' => $gst ,
			   'margins' => $margins ,
           ); 
            $this->load->template('gst',$data);
            //$this->load->view('welcome_message');
        }

	// public function create_gst() 
    //     {  
    //     $data = array( 
    //         'button' => 'Create' ,
    //         'action' => base_url('Product/add_gst') , 
    //         'id' =>  '', 
    //         'discription' =>  '',
    //         'gst' =>  ''
    //     ); 
    //     $this->load->template('addgst',$data);
	// 	}
		

	public function update_gst($id) 
    { 
    $row = $this->Product_md->get_by_id('gst',$id); 
    if ($row) 
        { 
        $data = array( 
            'button' => 'Update', 
            'action' => base_url('Product/update_gst_action') , 
            'id' =>  $row->id, 
			'gst' => $row->gst,
			'discription' =>  $row->discription,  
        ); 
        $this->load->template('addgst',$data);
        } 
      else 
        { 
        $this->session->set_flashdata('message', 'Record Not Found'); 
        redirect(base_url('Product/gst')); 
        } 
    }

    public function update_gst_action() 
        { 
			$this->form_validation->set_rules('gst', 'GST', 'required');
			$this->form_validation->set_rules('discription', 'Discription', 'required');
        if ($this->form_validation->run() == FALSE) 
            { 
            $this->update_gst($this->input->post('id', TRUE)); 
            } 
          else 
            { 
            $data = array( 
                'gst' => $this->input->post('gst', TRUE) ,  
				'discription' => $this->input->post('discription', TRUE) , 
            ); 
            $this->Product_md->update_service('gst',$this->input->post('id', TRUE) , $data); 
            $this->session->set_flashdata('message', 'Update Record Success'); 
            redirect(base_url('Product/gst')); 
            } 
        }

       public function delete_gst($id) 
        { 
        //$row = $this->Product_md->get_by_id('gst',$id); 
        if ($row) 
            { 
            $this->Product_md->delete('gst',$id); 
            $this->session->set_flashdata('message', 'Delete Record Success'); 
            redirect(base_url('Product/gst')); 
            } 
          else 
            { 
            $this->session->set_flashdata('message', 'Record Cant delete'); 
			redirect(base_url('Product/gst'));
            } 
		}
		
		public function view($id){
			$data = [];
			$images = $this->db->get_where('images', array('product_id' => $id))->result();
			$product = $this->db->get_where('products', array('id' => $id))->row();
			//$product = $this->Product_md->get_by_id('products',$id);  
			$data = [
				'images' =>$images,
				'product' =>$product,
            ];
			$this->load->template('viewproduct.php',$data);
		}

		public function active()  
			{ 
				$id  = $this->input->post('id');
				$data = array( 
					'status' => 1    
				); 
				$this->Product_md->update('products',$id, $data); 
				$this->session->set_flashdata('message', 'Update Record Success'); 
				redirect(base_url('Product/list_pending_product')); 
				
			}

		public function add_remark(){
			$id = $this->input->post('id');
			$userData = array(
				'remark' => $this->input->post('remark'),
				
				);
				$insertUserData = $this->Product_md->update('products',$id,$userData);
				redirect(base_url('Product/list_pending_product')); 
		}
}

?>