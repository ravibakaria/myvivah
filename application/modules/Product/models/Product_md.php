<?php

class Product_md extends CI_Model
{
    public $id = 'id'; 
  
    public $order = 'DESC'; 
    function __construct()
    {
          // parent::__construct();
           $this->column_order = array(null, 'name','invoice_no','total_amt', 'date','status');
           $this->column_order1 = array(null, 'products.name','products.pries','products.description','products.status');
           // Set searchable column fields
          $this->column_search = array('name','invoice_no','date','total_amt');
          $this->column_search1 = array('products.name','products.pries','products.description','products.status');
           // Set default order
           $this->order = array('name' => 'asc');
    }


function update_service($table,$id, $data) 
    { 
    $this->db->where($this->id, $id); 
    $this->db->update($table, $data); 
    } 
function get_all_pending($table) 
    { 
    $this->db->where('status', 0); 
    $this->db->order_by($this->id, 'DESC'); 
    return $this->db->get($table)->result(); 
} 
function get_all_active($table) 
    { 
    $this->db->where('status', 1); 
    $this->db->order_by($this->id, 'DESC'); 
    return $this->db->get($table)->result(); 
} 
function get_all($table) 
    { 
    $this->db->order_by($this->id, 'DESC'); 
    return $this->db->get($table)->result(); 
} 

function get_by_id($table,$id) 
{ 
$this->db->where($this->id, $id); 
return $this->db->get($table)->row(); 
} 

// insert data 

function insert($table,$data) 
{ 
$this->db->insert($table, $data); 
} 

// update data 

function update($table,$id, $data) 
{ 
$this->db->where($this->id, $id); 
$this->db->update($table, $data); 
} 

// delete data 

function delete($table,$id) 
{ 
$this->db->where($this->id, $id); 
$this->db->delete($table); 
}

function sub_get_by_id($table,$id) 
    { 
    $this->db->where('service_id', $id); 
    return $this->db->get($table)->result(); 
    } 

    public function getRows($postData){
        $this->_get_datatables_query($postData);
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
       // echo $this->db->last_query();exit();
        return $query->result();
    }
    public function getRows1($postData){
        $this->_get_datatables_query1($postData);
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();exit();
        return $query->result();
    }

    public function countAll(){
        //$this->db->from($this->table);
        $this->db->select('orders.u_id, orders.invoice_id,orders.date, orders.total_amt, users.name,invoices.invoice_no');
	    $this->db->from('orders');
	    $this->db->join('users', 'users.id = orders.u_id', 'left'); 
	    $this->db->join('invoices', 'invoices.id = orders.invoice_id', 'left'); 
        return $this->db->count_all_results();
    }
    public function countAll1(){
        $this->db->select(' products.id, products.name,products.pries, products.description, products.status,images.img_name, service.name as servicename');
	    $this->db->from('products');
	    $this->db->join('images', 'products.id = images.product_id', 'left'); 
        $this->db->join('service', 'service.id = products.service', 'left'); 
        $this->db->where('products.status',1);
        $this->db->group_by('products.id');
         return $this->db->count_all_results();
    
    }
    public function countFiltered($postData){
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function countFiltered1($postData){
        $this->_get_datatables_query1($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }
    private function _get_datatables_query($postData){
        $this->db->select('orders.u_id, orders.invoice_id,orders.date, orders.total_amt, users.name,invoices.invoice_no');
	    $this->db->from('orders');
	    $this->db->join('users', 'users.id = orders.u_id', 'left'); 
	    $this->db->join('invoices', 'invoices.id = orders.invoice_id', 'left'); 
	     
        $i = 0;
        // loop searchable columns 
        foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i===0){
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                
                // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
         
        if(isset($postData['order'])){
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    private function _get_datatables_query1($postData){
        $this->db->select('products.id, products.name,products.pries,products.remark, products.description, products.status,images.img_name, service.name as servicename');
	    $this->db->from('products');
	    $this->db->join('images', 'products.id = images.product_id', 'left'); 
        $this->db->join('service', 'service.id = products.service', 'left'); 
        $this->db->where('products.status',1);
        $this->db->group_by('products.id');
	     
        $i = 0;
        // loop searchable columns 
        foreach($this->column_search1 as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i===0){
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                
                // last loop
                if(count($this->column_search1) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
         
        if(isset($postData['order'])){
            $this->db->order_by($this->column_order1[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

}

?>