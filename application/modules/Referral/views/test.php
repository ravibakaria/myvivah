<section id="actions" class="py-4 mb-4">
    <div class="container">
      
    </div>
  </section>

  <!-- POSTS -->
  <section id="posts">
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-6">
         <div class="card text-center bg-primary text-white mb-3">
            <div class="card-body">
              <h3>My Earning</h3>
              <h4 class="display-4"><i class="fas fa-pencil-alt"></i>
               <span id="">&#x20B9; <?php echo $myearning;?></span>
              </h4>
              <button class="btn btn-outline-light btn-sm">View</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
        <div class="col-md-6">
         <div class="card text-center bg-primary text-white mb-3">
            <div class="card-body">
              <h3>Referral Code/Quest Account No.</h3>
              <h4 class="display-4"><i class="fas fa-pencil-alt"></i>
               <span id="referral"><?php echo $this->session->userdata('reff')->referral_code ?></span>
              </h4>
              <button data-toggle="modal" data-target="#addPostModal" id="copyBtn" class="btn btn-outline-light btn-sm">Copy</button>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="card text-center bg-warning text-white mb-3">
            <div class="card-body">
              <h3>Shopkeepers Referred</h3>
              <h4 class="display-4"><i class="fas fa-store"></i>
                <?php  echo count($shop); ?>
              </h4>
              <button data-toggle="modal" data-target="#viewShopkeeper" class="btn btn-outline-light btn-sm">View</button>
            </div>
          </div>
        </div>
      </div>
</section>

<!-- ADD CATEGORY MODAL -->
<div class="modal fade" id="addPostModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                <h5 class="modal-title">Referral Code Copied!</h5>
                <button class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4>Referral Code <?php echo $this->session->userdata('reff')->referral_code ?> Copied Successfully!</h4>
            </div>
        </div>
    </div>
</div>

<!-- ADD USER MODAL -->
<div class="modal fade" id="viewShopkeeper">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-warning text-white">
                <h5 class="modal-title">Shopkeepers Referred</h5>
                <button class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Shopkeeper Name</th>
                            <th scope="col">Shop Name</th>
                            <th scope="col">Shop Address</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($shop as $s){?>
                        <tr>
                            <td><?php echo $s->name?></td>
                            <td><?php echo $s->shopname?></td>
                            <td><?php echo $s->shopadd?></td>
                        </tr>
                       <?php }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>

var copyTextareaBtn = document.querySelector('#copyBtn');

copyTextareaBtn.addEventListener('click', function(event) {
  var copy_text = document.getElementById("referral");
  //alert(copy_text);
  var range = document.createRange();
  range.selectNode(copy_text);
  window.getSelection().removeAllRanges();
  window.getSelection().addRange(range);

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Copying text command was ' + msg);
  } catch (err) {
    console.log('Oops, unable to copy');
  }
});

</script>
