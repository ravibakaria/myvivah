<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

  
       
  <div class="jumbotron " style="
    background-color: rosybrown;
">
  			<div class="widget-header mr-3">
				<a href="https://quests.co.in/" class="brand-wrap">
					<img class="logo" src="https://quests.co.in/assets/images/logo.png" height = "47" width = "80"> 
				</a> <!-- brand-wrap.// -->
				</div>
    <h1 class = "text-center" style="
    margin-top: -61px;
">Referral Partner</h1>      
  </div>
<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-conten padding-y" style="min-height:84vh">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<!-- ============================ COMPONENT LOGIN   ================================= -->
	<div class="card mx-auto" style="max-width: 380px; margin-top:60px;">
    <?php if($this->session->flashdata('message')){?>
        <div class="alert alert-success">
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_r');?>.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('message_r')){?>
        <div class="alert alert-warning">
            <strong>Error!</strong> <?php echo $this->session->flashdata('message_r');?>.
        </div>
        <?php } ?>
      <div class="card-body">
      <h4 class="card-title mb-4">Sign in</h4>
      <form id = "loginform" method = "post" action = "<?php echo base_url('Referral/userlogin')?>">
      	 
          <div class="form-group">
			 <input  class="form-control" placeholder="Email" type="email" name ="email">
          </div> <!-- form-group// -->
          <div class="form-group">
			<input name="password" class="form-control" placeholder="Password" type="password">
          </div> <!-- form-group// -->
          
          <div class="form-group">
          	<a href="#" class="float-right">Forgot password?</a> 
            <label class="float-left custom-control custom-checkbox"> <input type="checkbox" class="custom-control-input" > <div class="custom-control-label"> Remember </div> </label>
          </div> <!-- form-group form-check .// -->
          <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block"> Login  </button>
          </div> <!-- form-group// -->    
      </form>
      </div> <!-- card-body.// -->
    </div> <!-- card .// -->

     <p class="text-center mt-4">Don't have account? <a class = "sheck" href="<?php echo base_url('Referral/register')?>">Sign up</a></p>
     <br><br>
<!-- ============================ COMPONENT LOGIN  END.// ================================= -->
<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function() {
    $j("#loginform").validate({
        rules: {
         
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            }
        },
        messages: {
          
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            }
        }
    });
});
</script>

</section>
<!-- ========================= SECTION CONTENT END// ========================= -->
<style>
.sheck{
	border: 0;
	font-size: 25px;
	padding: 10px 20px;
	position: relative;
	animation-name: shake;
	animation-duration: 5s;
	animation-iteration-count: infinite;
	animation-timing-function: ease-in;
	cursor: pointer;
}
@keyframes shake {
  0% {left: 0}
  1% {left: -3px}
  2% {left: 5px}
  3% {left: -8px}
  4% {left: 8px}
  5% {left: -5px}
  6% {left: 3px}
  7% {left: 0}
}
</style>