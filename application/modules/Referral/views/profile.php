
<style>
.error{
	color: red;
}
</style>
<div class="row text-center">
    <div class="col-md-8 text-center">
        <?php if($this->session->userdata('reff')->status == 0){?>
         <h3>Your profile not active. To activate <a href="<?php echo base_url('Referral/status')?>" class="btn btn-primary"> click here</a></h3>
      
       <?php } ?>
    </div>
    
  </div>

<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<!-- ============================ COMPONENT REGISTER   ================================= -->
	<div class="card mx-auto" style="max-width:520px; margin-top:40px;">
        <?php if($this->session->flashdata('message')){?>
        <div class="alert alert-success">
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_r');?>.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('message_r')){?>
        <div class="alert alert-warning">
            <strong>Error!</strong> <?php echo $this->session->flashdata('message_r');?>.
        </div>
        <?php } ?>
      <article class="card-body">
		<header class="mb-4"><h4 class="card-title">My Profile</h4></header>
		<form id = "registerform" method = "post" action = "<?php echo base_url('Referral/profile_action');?>">
				<div class="form-row">
					<div class="col form-group">
						<label>First name</label>
					  	<input type="text" class="form-control" placeholder="" name = "name" value = "<?php echo $prifile->name;?>">
					</div> <!-- form-group end.// -->
					<div class="col form-group">
						<label>Last name</label>
					  	<input type="text" class="form-control" placeholder="" name ="lname"  value = "<?php echo $prifile->lname;?>">
					</div> <!-- form-group end.// -->
				</div> <!-- form-row end.// -->
				<div class="form-group">
					<label>Email</label>
					<input type="email" class="form-control" placeholder="" name ="email"  value = "<?php echo $prifile->email;?>" readonly>
					<small class="form-text text-muted">We'll never share your email with anyone else.</small>
				</div> <!-- form-group end.// -->
                <div class="form-group">
					<label>Mobile No</label>
					<input type="number" class="form-control" placeholder="" name = "phone"  value = "<?php echo $prifile->phone;?>">
					<small class="form-text text-muted">We'll never share your Mobile No. with anyone else.</small>
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label class="custom-control custom-radio custom-control-inline">
					  <input class="custom-control-input" <?php if($prifile->gender =="Male"){echo "checked";}?> type="radio" name="gender" value="Male">
					  <span class="custom-control-label"> Male </span>
					</label>
					<label class="custom-control custom-radio custom-control-inline">
					  <input class="custom-control-input" type="radio" name="gender" value="Female" <?php if($prifile->gender =="Female"){echo "checked";}?>>
					  <span class="custom-control-label"> Female </span>
					</label>
				</div> <!-- form-group end.// -->
				<div class="form-row">
					<div class="form-group col-md-6">
					  <label>City</label>
					  <input type="text" class="form-control" name ="city" value = "<?php echo $prifile->city;?>">
					</div> <!-- form-group end.// -->
					<div class="form-group col-md-6">
					  <label>Country</label>
					  <select id="inputState" class="form-control" name="country">
					    <option> Choose...</option>
					      <option selected="">INDIA</option>
					  </select>
					</div> <!-- form-group end.// -->
				</div> <!-- form-row.// -->
				
				
				<div class="form-group">
					<label>Pin code</label>
					<input type="text" class="form-control" placeholder="" name = "pin" minlength="6"
					maxlength="6" value = "<?php echo $prifile->pin;?>">
				</div> <!-- form-group end.// -->

				<header class="mb-4"><h4 class="card-title">My Bank Details</h4></header>

				<div class="form-group">
					<label>Bank Account Holder Name</label>
					<input type="text" class="form-control" placeholder="" name = "holdername"  value = "<?php echo $prifile->holdername;?>">
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>Bank Name</label>
					<input type="text" class="form-control" placeholder="" name = "bankname" value = "<?php echo $prifile->bankname;?>">
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>Account No.</label>
					<input type="text" class="form-control" placeholder="" name = "accountno" value = "<?php echo $prifile->accountno;?>">
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>IFSC Code</label>
					<input type="text" class="form-control" placeholder="" name = "ifsc"  value = "<?php echo $prifile->ifsc;?>">
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>Google Pay Number Or UPI</label>
					<input type="text" class="form-control" placeholder="" name = "upi" value = "<?php echo $prifile->upi;?>">
				</div> <!-- form-group end.// -->
			    <div class="form-group">
			        <button type="submit" class="btn btn-primary btn-block"> Update  </button>
			    </div> <!-- form-group// -->      
			              
			</form>
		</article><!-- card-body.// -->
    </div> <!-- card .// -->
   
    <br><br>
<!-- ============================ COMPONENT REGISTER  END.// ================================= -->


</section>
<!-- ========================= SECTION CONTENT END// ========================= -->
<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function() {
    $j("#registerform").validate({
        rules: {
            name: "required",
            lname: "required",
            phone: "required",
            city: "required",
            country: "required",
            shopname: "required",
            shopno: "required",
            shopadd: "required",
            businss: "required",
           
			pin: {
				required: true,
				number: true
			},
            email: {
                required: true,
                email: true
            }
        },
        messages: {
          
           
        }
    });
});
</script>