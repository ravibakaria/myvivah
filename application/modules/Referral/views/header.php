<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Quests.co.in: Online Shopping for Electronics, Apparel, Computers, Books, DVDs &amp; more</title>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <link href="<?php echo base_url('assets/css/ui.css');?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/responsive.css');?>" rel="stylesheet">
</head>
<style>
.btn-primary {
  color: #fff !important;
  background-color: #ff6a00 !important;
  border-color: #ff6a00 !important;
}
.error{
	color:red;
}
.maincontener{
  margin-top : 40px;
  margin-bottom : 40px;
}
</style>
<body>
<header class="section-header">
<section class="header-main border-bottom">
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    
	<a href="<?php echo base_url();?>" class=" navbar-brandbrand-wrap">
					<img class="img img-thumbnail" src="<?php echo base_url('assets/images/logo.png');?>" width ="120px" height = "120px">
				</a> <!-- brand-wrap.// -->
    <?php if($this->session->userdata('reff')->status ==1){ ?>
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      
        <a class="nav-link" href="<?php echo base_url('Referral/Home');?>">MY Portfolio</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('Referral/report');?>">Reports</a>
      </li>
    </ul>
    <?php } ?>
    <?php if($this->session->userdata('reff') !=''){ ?>

					<div class="widget-header mr-3">
						<a href="#" class="widget-view" data-toggle="dropdown">
							<div class="icon-area">
								<i class="fa fa-user"></i>
								
							</div>
							<small class="text"> My profile </small>
						</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="<?php echo base_url('Referral/profileactivate');?>">Profile</a>
							<a class="dropdown-item" href="#">Change Password</a>
							<a class="dropdown-item" href="<?php echo base_url('Referral/logout');?>">Log Out</a>
							
						</div>
					</div>
          <div class="widget-header mr-3">
						
							<h5> Current balance &#x20B9; <?php echo $this->session->userdata('bal')?></h5>
						
					</div>
					<?php } else{?>
						<div class="widget-header mr-3">
						<a href="<?php echo base_url('Referral');?>" class="widget-view">
							<div class="icon-area">
								<i class="fa fa-user"></i>
								
							</div>
							<small class="text"> Login </small>
						</a>
					</div>
						<?php 
					} ?>
  </div>
</nav>


	</section>
</header> <!-- section-header.// -->
<div class="container maincontener">
  