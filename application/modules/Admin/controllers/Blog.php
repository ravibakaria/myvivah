<?php

class Blog extends MX_Controller
{
    function __construct(){
        $this->load->library('form_validation');
        $this->load->model('Admin_md'); 
        $this->load->library('pagination');
        parent::__construct();
    }

    public  function index(){
        if($this->session->userdata('user')){
            $blogs = $this->Admin_md->get_all('blogs'); 
            $data = array( 
                'blogs' => $blogs 
            ); 
            $this->load->template('bloglist',$data);
        }else{
            redirect (base_url("Admin"));
        }
    }

    public function create_blog(){
        if($this->session->userdata('user')){
            $data = array( 
                'button' => 'Create' ,
                'action' => base_url('Admin/blog/add_blog') , 
                'id' =>  '', 
                'heading' =>  '',
                'body' =>  '',
            ); 
            $this->load->template('addblog',$data);
        }else{
            redirect (base_url("Admin"));
        }
    }

    public function add_blog(){
        if($this->session->userdata('user')){
            
            $this->form_validation->set_rules('heading', 'Heading', 'trim|required'); 
            $this->form_validation->set_rules('body', 'Body', 'trim|required'); 
            
            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>'); 
            if ($this->form_validation->run() == FALSE) { 
                $this->create_blog(); 
            }else { 
                $data = array( 
                    'heading' => $this->input->post('heading', TRUE) , 
                    'body' => $this->input->post('body') , 
                    'date' => $this->input->post('date') , 
                ); 
                // print_r($data);die();
                $insert_id = $this->Admin_md->insert('blogs',$data); 
            
                $this->session->set_flashdata('message', 'Create Record Success'); 
                redirect(base_url('Admin/blog')); 
            } 
        }else{
            redirect (base_url("Admin"));
        }
    }

    public function inactive_blog($id) 
    { 
        $data = array( 
            'status' => 0    
        ); 
        $this->Admin_md->update_service('blogs',$id , $data); 
        $this->session->set_flashdata('message', 'Update Record Success'); 
        redirect(base_url('Admin/blog')); 
        
    }
    public function active_blog($id) 
    { 
        $data = array( 
            'status' => 1    
        ); 
        $this->Admin_md->update_service('blogs',$id, $data); 
        $this->session->set_flashdata('message', 'Update Record Success'); 
        redirect(base_url('Admin/blog'));  
        
    }
    public function blog($id){
        if($this->session->userdata('user')){
            $blog = $this->Admin_md->get_by_id('blogs',$id);
            $data = ['blog' => $blog];
            $this->load->template('singleblog',$data);
        }else{
            redirect (base_url("Admin"));
        }
    }

    public function delete_blog($id){
        
        $this->Admin_md->delete('blogs',$id);
        $this->session->set_flashdata('message', 'Delete Record Success'); 
        redirect(base_url('Admin/blog'));  
    }

    public function edit_blog($id){
        if($this->session->userdata('user')){
            $blog = $this->Admin_md->get_by_id('blogs',$id);
            $data = array( 
                'button' => 'Update' ,
                'action' => base_url('Admin/blog/update_blog') , 
                'id' =>  $blog->id, 
                'heading' =>  $blog->heading,
                'body' =>  $blog->body,
            ); 
            $this->load->template('addblog',$data);
        }else{
            redirect (base_url("Admin"));
        }
    }

    public function update_blog(){
        $this->form_validation->set_rules('heading', 'Heading', 'trim|required'); 
        $this->form_validation->set_rules('body', 'Body', 'trim|required'); 
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>'); 
        if ($this->form_validation->run() == FALSE) { 
            $this->edit_blog($this->input->post('id', TRUE)); 
        } else  { 
            $data = array( 
                'heading' => $this->input->post('heading', TRUE) , 
                'body' => $this->input->post('body') , 
                
            ); 
            $this->Admin_md->update('blogs',$this->input->post('id', TRUE) , $data);
            $this->session->set_flashdata('message', 'Update Record Success'); 
            redirect(base_url('Admin/blog'));  
        }
    }

}