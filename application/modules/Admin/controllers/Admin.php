<?php

class Admin extends MX_Controller
{
    function __construct()
    {
        $this->load->library('form_validation');
        $this->load->model('Admin_md'); 
        $this->load->library('pagination');
        parent::__construct();
    }

public  function index()
    {
        if($this->session->userdata('user')){
            redirect (base_url("Admin/adminhome"));
        }else{
         $this->load->view('login');
        }
    }

    public function adminlogin(){
       
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $this->load->model('Admin_md');
            //$email = 'ravi@gmail.com';
            $var = $this->Admin_md->login($email);
            if(count($var) == 1){
                if($var->password == md5($password)){
                    $this->session->set_userdata('user',$var);
                    redirect (base_url("Admin/adminhome"));
                    
                }else{
                    $this->session->set_flashdata('message', 'Password invalid');
                    redirect (base_url("Admin"));
                }
            }else{
                $this->session->set_flashdata('message', 'email invalid');
                redirect (base_url("Admin"));
            }
        // }else{
        //     redirect("Admin");
        // }
    }
    public function adminhome(){
        if($this->session->userdata('user')){
            $referrals = $this->Admin_md->get_all('referrals'); 
            $shopkeepers = $this->Admin_md->get_all('bussines_partner');
            $data = [
                'referrals'=>count($referrals),
                'shopkeepers'=>count($shopkeepers),
            ];
            $this->load->template('test',$data);
        }else{
            redirect (base_url("Admin"));
        }
        
    }
    public function adminlogout(){
        $this->session->unset_userdata('user');
        $this->session->sess_destroy();
        redirect (base_url("Admin"));
        
    }

    public function bannersetting(){
        if($this->session->userdata('user')){
            $service = $this->Admin_md->get_all('banner'); 
            $data = array( 
                'banner' => $service 
            ); 
            $this->load->template('beneerlist',$data);
        }else{
            redirect (base_url("Admin"));
        }
        
    }

    public  function role()
    {
       // echo "WELOME TO HERE HOME";
       $service = $this->Admin_md->get_all('roles'); 
       $data = array( 
           'service_data' => $service 
       ); 
        $this->load->template('role_list',$data);
        //$this->load->view('welcome_message');
    }

    public function create_role() 
    {  
    $data = array( 
        'button' => 'Create' ,
        'action' => base_url('Admin/Admin/add_role') , 
        'id' =>  '', 
        'name' =>  ''
    ); 
    $this->load->template('add_role',$data);
    }
    public function create_banner() 
    {  
    $data = array( 
        'button' => 'Create' ,
        'action' => base_url('Admin/Admin/add_banner') , 
        'id' =>  '', 
        'name' =>  '',
        'error' =>''
    ); 
    $this->load->template('addbanner',$data);
    }

public function add_role() 
{ 
$this->_rulese(); 
if ($this->form_validation->run() == FALSE) 
    { 
    $this->create_role(); 
    } 
  else 
    { 
    $data = array( 
        'name' => $this->input->post('name', TRUE) ,  
        'created_at' => date('Y-m-d H:i:s') , 
    ); 
    $this->Admin_md->insert('roles',$data); 
    $this->session->set_flashdata('message', 'Create Record Success'); 
    redirect(base_url('Admin/Admin/role')); 
    } 
}
public function add_banner() 
{ 
    $config['upload_path'] = 'assets/images/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = 2000;
    // $config['max_width'] = 1500;
    // $config['max_height'] = 1500;
    $this->load->library('upload', $config);
    if (!$this->upload->do_upload('banner')) {
        $error =  $this->upload->display_errors();
        $this->session->set_flashdata('message', $error); 
        redirect(base_url('Admin/Admin/create_banner')); 
    } else {
        $data = $this->upload->data();
        $imagename = $data['file_name'];
    $sdata = array( 
        'name' => $imagename ,  
        'created_at' => date('Y-m-d H:i:s') ,
        'status' =>0
    ); 
    $this->Admin_md->insert('banner',$sdata); 
    $this->session->set_flashdata('message', 'Create Record Success'); 
    redirect(base_url('Admin/Admin/bannersetting')); 
}
    
}

public function update_role($id) 
{ 
$row = $this->Admin_md->get_by_id('roles',$id); 
if ($row) 
    { 
    $data = array( 
        'button' => 'Update', 
        'action' => base_url('Admin/Admin/update_role_action') , 
        'id' =>  $row->id, 
        'name' => $row->name,  
    ); 
    $this->load->template('add_role', $data); 
    } 
  else 
    { 
    $this->session->set_flashdata('message', 'Record Not Found'); 
    redirect(base_url('Admin/Admin/role')); 
    } 
}

public function update_role_action() 
    { 
    $this->_rulese(); 
    if ($this->form_validation->run() == FALSE) 
        { 
        $this->update_service($this->input->post('id', TRUE)); 
        } 
      else 
        { 
        $data = array( 
            'name' => $this->input->post('name', TRUE) , 
             
        ); 
        $this->Admin_md->update_service('roles',$this->input->post('id', TRUE) , $data); 
        $this->session->set_flashdata('message', 'Update Record Success'); 
        redirect(base_url('Admin/Admin/role')); 
        } 
    }

    public function _rulese() 
    { 
    $this->form_validation->set_rules('name', 'Name', 'trim|required');   
    $this->form_validation->set_rules('id', 'id', 'trim'); 
    $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>'); 
    }

   public function delete_role($id) 
    { 
    $row = $this->Admin_md->get_by_id('roles',$id); 
    if ($row) 
        { 
        $this->Admin_md->delete('roles',$id); 
        $this->session->set_flashdata('message', 'Delete Record Success'); 
        redirect(base_url('Admin/Admin/role')); 
        } 
      else 
        { 
        $this->session->set_flashdata('message', 'Record Not Found'); 
        redirect(base_url('Admin/Admin/role')); 
        } 
    }
   public function delete_banner($id) 
    { 
    $row = $this->Admin_md->get_by_id('banner',$id); 
    if ($row) 
        { 
        $this->Admin_md->delete('banner',$id); 
        $this->session->set_flashdata('message', 'Delete Record Success'); 
        redirect(base_url('Admin/Admin/bannersetting')); 
        } 
      else 
        { 
        $this->session->set_flashdata('message', 'Record Not Found'); 
        redirect(base_url('Admin/Admin/bannersetting')); 
        } 
    }

    public function inactive_banner($id) 
    { 
        $data = array( 
            'status' => 0    
        ); 
        $this->Admin_md->update_service('banner',$id , $data); 
        $this->session->set_flashdata('message', 'Update Record Success'); 
        redirect(base_url('Admin/Admin/bannersetting')); 
        
    }
    public function active_banner($id) 
    { 
        $data = array( 
            'status' => 1    
        ); 
        $this->Admin_md->update_service('banner',$id, $data); 
        $this->session->set_flashdata('message', 'Update Record Success'); 
        redirect(base_url('Admin/Admin/bannersetting')); 
        
    }

    public function database(){
        $this->load->template('test1');
    }
    public function profile($id){
        $row = $this->Admin_md->get_by_id('admin',$id);
        $data = array( 
            'button' => 'Update', 
            'action' => base_url('Admin/update_profile_action') , 
            'id' =>  $row->id, 
            'name' => $row->name, 
            'email' => $row->email, 
            'phone' => $row->phone,  

        ); 
        $this->load->template('profile',$data);
    }
    public function update_profile_action(){
            $this->form_validation->set_rules('name', 'Name', 'trim|required'); 
            $this->form_validation->set_rules('email', 'Email', 'trim|required'); 
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required'); 
            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>'); 
        if ($this->form_validation->run() == FALSE) 
            { 
            $this->profile($this->input->post('id', TRUE)); 
            } 
          else  
            { 
            $data = array( 
                'name' => $this->input->post('name', TRUE) , 
                'email' => $this->input->post('email', TRUE) , 
                'phone' => $this->input->post('phone', TRUE) ,
               
            ); 
            $this->Admin_md->update('admin',$this->input->post('id', TRUE) , $data);
            
            $this->session->set_flashdata('message', 'Update Record Success'); 
            redirect(base_url('Admin')); 
            } 
    }

    public function changepassword(){
        $this->load->template('changepassword');
    }

    public function changepass_action(){
        
        $data = array( 
            'password' => md5($this->input->post('password', TRUE)) 
        ); 
        $this->Admin_md->update('admin',$this->session->userdata('user')->id , $data);
        
        $this->session->set_flashdata('message', 'Update Record Success'); 
        redirect(base_url('Admin')); 
    }

    public function referrals(){
        $referrals = $this->Admin_md->get_all('referrals');
        $data = [
            'referrals'=>$referrals
        ];
        $this->load->template('referral',$data);
    }
    public function referralpay($id){
        $data = [
            'id'=>$id
        ];
        $this->load->template('referralpay',$data);
    }


    public function payaction(){
        $id = $this->input->post('id', TRUE);
        $this->_rulese(); 
        if ($this->form_validation->run() == FALSE) { 
            $this->referralpay($id); 
        }else{ 
            $id = $this->input->post('id', TRUE);
            $referrals = $this->Admin_md->get_referral_by_id($id);
            $totalamout = $referrals->code * 70;
            $payment = $this->Admin_md->bal_payment($id);
                if(!empty($payment)){
                    $amout = $payment->amout;
                }else{
                    $amout = 0;
                }
            $totalamout -= $amout;
            $amout1 = $this->input->post('name', TRUE);
                if($totalamout < $amout1){
                    $this->session->set_flashdata('message', 'Amount cannot Greater Than Total Amount'); 
                    $this->referralpay($id);
                    redirect(base_url('Admin/referralpay/'.$id));  
                }
                $data = array( 
                    'amout' => $this->input->post('name', TRUE) ,  
                    'created_at' => date('Y-m-d H:i:s') , 
                    'reff_id' => $this->input->post('id', TRUE) ,
                ); 
        $this->Admin_md->insert('referral_pay',$data); 
        $this->session->set_flashdata('message', 'Create Record Success'); 
        redirect(base_url('Admin/referrals')); 
        }
    }

    public function shopkeepers(){
        $shopkeepers = $this->Admin_md->get_all('bussines_partner'); 
        $data = [
            'shopkeepers'=>$shopkeepers
        ];
        $this->load->template('shopkeeper',$data);
    }

    public function getreferrals(){
        $referrals = $this->Admin_md->get_all('referrals'); 
        $data = [
            'referrals'=>$referrals
        ];
        $this->load->template('referral1',$data);
    }

    public function Show_shopkeeper($id){
        $shopkeepers = $this->Admin_md->show_shopkeeper($id); 
        $data = [
            'shopkeepers'=>$shopkeepers
        ];
        $this->load->template('shopkeepeeview',$data);
    }
    public function delete_shopkeeper($id){
        $data = [
            'isdelete' =>1
        ];
        $this->Admin_md->update('bussines_partner',$id,$data); 
        $this->Admin_md->custom_update('products',$id,$data,'created_by'); 
        $this->Admin_md->custom_update('appointment_product',$id,$data,'uid'); 
        $this->Admin_md->custom_update('service_contract',$id,$data,'created_by'); 
        $this->session->set_flashdata('message', 'Delete Record Success'); 
         redirect(base_url('Admin/shopkeepers')); 
    }
    public function active_shopkeeper($id){
        $data = [
            'isdelete' =>0
        ];
        $this->Admin_md->update('bussines_partner',$id,$data); 
        $this->Admin_md->custom_update('products',$id,$data,'created_by'); 
        $this->Admin_md->custom_update('appointment_product',$id,$data,'uid'); 
        $this->Admin_md->custom_update('service_contract',$id,$data,'created_by'); 
        $this->session->set_flashdata('message', 'Delete Record Success'); 
         redirect(base_url('Admin/shopkeepers')); 
    }

    public function service_contract(){
        redirect(base_url('Admin/loadRecord'));
    }

    public function loadRecord($rowno=0){
        $search_text = "";
        if($this->input->post('submit') != NULL ){
          $search_text = $this->input->post('search');
          $this->session->set_userdata(array("search"=>$search_text));
        }else{
          if($this->session->userdata('search') != NULL){
            $search_text = $this->session->userdata('search');
          }
        }
        $rowperpage = 10;
        if($rowno != 0){
          $rowno = ($rowno-1) * $rowperpage;
        }
     
        $allcount = $this->Admin_md->getrecordCount($search_text);
    
        $users_record = $this->Admin_md->getData($rowno,$rowperpage,$search_text);
     
        $config['base_url'] = base_url().'Admin/loadRecord';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;
    
        // Initialize
        $this->pagination->initialize($config);
     
        $data['pagination'] = $this->pagination->create_links();
        $data['result'] = $users_record;
        $data['row'] = $rowno;
        $data['search'] = $search_text;
    
        // Load view
        $this->load->template('service_contract',$data);
     
      }

      public function edit_Services($id){
        $Service1 = $this->Admin_md->get_by_id('service_contract',$id);
    
        $service = $this->Admin_md->get_all('service');
    
            $data = array( 
                'services' =>$service,
                'Services'  =>$Service1,
            );
            $this->load->template('service_edit',$data);
        
      }
      public function edit_advertisement($id){
            $advertise = $this->Admin_md->get_by_id('advertise',$id);
                $data = array( 
                    'advertise' =>$advertise,
                );
                    $this->load->template('advertise_edit',$data);
        
      }

      public function update_advertise_action($id){
        if($this->session->userdata('user')){
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            if(!empty($_FILES['fname']['name'])){
                $count = count($_FILES['fname']['size']);
                for($s=0; $s<=$count-1; $s++) {
                    if (empty($_FILES['fname']['name'][$s])){
                        $this->form_validation->set_rules('fname', 'Image', 'required');
                    } 
                }
            }
            if ($this->form_validation->run() == FALSE) {
                $this->edit_advertisement(); 
            }else{	
                $uid =$this->session->userdata('user')->id;
                // $insertUserData = $this->Mymodel->insert('service_contract',$userData);
                // $last_id = $this->db->insert_id();
                if(!empty($_FILES['fname']['name'])){
                    //$config['upload_path'] = $image_path; 
                    $config['upload_path'] = 'assets/images/'; 
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $config['max_size']             = 4800;
                    $this->load->library('upload', $config);
                    if($this->upload->do_upload('fname')){
                        $fileData = $this->upload->data();
                        $name = $fileData['file_name'];
                        $userData = array(
                            'name' => $this->input->post('name'),
                            // 'service' => $this->input->post('service'),
                            'discription' => $this->input->post('description'),
                            'status' => 0,
                            'created_at' => date('Y-m-d H:i:s'),
                            'img_name' =>$name
                            );
                       // $insertUserData = $this->Mymodel->insert('advertise',$userData);
                        $insertUserData = $this->Admin_md->update('advertise',$id,$userData);
                    }else{
                        $error = array('error' => $this->upload->display_errors());
                        print_r($error);
                    }
                }else{
                    $userData = array(
                        'name' => $this->input->post('name'),
                        // 'service' => $this->input->post('service'),
                        'discription' => $this->input->post('description'),
                        'status' => 0,
                        );
                    $insertUserData = $this->Admin_md->update('advertise',$id,$userData);
                }
            
                $this->session->set_flashdata('message', 'Record added successfully');
                redirect(base_url('Admin/advertisement'));
            
            }
        }	
    }


    public function update_Services($id){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('pries', 'Pries', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('service', 'Service', 'required');
        
        
        if ($this->form_validation->run() == FALSE){
            $this->edit_Services($id);

        }
        else{
            $userData = array(
                'name' => $this->input->post('name'),
                'pries' => $this->input->post('pries'),
                'service' => $this->input->post('service'),
                'discription' => $this->input->post('description'),
                'share' => $this->input->post('share'),
                'showpries' => $this->input->post('showpries'),
                'timesservice' => $this->input->post('timesservice'),
                'packtype' => $this->input->post('packtype'),
                
                );
            $insertUserData = $this->Admin_md->update('service_contract',$id,$userData);
    
            if(!empty($_FILES['fname']['name'])){
                $count = count($_FILES['fname']['size']);
                    foreach($_FILES as $key=>$value){
                        for($s=0; $s<=$count-1; $s++) {
                            $_FILES['fname']['name']     = $value['name'][$s];
                            $_FILES['fname']['type']     = $value['type'][$s];
                            $_FILES['fname']['tmp_name'] = $value['tmp_name'][$s];
                            $_FILES['fname']['error']    = $value['error'][$s];
                            $_FILES['fname']['size']     = $value['size'][$s]; 
                            $image_path = realpath(APPPATH . '../assets/images/');
                            //$config['upload_path'] = $image_path; 
                            $config['upload_path'] = 'assets/images/'; 
                            $config['allowed_types']        = 'gif|jpg|png|jpeg';
                            $config['max_size']             = 4800;
                            // $config['max_width']            = 1024;
                            // $config['max_height']           = 768;
                            $this->load->library('upload', $config);
                            if($this->upload->do_upload('fname')){
                                // Uploaded file data
                                $fileData = $this->upload->data();
                                $name = $fileData['file_name'];
                              
                                $upload_data = $this->upload->data();
                                $data['filename'] = $upload_data['file_name'];
                                //print_r($data['filename']);exit();
                                $this->resize_image($data['filename']);
                               
                                $userData = array(
                                    'img_name' => $name,
                                    'service_contract_id' =>$last_id
                                    );
                            $this->Mymodel->insert('service_contract_img',$userData);
                            } else{
                                 $error = array('error' => $this->upload->display_errors());
                                print_r($error);
                            }
                           
                        }
                    }
                    $this->session->set_flashdata('message', 'Record Update successfully');
                    redirect(base_url('Admin/service_contract'));
                
            }
             $this->session->set_flashdata('message', 'Record Update successfully');
             redirect(base_url('Admin/service_contract'));
           
            
        }     
    }

    public function delete_Services($id){
        
        $this->Admin_md->delete('service_contract',$id);
        redirect(base_url('Admin/service_contract'));
    }

    public function list_pending_product(){
        redirect(base_url('Admin/loadRecord1'));
    }
   

    public function loadRecord1($rowno=0){
        $search_text = "";
        if($this->input->post('submit') != NULL ){
          $search_text = $this->input->post('search');
          $this->session->set_userdata(array("search"=>$search_text));
        }else{
          if($this->session->userdata('search') != NULL){
            $search_text = $this->session->userdata('search');
          }
        }
        $rowperpage = 10;
        if($rowno != 0){
          $rowno = ($rowno-1) * $rowperpage;
        }
     
        $allcount = $this->Admin_md->getrecordCount1($search_text);
    
        $users_record = $this->Admin_md->getData1($rowno,$rowperpage,$search_text);
     
        $config['base_url'] = base_url().'Admin/loadRecord1';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;
    
        // Initialize
        $this->pagination->initialize($config);
     
        $data['pagination'] = $this->pagination->create_links();
        $data['result'] = $users_record;
        $data['row'] = $rowno;
        $data['search'] = $search_text;
    
        // Load view
        $this->load->template('service_contract_pending',$data);
     
      }

      public function view_Services($id){
        $data = [];
        $images = $this->db->get_where('service_contract_img', array('service_contract_id' => $id))->result();
        $product = $this->db->get_where('service_contract', array('id' => $id))->row();
        //$product = $this->Product_md->get_by_id('products',$id);  
        $data = [
            'images' =>$images,
            'product' =>$product,
        ];
        $this->load->template('viewproduct',$data);
    }
      public function view_advertisement($id){
        $data = [];
        $advertise = $this->db->get_where('advertise', array('id' => $id))->row();
        //$product = $this->Product_md->get_by_id('products',$id);  
        $data = [
            'advertise' =>$advertise,
        ];
        $this->load->template('viewadvertise',$data);
    }
    public function active() 
        { 
            $id  = $this->input->post('id');
            $data = array( 
                'status' => 1    
            ); 
            $this->Admin_md->update('service_contract',$id, $data); 
            $this->session->set_flashdata('message', 'Update Record Success'); 
            redirect(base_url('Admin/service_contract')); 
            
        }

    public function add_remark(){
        $id = $this->input->post('id');
        $userData = array(
            'remark' => $this->input->post('remark'),
            
            );
            $insertUserData = $this->Admin_md->update('service_contract',$id,$userData);
            redirect(base_url('Admin/service_contract'));  
    }

    public function advertisement(){
        redirect(base_url('Admin/loadRecord2'));
    }

    public function loadRecord2($rowno=0){
        $search_text = "";
        if($this->input->post('submit') != NULL ){
          $search_text = $this->input->post('search');
          $this->session->set_userdata(array("search"=>$search_text));
        }else{
          if($this->session->userdata('search') != NULL){
            $search_text = $this->session->userdata('search');
          }
        }
        $rowperpage = 10;
        if($rowno != 0){
          $rowno = ($rowno-1) * $rowperpage;
        }
     
        $allcount = $this->Admin_md->getrecordCount3($search_text);
    
        $users_record = $this->Admin_md->getData3($rowno,$rowperpage,$search_text);
     
        $config['base_url'] = base_url().'Admin/loadRecord2';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;
    
        // Initialize
        $this->pagination->initialize($config);
     
        $data['pagination'] = $this->pagination->create_links();
        $data['result'] = $users_record;
        $data['row'] = $rowno;
        $data['search'] = $search_text;
    
        // Load view
        $this->load->template('advertisement',$data);
     
      }
    public function list_pending_advertisement(){
        redirect(base_url('Admin/loadRecord3'));
    }

    public function loadRecord3($rowno=0){
        $search_text = "";
        if($this->input->post('submit') != NULL ){
          $search_text = $this->input->post('search');
          $this->session->set_userdata(array("search"=>$search_text));
        }else{
          if($this->session->userdata('search') != NULL){
            $search_text = $this->session->userdata('search');
          }
        }
        $rowperpage = 10;
        if($rowno != 0){
          $rowno = ($rowno-1) * $rowperpage;
        }
     
        $allcount = $this->Admin_md->getrecordCount2($search_text);
    
        $users_record = $this->Admin_md->getData2($rowno,$rowperpage,$search_text);
     
        $config['base_url'] = base_url().'Admin/loadRecord2';
        $config['use_page_numbers'] = TRUE;
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;
    
        // Initialize
        $this->pagination->initialize($config);
     
        $data['pagination'] = $this->pagination->create_links();
        $data['result'] = $users_record;
        $data['row'] = $rowno;
        $data['search'] = $search_text;
    
        // Load view
        $this->load->template('advertisement_pending',$data);
     
      }
      public function viewreferral($id){
        $profile = $this->Admin_md->referalprofile($id);
		
		$data = [
			'prifile' =>$profile,
			
        ];
        $this->load->template('viewreferral',$data);
      }
}
?>