<?php

class Admin_md extends CI_Model
{
		public $id = 'id'; 
 
		public $order = 'DESC'; 
		function __construct()
		{
			 //    parent::__construct();
		}

public  function login($email)
		{
				$query = $this->db->get_where('admin', array('email' => $email));
				 return $query->row();
		}
function update_service($table,$id, $data) 
		{ 
		$this->db->where($this->id, $id); 
		$this->db->update($table, $data); 
		} 
function get_all($table) 
		{ 
		$this->db->order_by($this->id, $this->order); 
		return $this->db->get($table)->result(); 
} 

function get_by_id($table,$id) 
{ 
$this->db->where($this->id, $id); 
return $this->db->get($table)->row(); 
} 

// insert data 

function insert($table,$data) 
{ 
$this->db->insert($table, $data); 
} 

// update data 

function update($table,$id, $data) 
{ 
$this->db->where($this->id, $id); 
$this->db->update($table, $data); 
} 
function custom_update($table,$id, $data, $fild) 
{ 
$this->db->where($fild, $id); 
$this->db->update($table, $data); 
} 

// delete data 

function delete($table,$id) 
{ 
$this->db->where($this->id, $id); 
$this->db->delete($table); 
}
function get_all_referral() 
{ 
		$this->db->select('referrals.name,referrals.referral_code,referrals.id,count(bussines_partner.referral_code) as code');
		$this->db->from('referrals');
		$this->db->join('bussines_partner ','bussines_partner.referral_code  = referrals.referral_code ', 'left'); 
		$this->db->group_by('bussines_partner.referral_code');
		$query = $this->db->get();

	 // echo $this->db->last_query();die;
		return $query->result();
}

function get_referral_by_id($id) 
{ 
		$this->db->select('referrals.name,referrals.id,count(bussines_partner.referral_code) as code');
		$this->db->from('referrals');
		$this->db->join('bussines_partner ','bussines_partner.referral_code  = referrals.referral_code ', 'left'); 
		$this->db->where('referrals.id', $id); 
		$query = $this->db->get();
		return $query->row();
}

function bal_payment($id) 
{ 
		$this->db->select('sum(amout) as amout');
		$this->db->from('referral_pay'); 
		$this->db->where('reff_id', $id); 
		$query = $this->db->get();
	 // echo $this->db->last_query();die;
		return $query->row();
}

function show_shopkeeper($id){
		$this->db->select('bussines_partner.name, bussines_partner.accountno, bussines_partner.bankname, bussines_partner.holdername, bussines_partner.ifsc, bussines_partner.upi, bussines_partner.latitude, bussines_partner.longitude,bussines_partner.id,bussines_partner.lname,bussines_partner.email,bussines_partner.phone,bussines_partner.gender,bussines_partner.city,bussines_partner.shopno,bussines_partner.shopname,bussines_partner.shopadd,bussines_partner.pin,bussines_partner.img1,bussines_partner.img2,bussines_partner.img3,bussines_partner.img4,bussines_partner.aria,business.name as bussines_module,service.name as bussines_type, s1.name as bussines_type1,s2.name as bussines_type2');
		$this->db->from('bussines_partner');
		$this->db->join('business ','bussines_partner.bussines_module  = business.id ', 'left'); 
		$this->db->join('service ','bussines_partner.bussines_type  = service.id ', 'left'); 
		$this->db->join('service as s1 ','bussines_partner.bussines_type1  = s1.id ', 'left'); 
		$this->db->join('service as s2','bussines_partner.bussines_type2  = s2.id ', 'left'); 
		$this->db->where('bussines_partner.id', $id); 
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->row();
}

public function getData($rowno,$rowperpage,$search="") {
		$this->db->select('*');
		$this->db->from('service_contract');
		if($search != ''){
				$this->db->like('name', $search);
				$this->db->or_like('discription', $search);
				$this->db->or_like('packtype', $search);
		}
		$this->db->where('status',1);
		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
}

	// Select total records
public function getrecordCount($search = '') {
		$this->db->select('count(*) as allcount');
		$this->db->from('service_contract');
 
		if($search != ''){
			$this->db->like('name', $search);
			$this->db->or_like('discription', $search);
			$this->db->or_like('packtype', $search);
		}
		$this->db->where('status',1);
		$query = $this->db->get();
		$result = $query->result_array();
 
		return $result[0]['allcount'];
	}
public function getData1($rowno,$rowperpage,$search="") {
		$this->db->select('*');
		$this->db->from('service_contract');
		if($search != ''){
				$this->db->like('name', $search);
				$this->db->or_like('discription', $search);
				$this->db->or_like('packtype', $search);
		}
		$this->db->where('status',0);
		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
}

	// Select total records
public function getrecordCount1($search = '') {
		$this->db->select('count(*) as allcount');
		$this->db->from('service_contract');
 
		if($search != ''){
			$this->db->like('name', $search);
			$this->db->or_like('discription', $search);
			$this->db->or_like('packtype', $search);
		}
		$this->db->where('status',0);
		$query = $this->db->get();
		$result = $query->result_array();
 
		return $result[0]['allcount'];
	}
public function getData2($rowno,$rowperpage,$search="") {
		$this->db->select('*');
		$this->db->from('advertise');
		if($search != ''){
				$this->db->like('name', $search);
				$this->db->or_like('discription', $search);
				
		}
		$this->db->where('status',0);
		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
}

	// Select total records
public function getrecordCount2($search = '') {
		$this->db->select('count(*) as allcount');
		$this->db->from('advertise');
 
		if($search != ''){
			$this->db->like('name', $search);
			$this->db->or_like('discription', $search);
			
		}
		$this->db->where('status',0);
		$query = $this->db->get();
		$result = $query->result_array();
 
		return $result[0]['allcount'];
	}
public function getData3($rowno,$rowperpage,$search="") {
		$this->db->select('*');
		$this->db->from('advertise');
		if($search != ''){
				$this->db->like('name', $search);
				$this->db->or_like('discription', $search);
				
		}
		$this->db->where('status',1);
		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
}

	// Select total records
public function getrecordCount3($search = '') {
		$this->db->select('count(*) as allcount');
		$this->db->from('advertise');
 
		if($search != ''){
			$this->db->like('name', $search);
			$this->db->or_like('discription', $search);
			
		}
		$this->db->where('status',1);
		$query = $this->db->get();
		$result = $query->result_array();
 
		return $result[0]['allcount'];
	}


public function referalprofile($id){
	$this->db->select('*');
	$this->db->from('referrals');
	$this->db->where('id',$id);
	$prevQuery = $this->db->get();
	return $prevQuery->row();
}
}

?>