<script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>
    <style> 
        body { 
            padding: 15px; 
        } 

       
    </style> 
</head> 
<body> 
    <h2 style="margin-top:0px">Blog <?php echo $button ?></h2> 
    <form action="<?php echo $action; ?>" method="post"> 
        <div class="form-group"> 
            <label for="varchar">Blog Heading 
                <?php echo form_error('heading') ?> 
            </label> 
            <input type="text" class="form-control" name="heading"  placeholder="Heading" value="<?php echo $heading; ?>" required/> 
        </div> 
        <div class="form-group"> 
            <label for="varchar">Body 
                <?php echo form_error('body') ?> 
            </label> 
            <textarea name="body" id="editor1" class="form-control" cols="30" rows="5"><?php echo $body?></textarea>
           
        </div>
        <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
        <input type="hidden" name="date" id ="date"value="" /> 
        <button type="submit" class="btn btn-primary"> 
            <?php echo $button ?> 
        </button> 
        <a href="<?php echo base_url('Admin/Blog') ?>" class="btn btn-default">Cancel</a> 
    </form> 
<script>
CKEDITOR.replace('editor1', {
      fullPage: true,
      extraPlugins: 'docprops',
      // Disable content filtering because if you use full page mode, you probably
      // want to  freely enter any HTML content in source mode without any limitations.
      allowedContent: true,
      height: 320
    });

var d = new Date();
var strDate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate() +" "+ d.getHours()+":"+ d.getMinutes();
$("#date").val(strDate);
</script>