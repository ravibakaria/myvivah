
<style>
.error{
	color: red;
}
</style>


<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<!-- ============================ COMPONENT REGISTER   ================================= -->
	<div class="card mx-auto" style="max-width:520px; margin-top:40px;">
       
      <article class="card-body">
		<header class="mb-4"><h4 class="card-title">Profile</h4></header>
		
				<div class="form-row">
					<div class="col form-group">
						<label>First name</label>
					  	<input type="text" class="form-control" placeholder="" name = "name" value = "<?php echo $prifile->name;?>">
					</div> <!-- form-group end.// -->
					<div class="col form-group">
						<label>Last name</label>
					  	<input type="text" class="form-control" placeholder="" name ="lname"  value = "<?php echo $prifile->lname;?>">
					</div> <!-- form-group end.// -->
				</div> <!-- form-row end.// -->
				<div class="form-group">
					<label>Email</label>
					<input type="email" class="form-control" placeholder="" name ="email"  value = "<?php echo $prifile->email;?>" readonly>
					<small class="form-text text-muted">We'll never share your email with anyone else.</small>
				</div> <!-- form-group end.// -->
                <div class="form-group">
					<label>Mobile No</label>
					<input type="number" class="form-control" placeholder="" name = "phone"  value = "<?php echo $prifile->phone;?>">
					<small class="form-text text-muted">We'll never share your Mobile No. with anyone else.</small>
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label class="custom-control custom-radio custom-control-inline">
					  <input class="custom-control-input" <?php if($prifile->gender =="Male"){echo "checked";}?> type="radio" name="gender" value="Male">
					  <span class="custom-control-label"> Male </span>
					</label>
					<label class="custom-control custom-radio custom-control-inline">
					  <input class="custom-control-input" type="radio" name="gender" value="Female" <?php if($prifile->gender =="Female"){echo "checked";}?>>
					  <span class="custom-control-label"> Female </span>
					</label>
				</div> <!-- form-group end.// -->
				<div class="form-row">
					<div class="form-group col-md-6">
					  <label>City</label>
					  <input type="text" class="form-control" name ="city" value = "<?php echo $prifile->city;?>">
					</div> <!-- form-group end.// -->
					<div class="form-group col-md-6">
					  <label>Country</label>
					  <select id="inputState" class="form-control" name="country">
					    <option> Choose...</option>
					      <option selected="">INDIA</option>
					  </select>
					</div> <!-- form-group end.// -->
				</div> <!-- form-row.// -->
				
				
				<div class="form-group">
					<label>Pin code</label>
					<input type="text" class="form-control" placeholder="" name = "pin" minlength="6"
					maxlength="6" value = "<?php echo $prifile->pin;?>">
				</div> <!-- form-group end.// -->

				<header class="mb-4"><h4 class="card-title">Bank Details</h4></header>

				<div class="form-group">
					<label>Bank Account Holder Name</label>
					<input type="text" class="form-control" placeholder="" name = "holdername"  value = "<?php echo $prifile->holdername;?>">
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>Bank Name</label>
					<input type="text" class="form-control" placeholder="" name = "bankname" value = "<?php echo $prifile->bankname;?>">
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>Account No.</label>
					<input type="text" class="form-control" placeholder="" name = "accountno" value = "<?php echo $prifile->accountno;?>">
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>IFSC Code</label>
					<input type="text" class="form-control" placeholder="" name = "ifsc"  value = "<?php echo $prifile->ifsc;?>">
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>Google Pay Number Or UPI</label>
					<input type="text" class="form-control" placeholder="" name = "upi" value = "<?php echo $prifile->upi;?>">
				</div> <!-- form-group end.// -->
			    
			              
			
		</article><!-- card-body.// -->
    </div> <!-- card .// -->
   
    <br><br>
<!-- ============================ COMPONENT REGISTER  END.// ================================= -->


</section>
<!-- ========================= SECTION CONTENT END// ========================= -->
