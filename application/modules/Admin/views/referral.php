<!doctype html> 
<html> 
<head> 
  
    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> -->


<!--Data Table-->


<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
    <style> 
        body { 
            padding: 15px; 
        } 
    </style> 
</head> 
<body> 
    <div class="row" style="margin-bottom: 10px"> 
        <div class="col-md-4"> 
            <h2 style="margin-top:0px">Referrals List</h2> 
        </div> 
         
    </div> 
    <table class="table table-bordered table-striped" id="mytable"> 
        <thead> 
            <tr> 
                <th width="80px">No</th> 
                <th>Name</th> 
                <th>A/C No.</th> 
                <th>Total Amount</th> 
                <th>Balance Amount</th> 
                <th>Payment</th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php 
            $start = 0; 
            foreach ($referrals as $service) 
            { 
                ?> 
                <tr> 
                    <td> 
                        <?php echo ++$start ?> 
                    </td> 
                    <td> 
                        <?php echo $service->name.' '.$service->lname ?> 
                    </td>  
                    <td> 
                        RF<?php echo $service->referral_code ?> 
                    </td>  
                    <td> 
                        <?php 
                        $this->db->where('referral_code',$service->referral_code);
                        $this->db->where('status',1);
                        $query = $this->db->get('bussines_partner')->result();;
                        $totalamout = count($query) * 70; 
                        echo $totalamout;
                        ?> 
                    </td>  
                    <td> 
                        <?php 
                            //$totalamout = $service->code * 70;
                            $payment = $this->Admin_md->bal_payment($service->id);
                            if(!empty($payment)){
                                $amout = $payment->amout;
                            }else{
                                $amout = 0;
                            }
                        $totalamout -= $amout;
                        echo $totalamout ?> 
                    </td>  
                    
                    <td style="text-align:center" width="200px">
                    <a href = "<?php echo base_url('Admin/referralpay/'.$service->id);?>" class="btn btn-primary"> Pay Now</a> 
                        
                    </td> 
                </tr> 
                <?php 
            } 
            ?> 
        </tbody> 
    </table> 
    <script type="text/javascript"> 
        $(document).ready(function() { 
            $("#mytable").dataTable(); 
        }); 
    </script> 
</body> 
</html>