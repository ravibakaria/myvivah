<style> 
        body { 
            padding: 15px; 
        } 
    </style> 
<section id="posts">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card text-center bg-primary text-white mb-3">
                    <div class="card-body">
                        <h3>Shopkeepers</h3>
                        <h4 class="display-4"><i class="fas fa-user"></i>
                            <span id=""><?php echo $shopkeepers?></span>
                        </h4>
                        <a href ="<?php echo base_url('Admin/shopkeepers')?>" class="btn btn-outline-light btn-sm">View</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card text-center bg-warning text-white mb-3">
                    <div class="card-body">
                        <h3>Referrals</h3>
                        <h4 class="display-4"><i class="fas fa-user"></i>
                            <span id=""><?php echo $referrals?></span>
                        </h4>
                        <a href ="<?php echo base_url('Admin/getreferrals')?>" class="btn btn-outline-light btn-sm">View</a>
                    </div>
                </div>
            </div>
    </div>
</section>