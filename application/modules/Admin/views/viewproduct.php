<style>
.gallery-wrap .img-big-wrap {
    margin-bottom: 10px;
    border-radius: 0.37rem;
    overflow: hidden;
    background-color: #fff;
}
img .big{
    height: 480px;
}
</style>
<!-- ============================ ITEM DETAIL ======================== -->
<div class="row " style = "margin:15px" >
		<aside class="col-md-6">
<div class="card">
<article class="gallery-wrap"> 
	<div class="img-big-wrap">
	  <div class = "showimg"> <img src="<?php echo base_url('assets/images/'.$images[0]->img_name)?>"style ="height: 400px"></div>
	</div> <!-- slider-product.// -->
	<div class="thumbs-wrap">
		<?php 
			if(!empty($images)){
                $i = 1;
                foreach($images as $img){?>
					<a  class="item-thumb getimg" dataid="<?php echo $img->img_name?>"> 
						<img src="<?php echo base_url('assets/images/thumbnails/'.$img->img_name)?>" class = "img img-thumbnail">
					</a>
					<!-- <a href="#" class="item-thumb"> <img src="<?php echo base_url('assets/items/15-1.jpg')?>"></a>
					<a href="#" class="item-thumb"> <img src="<?php echo base_url('assets/items/15-2.jpg')?>"></a>
					<a href="#" class="item-thumb"> <img src="<?php echo base_url('assets/items/15-1.jpg')?>"></a> -->
					<?php
				}
			}
		?>
	</div> <!-- slider-nav.// -->
</article> <!-- gallery-wrap .end// -->
</div> <!-- card.// -->
		</aside>
		<main class="col-md-6">
<article class="product-info-aside">

<h2 class="title mt-3"><?php echo ucfirst($product->name) ;?></h2>

<!-- <div class="rating-wrap my-3">
	<ul class="rating-stars">
		<li style="width:80%" class="stars-active"> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> 
		</li>
		<li>
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> 
		</li>
	</ul>
	<small class="label-rating text-muted">132 reviews</small>
	<small class="label-rating text-success"> <i class="fa fa-clipboard-check"></i> 154 orders </small>
</div>  -->

<div class="mb-3"> 
    <var class="price h4"> MRP.&#x20B9; <?php echo $product->showpries ;?></var> <br>
	<var class="price h4">&#x20B9; <?php echo $product->pries ;?></var> 
	<span class="text-muted">&#x20B9; <?php echo $product->pries ;?> incl. VAT</span> 
</div> <!-- price-detail-wrap .// -->

<p><?php echo $product->discription ;?></p>


<dl class="row">
<?php if($product->packtype != ''){?>
  <dt class="col-sm-3">Packthyt Type</dt>
  <dd class="col-sm-9"><?php echo $product->packtype ?></dd>
<?php } ?>
<?php if($product->timesservice !=''){?>
  <dt class="col-sm-3">Times Service</dt>
  <dd class="col-sm-9"><?php echo $product->timesservice?></dd>
</dl>
<?php } ?>
<form method = "post" action="<?= base_url().'Admin/active'?>">
   
   <input type="hidden" name="id" value= "<?= $product->id?>">
   <br>
   <input type = "submit" class="btn btn-warning" value = "Active">
</form>
<div class="form-row  mt-4">
		<form method = "post" action="<?= base_url().'Admin/add_remark'?>">
			
			<input type="hidden" name="id" value= "<?= $product->id?>">
			
		
		<div class="form-group col-md">
        <textarea class="form-control"  name="remark" cols="30" rows="3" placeholder = "Enter Remark On Product"><?php if($product->remark !=''){
            echo  $product->remark;
        }?></textarea>
           
		</div>
		<div class="form-group col-md">
		
			<button class="btn btn-primary" type = "submit"> 
				<span class="text">Add Remark</span> 
			</button>
		</div> <!-- col.// -->
	</form>
   
</div>


</article> <!-- product-info-aside .// -->
		</main> <!-- col.// -->
	</div> <!-- row.// -->

<!-- ================ ITEM DETAIL END .// ================= -->
<style>
    .btn-primary {
  color: #fff !important;
  background-color: #ff6a00 !important;
  border-color: #ff6a00 !important;
}
</style>

<script>
	$('.getimg').click(function () {
		
		var img = $(this).attr('dataid');
		var src = "<?php echo base_url('assets/images/')?>";
		var html = '<img src="'+src+img+'" style ="height: 400px">';
		$(".showimg").html(html);
	});
</script>
