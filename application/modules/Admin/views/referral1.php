
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
    <style> 
        body { 
            padding: 15px; 
        } 
    </style> 
</head> 
<body> 
    <div class="row" style="margin-bottom: 10px"> 
        <div class="col-md-4"> 
            <h2 style="margin-top:0px">Referrals List</h2> 
        </div> 
         
    </div> 
    <table class="table table-bordered table-striped" id="mytable"> 
        <thead> 
            <tr> 
                <th width="80px">No</th> 
                <th>Name</th> 
                <th>A/C No.</th> 
                <th>Phone</th> 
                <th>Email</th> 
                <th>City</th> 
                <th>Action</th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php 
            $start = 0; 
            foreach ($referrals as $service) 
            { 
                ?> 
                <tr> 
                    <td> 
                        <?php echo ++$start ?> 
                    </td> 
                    <td> 
                        <?php echo $service->name.' '.$service->lname ?> 
                    </td>  
                    <td> 
                        RF<?php echo $service->referral_code ?> 
                    </td>  
                    <td> 
                        <?php echo $service->phone; ?> 
                    </td>  
                    <td> 
                        <?php  echo $service->email ?> 
                    </td>  
                    
                    <td>
                    <?php  echo $service->city ?> 
                        
                    </td> 
                    <td>
                    <a href="<?php echo base_url('Admin/viewreferral/').$service->id?>"> View</a> |                      
                    <a href='<?php echo base_url()."Admin/delete_referral/".$service->id;?>' class='btn btn-primary' onclick="return confirm('Are You Sure ?')">Delete</a>                       
                    </td> 
                </tr> 
                <?php 
            } 
            ?> 
        </tbody> 
    </table> 
    <script type="text/javascript"> 
        $(document).ready(function() { 
            $("#mytable").dataTable(); 
        }); 
    </script> 
</body> 
</html>