<?php

class Mymodel extends CI_Model
{
    public $id = 'id'; 
 
    public $order = 'DESC'; 
    function __construct()
    {
          // parent::__construct();
           $this->column_order = array(null, 'name','invoice_no','total_amt', 'date','status');
           $this->column_order1 = array(null, 'name','pries','description','status');
           // Set searchable column fields
          $this->column_search = array('name','invoice_no','date','total_amt');
          $this->column_search1 = array('name','pries','description','status');
           // Set default order
           $this->order = array('name' => 'asc');
    }



function get_all_active($table) 
    { 
    $this->db->where('status', 1); 
    $this->db->order_by($this->id, 'DESC'); 
    return $this->db->get($table)->result(); 
} 

function get_by_id($table,$id) 
{ 
$this->db->where($this->id, $id); 
return $this->db->get($table)->row(); 
} 

}