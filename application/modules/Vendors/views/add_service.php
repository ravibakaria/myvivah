 
    <style> 
        body { 
            padding: 15px; 
        } 
    </style> 
</head> 
<body> 
    <h2 style="margin-top:0px">Business Type <?php echo $button ?></h2> 
    <form action="<?php echo $action; ?>" method="post"> 
        <div class="form-group"> 
            <label for="varchar"><p class="font-weight-bold">Name </p>
                <?php echo form_error('name') ?> 
            </label> 
            <input type="text" class="form-control" name="name" id="job_position" placeholder="name" value="<?php echo $name; ?>" /> 
        </div>  
       
    <p class="font-weight-bold">Business Modules</p>
        <?php echo form_error('access') ?> </label>
    <ul class="list-group list-group-flush">
        <?php $i = 1;
            foreach($business as $b ){ ?>
                <li class="list-group-item">
                <div class="custom-control custom-checkbox">
                    <?php 
                        $checked ="";
                        if(!empty($bm)){
                            foreach($bm as $m){
                                if($b->id == $m->bussines_id){
                                    $checked ="checked";
                                }
                            }
                        }
                    ?>
                    <input type="checkbox" class="custom-control-input" id="check<?php echo $i;?>"  name = 'bmodules[]' value="<?php echo $b->id?>" <?php echo "$checked";?> >
                    <label class="custom-control-label" for="check<?php echo $i;?>"><?php echo $b->name?></label>
                </div>
                </li>
            <?php $i++;
            }
        ?>
    </ul>
<input type="hidden" name="id" value="<?php echo $id; ?>" /> 
        <button type="submit" class="btn btn-primary"> 
            <?php echo $button ?> 
        </button> 
        <a href="<?php echo base_url('Vendors/service') ?>" class="btn btn-default">Cancel</a> 
    </form> 