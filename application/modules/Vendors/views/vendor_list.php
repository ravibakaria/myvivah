<!doctype html> 
<html> 
<head> 

    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script> -->


<!--Data Table-->


<!--Export table button CSS-->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
    <style> 
        body { 
            padding: 15px; 
        } 
    </style> 
</head> 
<body> 
    <div class="row" style="margin-bottom: 10px"> 
        <div class="col-md-4"> 
            <h2 style="margin-top:0px">Staff List</h2> 
        </div> 
        <div class="col-md-4 text-center"> 
            <div style="margin-top: 4px" id="message"> 
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?> 
            </div> 
        </div> 
        <div class="col-md-4 text-right"> 
            <?php echo anchor(base_url('Vendors/Vendors/create'), 'Create', 'class="btn btn-primary"'); ?> 
        </div> 
    </div> 
    <table class="table table-bordered table-striped" id="mytable"> 
        <thead> 
            <tr> 
                <th width="80px">No</th> 
                <th>Name</th> 
                <th>Email</th> 
                <th>Phone</th> 
                <th>Role</th> 
                <th>Status</th>
                <th>Added On</th> 
                <th>Action</th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php 
            $start = 0; 
            foreach ($vendor_data as $vender) 
            { 
                ?> 
                <tr> 
                    <td> 
                        <?php echo ++$start ?> 
                    </td> 
                    <td> 
                        <?php echo $vender->name ?> 
                    </td> 
                    <td> 
                        <?php echo $vender->email ?> 
                    </td> 
                    <td> 
                        <?php echo $vender->phone ?> 
                    </td>
                    <td> 
                        <b><?php echo $vender->rolename ?> </b>
                    </td>
                    <td> 
                        <?php if( $vender->status  == 1){
                            echo "Active";
                        }else{
                            echo "InActive";
                        }?> 
                    </td> 
                    <td> 
                        <?php echo $vender->created_at ?> 
                    </td> 
                    <td style="text-align:center" width="200px"> 
                        <?php  
            echo anchor(base_url('Vendors/Vendors/update/'.$vender->id),'Update');  
            echo ' | ';  
            echo anchor(base_url('Vendors/Vendors/delete/'.$vender->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"');  
            ?> 
                    </td> 
                </tr> 
                <?php 
            } 
            ?> 
        </tbody> 
    </table> 
    <script type="text/javascript"> 
        $(document).ready(function() { 
            $("#mytable").dataTable(); 
        }); 
    </script> 
</body> 
</html>