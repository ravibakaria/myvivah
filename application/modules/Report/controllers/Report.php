<?php

class Report extends MX_Controller
{
    function __construct()
    {
        $this->load->model('Report_Md');
        $this->load->library('form_validation');
        $this->load->library('session');
        parent::__construct();
    }

    public function index(){
    
    		//$payments = $this->db->get_where('payments',['STATUS'=>'TXN_SUCCESS']);
    		 $lastday =  date('Y-m-d', strtotime('last day of previous month'));
    		 $firstday =  date('Y-m-d', strtotime('first day of last month'));
    		 $this->db->select("sum(TXNAMOUNT) amt,DATE_FORMAT(TXNDATE, '%Y-%m-%d') orderDate");
    		 $this->db->where('TXNDATE BETWEEN ADDDATE(NOW(),-7) AND NOW()');
    		 $this->db->where('STATUS','TXN_SUCCESS');
    		 $this->db->group_by("orderDate");
    		$data['payments'] =  $this->db->get('payments')->result();
    		// echo "<pre>";
    		// print_r($data);exit();
    		//echo $this->db->last_query();exit();
           // $this->load->view('admin/report/report.php', $data);
            $this->load->Template('report.php',$data);
    }
    public function getsale(){
       	$var = $this->input->post('to');
    	$chart = $this->input->post('bar');
		$date1 = str_replace('/', '-', $var);
		$var = $this->input->post('from');
		$date2 = str_replace('/', '-', $var);
		$to = date('Y-m-d', strtotime($date1));
		$from = date('Y-m-d', strtotime($date2));
		//$data['to'] = $to;
		//$data['from'] = $to;
		$this->db->select("sum(TXNAMOUNT) amt,DATE_FORMAT(TXNDATE, '%Y-%m-%d') orderDate");
    		 $this->db->where('TXNDATE BETWEEN "'. date('Y-m-d H:i:s', strtotime($to)). '" and "'. date('Y-m-d 23:59:59', strtotime($from)).'"');
    		// $this->db->where('STATUS','TXN_SUCCESS');
    		 $this->db->group_by("orderDate");
    		$data =  $this->db->get('payments')->result();
    		$data1['categories'] = [];
    		$data1['chart'] = $chart;
    		$data1['amt']=[];
    		foreach ($data as $key => $value) {
    			//$days = date('d', strtotime($value->orderDate));
    			$days = $value->orderDate;
    			$data1['categories'][] = $days;

    			$data1['amt'][]= (int)$value->amt;
    		}
    		//echo $this->db->last_query();die;
    		echo json_encode($data1);
    		//$this->load->view('admin/report/ajax_report.php', $data);
    	
		
	}
	
	public function sellar(){

	}

}

?>