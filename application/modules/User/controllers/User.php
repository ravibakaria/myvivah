<?php

class User extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('cart');
        $this->load->model('User_md');
    }

public  function index()
    {
        echo "WELOME TO HERE USER";
    }
public  function product_details($id =''){
    if($id != ''){
        $data = [];
            $images = $this->db->get_where('images', array('product_id' => $id))->result();
            $product = $this->db->get_where('products', array('id' => $id))->row();
            //$product = $this->Product_md->get_by_id('products',$id);  
            $data = [
                'images' =>$images,
                'product' =>$product,
            ];
            // echo "<pre>";
            // print_r($data);die();
        $this->load->userTemplate('product_details',$data);
    }else{
        redirect(base_url('Home'));
    }
    
}

    // public function cart(){
    //     $this->load->userTemplate('cart');
    // }

    public function cart() { 
       if($this->session->userdata('userm') != ''){
            $id = 2;
            $data['itemInfo'] = $this->db->get_where('products', array('id' => $id))->row();
            $data['return_url'] = base_url().'Razorpay/callback';
            $data['surl'] = base_url().'Razorpay/success';
            $data['furl'] = base_url().'Razorpay/failed';
            $data['currency_code'] = 'INR';
            //$this->load->view('razorpay/checkout', $data);
            $this->load->userTemplate('cart',$data);
       }else{
        redirect(base_url('Login'));
       }
    }

    public function add_cart(){
        $id = $this->input->post('id');
        $qty = $this->input->post('qty');
        $data = $this->db->get_where('products', array('id' => $id))->row();
        
        if((int)$data->quantity < (int)$qty){
            $this->session->set_flashdata('message_e', 'You Have Not Get More Than '.$data->quantity.' Products');
            redirect(base_url('Home'));
        }
        $images = $this->db->get_where('images', array('product_id' => $id))->row();
        $data1 = array(
                'id'      => $data->id,
                'qty'     => $qty,
                'price'   => $data->pries,
                'name'    => $data->name,
                'image'   => $images->img_name,
                'Description'   => $data->Description,
                'seller_id'   => $data->created_by
        );   
        $this->cart->insert($data1);
        $this->session->set_flashdata('message', ''.$data->name.' Add On Cart Successfully');
        redirect(base_url());

    }

    public function update_cart(){
        
        $post = $this->input->post(); 
        // echo "<pre>";
        //  print_r($post); die; 
        foreach($post as $p){
            if (isset($p['id'])) {
                $picid = $p['id'];
          }
          if (isset($p['qty'])) {
            $qty = $p['qty'];
      }
            $data = $this->db->get_where('products', array('id' => $picid))->row();
        //echo $this->db->last_query();die;
            if((int)$data->quantity < (int)$qty){
                $this->session->set_flashdata('message_e', 'You Have Not Get More Than '.$data->quantity.' Products');
                redirect(base_url('User/cart'));
             //echo "kum";die;
            }
            
        }
         //  die; 
        if($this->input->post('payment')){
             for ($i=1; $i <= count($post) ; $i++) { 
                if(isset($post[$i]['rowid'])){
                    $data2 ['rowid']=$post[$i]['rowid'];
                    unset($post[$i]['rowid']);
                    unset($post[$i]['id']);
                    $insert = $this->db->insert('orders_items', $post[$i]);
                }
            }
            $this->paytmpost($post);
        }else{
            if(isset($post['cod'])){
                $data1 = $post;
                $this->cart->update($data1);
                if($this->session->userdata('userm')){
                    $data2 = [];
                    //$invoice = array('invoice_no'=>$post['invoice_no']);
                    $merchant_order_id =  rand(10000,99999);
                   // echo $merchant_order_id;die;
                   // $invoice_id = $this->My_Model->insert1('invoices', $merchant_order_id);
                    $this->insert_oder_items1($merchant_order_id);
                    $this->shipingadder($merchant_order_id);
                    $this->session->set_flashdata('message', 'Your Order Has Been Created Successfully');
                    redirect(base_url('Home'));
                }else{
                    redirect(base_url('Login'));
                }
            }else{
                $data = $post;
                // echo "<pre>";
                // print_r($post);die;
                $this->cart->update($data);
                redirect(base_url('User/cart'));
            }
        }
    }
    public function insert_oder_items1($invoice){
       
       
        foreach ($this->cart->contents() as $items){
            unset($items['rowid']);
           $oitem =[
               'u_id' =>$this->session->userdata('userm')->id,
               'p_id' =>$items['id'],
               'qty' =>$items['qty'],
               'price' =>$items['price'],
               'product_name' =>$items['name'],
               'total_amt' =>$items['subtotal'],
               'invoice_no' =>$invoice,
               'paystatus' =>0,
               'seller_id' =>$items['seller_id'],
           ]; 
        //    echo "<pre>";
        //    print_r($oitem); die;get_by_id
            $insert = $this->db->insert('orders_items', $oitem);
            $product = $this->User_md->get_by_id('products',$items['id']);
            $q =  $product->quantity - $items['qty'];
            
            $userData = array(
                'quantity' => $q,
                );
                //print_r($userData);die;
                $insertUserData = $this->User_md->update('products',$items['id'],$userData);
            
        }
        $order = array(
            'u_id'=>$this->session->userdata('userm')->id,
            'invoice_id'=>$invoice,
            'total_amt'=>$this->cart->format_number($this->cart->total()),
            'paymet_status' => 'pending',
            'date' => date('Y-m-d H:i:s')
        );
       // print_r($order);die;
        $this->cart->destroy();
        $this->db->insert('orders', $order);
            
    }
    private function get_curl_handle($payment_id, $amount)  {
        $url = 'https://api.razorpay.com/v1/payments/'.$payment_id.'/capture';
        $key_id = RAZOR_KEY_ID;
        $key_secret = RAZOR_KEY_SECRET;
        $fields_string = "amount=$amount";
        //cURL Request
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $key_id.':'.$key_secret);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
       // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__).'/ca-bundle.crt');
        return $ch;
    }   
        
    // callback method
    public function callback() {        
        if (!empty($this->input->post('razorpay_payment_id')) && !empty($this->input->post('merchant_order_id'))) {
            $razorpay_payment_id = $this->input->post('razorpay_payment_id');
            $merchant_order_id = $this->input->post('merchant_order_id');
            $currency_code = 'INR';
            $amount = $this->input->post('merchant_total');
            $success = false;
            $error = '';
            try {                
                $ch = $this->get_curl_handle($razorpay_payment_id, $amount);
                //execute post
                $result = curl_exec($ch);
                $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if ($result === false) {
                    $success = false;
                    $error = 'Curl error: '.curl_error($ch);
                } else {
                    $response_array = json_decode($result, true);
                  // echo "<pre>";print_r($response_array);exit;

                        //Check success response
                        if ($http_status === 200 and isset($response_array['error']) === false) {
                            $success = true;
                           $this->insert_oder_items($merchant_order_id);
                           $this->shipingadder($merchant_order_id);
                           $this->payment_order($response_array,$merchant_order_id);
                        } else {
                            $success = false;
                            if (!empty($response_array['error']['code'])) {
                                $error = $response_array['error']['code'].':'.$response_array['error']['description'];
                            } else {
                                $error = 'RAZORPAY_ERROR:Invalid Response <br/>'.$result;
                            }
                        }
                }
                //close connection
                curl_close($ch);
            } catch (Exception $e) {
                $success = false;
                $error = 'OPENCART_ERROR:Request to Razorpay Failed';
            }
            if($success === true) {
                if(!empty($this->session->userdata('ci_subscription_keys'))) {
                    $this->session->unset_userdata('ci_subscription_keys');
                 }
                if (!$order_info['order_status_id']) {
                    redirect($this->input->post('merchant_surl_id'));
                } else {
                    redirect($this->input->post('merchant_surl_id'));
                }
 
            } else {
                redirect($this->input->post('merchant_furl_id'));
            }
        } else {
            echo 'An error occured. Contact site administrator, please!';
        }
    } 
    public function success() {
        $this->load->userTemplate('success.php');

        //$this->load->Template('success.php',$data);
    }  
    public function failed() {
        $data['title'] = 'Razorpay Failed | TechArise';            
        echo "payment failed";
        //$this->load->Template('failed.php',$data);
    }

    public function insert_oder_items($invoice){
       
       
        foreach ($this->cart->contents() as $items){
            unset($items['rowid']);
           $oitem =[
               'u_id' =>$this->session->userdata('userm')->id,
               'p_id' =>$items['id'],
               'qty' =>$items['qty'],
               'price' =>$items['price'],
               'product_name' =>$items['name'],
               'total_amt' =>$items['subtotal'],
               'invoice_no' =>$invoice,
               'paystatus' =>1,
               'seller_id' =>$items['seller_id'],
           ];
        //    echo "<pre>";
        //    print_r($oitem); die;
            $insert = $this->db->insert('orders_items', $oitem);
            $product = $this->User_md->get_by_id('products',$items['id']);
            $q =  $product->quantity - $items['qty'];
            
            $userData = array(
                'quantity' => $q,
                );
                //print_r($userData);die;
                $insertUserData = $this->User_md->update('products',$items['id'],$userData);
            
        }
        $order = array(
            'u_id'=>$this->session->userdata('userm')->id,
            'invoice_id'=>$invoice,
            'total_amt'=>$this->cart->format_number($this->cart->total()),
            'paymet_status' => 'done',
            'date' => date('Y-m-d H:i:s')
        );
        $this->cart->destroy();
       // print_r($order);die;
        $this->db->insert('orders', $order);
            
    }
    public function payment_order($data,$invoice){
        $data1 = array(
            'ORDERID'=>$invoice,
            'TXNID'=>$data['id'],
            'TXNAMOUNT'=>$data['amount']/100,
            'PAYMENTMODE'=>$data['method'],
            'TXNDATE'=>$data['created_at'],
            'STATUS'=>$data['status'],
            
            'email'=>$data['email'],
            'contact'=>$data['contact'],
            'BANKNAME'=>$data['bank'],
           
            'DATA'=> serialize($data)
        );
        $this->db->insert('payments', $data1);
    }

    public function removeitom($id){
        //$id = $this->input->post('id'); 
        $data = [
            'rowid' => $id,
            'qty' => 0
        ] ;
        $this->cart->update($data);
         redirect(base_url('User/cart'));
    }

    public function myorder(){
        if($this->session->userdata('userm')!=''){
            $id = $this->session->userdata('userm')->id;
            $order = $this->User_md->get_order_by_id('orders',$id);
            $order1 = $this->User_md->get_order_by_id('events',$id);
            $order2 = $this->User_md->get_order_by_id1('service_contract_order',$id);
            $data = [
                'order' => $order,
                'order1' => $order1,
                'order2' => $order2,
                
            ] ;
            // echo "<pre>";
            // print_r($data);die;
            $this->load->userTemplate('myorder',$data);
        }else{
            redirect(base_url('Home'));
        }
    }
    // public function my_appointment(){
    //     if($this->session->userdata('userm')!=''){
    //         $id = $this->session->userdata('userm')->id;
    //         $order = $this->User_md->get_order_by_id('events',$id);
    //         $data = [
    //             'order' => $order,
                
    //         ] ;
    //         // echo "<pre>";
    //         // print_r($data);die;
    //         $this->load->userTemplate('myorder_app',$data);
    //     }else{
    //         redirect(base_url('Home'));
    //     }
    // }
    // public function contract_services(){
    //     if($this->session->userdata('userm')!=''){
    //         $id = $this->session->userdata('userm')->id;
    //         $order = $this->User_md->get_order_by_id1('service_contract_order',$id);
    //         $data = [
    //             'order' => $order,
                
    //         ] ;
    //         // echo "<pre>";
    //         // print_r($data);die;
    //         $this->load->userTemplate('my_contract_services',$data);
    //     }else{
    //         redirect(base_url('Home'));
    //     }
    // }
    public function vieworder($invoice){
        $id = $this->session->userdata('userm')->id;
        $order = $this->User_md->get_order_by_invoice($invoice,$id);
        $data = [
            'order' => $order,
            
        ] ;
        // echo "<pre>";
        // print_r($data);die;
        $this->load->userTemplate('myorderdetail',$data);
    }
    public function placedorder() {
        if($this->session->userdata('userm') != ''){
             $id = 2;
             $data['itemInfo'] = $this->db->get_where('products', array('id' => $id))->row();
             $data['return_url'] = base_url().'Razorpay/callback';
             $data['surl'] = base_url().'Razorpay/success';
             $data['furl'] = base_url().'Razorpay/failed';
             $data['currency_code'] = 'INR';
             //$this->load->view('razorpay/checkout', $data);
             $this->load->userTemplate('placedorder',$data);
        }else{
         redirect(base_url('Login'));
        }
     }
     public function shiping(){
        $this->load->userTemplate('shipingaddress');
     }

     public function pay(){
        
         $this->session->set_userdata('useradd',$_POST);
         echo $this->session->userdata('useradd')['name'];
         redirect(base_url('User/placedorder'));
     }
     public function shipingadder($invoice){
         if(isset($this->session->userdata('useradd')['bname'])){
             $name = $this->session->userdata('useradd')['bname'];
         }else{
             $name = '';
         }
         if(isset($this->session->userdata('useradd')['blname'])){
             $lname = $this->session->userdata('useradd')['blname'];
         }else{
             $lname = '';
         }
         if(isset($this->session->userdata('useradd')['baddress'])){
             $baddress = $this->session->userdata('useradd')['baddress'];
         }else{
             $baddress = '';
         }
         if(isset($this->session->userdata('useradd')['bphone'])){
             $bphone = $this->session->userdata('useradd')['bphone'];
         }else{
             $bphone = '';
         }
         if(isset($this->session->userdata('useradd')['bpin'])){
             $bpin = $this->session->userdata('useradd')['bpin'];
         }else{
             $bpin = '';
         }
        $oitem =[
            'uid' =>$this->session->userdata('userm')->id,
            'name' =>$this->session->userdata('useradd')['name'],
            'lname' =>$this->session->userdata('useradd')['lname'],
            'address' =>$this->session->userdata('useradd')['add'],
            'pin' =>$this->session->userdata('useradd')['pin'],
            'phone' =>$this->session->userdata('useradd')['phone'],
            'bname' =>$name,
            'blname' =>$lname,
            'baddress' =>$bphone,
            'bpin' =>$bpin,
            'bphone' => $bphone,
            'invoice' => $invoice
        ];
        // echo "<pre>";
        // print_r($oitem); die;
         $insert = $this->db->insert('shipingadd', $oitem);
     }
     public function done(){
        $this->load->userTemplate('success.php');
     }
}

?>