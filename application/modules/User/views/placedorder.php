<style>
	.modal-confirm {		
		color: #636363;
		width: 325px;
	}
	.modal-confirm .modal-content {
		padding: 20px;
		border-radius: 5px;
		border: none;
	}
	.modal-confirm .modal-header {
		border-bottom: none;   
    position: relative;
    text-
	}
	.modal-confirm h4 {
		text-align: center;
		font-size: 26px;
		margin: 30px 0 -15px;
	}
	.modal-confirm .form-control, .modal-confirm .btn {
		min-height: 40px;
		border-radius: 3px; 
	}
	.modal-confirm .close {
        position: absolute;
		top: -5px;
		right: -5px;
	}	
	.modal-confirm .modal-footer {
		border: none;
		text-align: center;
		border-radius: 5px;
		font-size: 13px;
	}	
	.modal-confirm .icon-box {
		color: #fff;		
		position: absolute;
		margin: 0 auto;
		left: 0;
		right: 0;
		top: -70px;
		width: 95px;
		height: 95px;
		border-radius: 50%;
		z-index: 9;
		background: #82ce34;
		padding: 15px;
		text-align: center;
		box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
	}
	.modal-confirm .icon-box i {
		font-size: 58px;
		position: relative;
		top: 3px;
	}
	.modal-confirm.modal-dialog {
		margin-top: 40px;
	}
    .modal-confirm .btn {
        color: #fff;
        border-radius: 4px;
		background: #82ce34;
		text-decoration: none;
		transition: all 0.4s;
        line-height: normal;
        border: none;
    }
	.modal-confirm .btn:hover, .modal-confirm .btn:focus {
		background: #6fb32b;
		outline: none;
	}
	.trigger-btn {
		display: inline-block;
		margin: 100px auto;
	}
</style>

<!-- ========================= SECTION CONTENT ========================= -->
<?php if(count($this->cart->contents()) != 0){?>
<section class="section-content padding-y">
<div class="container">

<div class="row justify-content-md-center">

	<main class="col-md-5">
<form action="<?= base_url().'User/update_cart'?>" method  = "post">
<div class="card">
<div class="table-responsive-sm" style = "display:none">
<table class="table table-borderless table-shopping-cart">

<tbody>
<?php 
$i = 1;
// print_r($this->cart->contents());die;
foreach ($this->cart->contents() as $items): ?>
	<?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>
    <?php echo form_hidden($i.'[price]', $items['price']); ?>
	<?php echo form_hidden($i.'[product_name]', $items['name']); ?>
	<?php echo form_hidden($i.'[seller_id]', $items['seller_id']); ?>
  <?php echo form_hidden($i.'[id]', $items['id']); ?>
  <?php echo form_hidden($i.'[qty]', $items['qty']); ?>
        <?php echo form_hidden($i.'[total_amt]', $this->cart->total()); ?>
		<?php 
			if($this->session->userdata('user')){    
                echo form_hidden($i.'[u_id]', $this->session->userdata('userm')->id);
            } else {
                echo form_hidden($i.'[u_id]', '0');
			}
		?>
<tr>
	<td>
		
	<td> 
		
		<?php echo form_hidden(array('name' => $i.'[qty]', 'value' => $items['qty'], 'maxlength' => '3','class' => 'form-control', 'size' => '5','readonly'=>'readonly')); ?>	
		
	</td>
	<td> 
	
	</td>
	
</tr>
<?php $i++; ?>

<?php endforeach; ?>

</tbody>
</table>
</div>
<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header text-center">
							
				<h4 class="modal-title ">Awesome!</h4>	
			</div>
			<div class="modal-body">
        <dl class="dlist-align"> 
					  <dt>Total:</dt>
					  <dd class="text-right  h5"><strong>&#x20B9;<?php echo $this->cart->format_number($this->cart->total()); ?></strong></dd>
					</dl>
					<hr>
					<p class="text-center mb-3">
						<img src="<?php echo base_url('assets/items/payments.png')?>" height="26">
					</p>
				<p class="text-center">Make Payment For Order.</p>
			</div>
			<div class="modal-footer">
				<!-- <button onclick="razorpaySubmit(this);" class="btn btn-success btn-block" >Pay Now</button> -->
        <a href="#" class="btn btn-success btn-block" onclick="razorpaySubmit(this);"> Proceed to Buy </a>
        <?php echo form_submit(['class'=>'btn btn-primary btn-block','name' =>'cod'], 'COD'); ?>
			</div>
		</div>
	</div>
<div class="card-body text-center">


	
</div>	
</div> <!-- card.// -->
</form>
<!-- <div class="alert alert-success mt-3">
	<p class="icontext"><i class="icon text-success fa fa-truck"></i> Delivery within 1-2 weeks</p>
</div> -->

	</main> <!-- col.// -->
	
</div>

</div> <!-- container .//  -->
</section>
<?php } ?>
<!-- ========================= SECTION CONTENT END// ========================= -->



<?php
if(empty($itemInfo->description)){
  $productinfo = '';
}else{
  $productinfo = $itemInfo->description;
}

$txnid = time();        
$key_id = RAZOR_KEY_ID;
$currency_code = 'INR';            
$total =  (int)$this->cart->total() * 100; 
$amount = $this->cart->format_number($this->cart->total()); 
$merchant_order_id =  rand(10000,99999);

$email = 'info@techarise.com';
$phone = '9000000001';
$name = APPLICATION_NAME;
$return_url = base_url().'User/callback';
$surl = base_url().'User/success';
$furl = base_url().'User/failed';
?>
 <form name="razorpay-form" id="razorpay-form" action="<?php echo $return_url; ?>" method="POST">
  <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" />
  <input type="hidden" name="merchant_order_id" id="merchant_order_id" value="<?php echo $merchant_order_id; ?>"/>
  <input type="hidden" name="merchant_trans_id" id="merchant_trans_id" value="<?php echo $txnid; ?>"/>
  <!-- <input type="hidden" name="merchant_product_info_id" id="merchant_product_info_id" value="<?php echo $productinfo; ?>"/> -->
  <input type="hidden" name="merchant_surl_id" id="merchant_surl_id" value="<?php echo $surl; ?>"/>
  <input type="hidden" name="merchant_furl_id" id="merchant_furl_id" value="<?php echo $furl; ?>"/>
  <!-- <input type="hidden" name="card_holder_name_id" id="card_holder_name_id" value="<?php echo $card_holder_name; ?>"/> -->
  <input type="hidden" name="merchant_total" id="merchant_total" value="<?php echo $total; ?>"/>
  <input type="hidden" name="merchant_amount" id="merchant_amount" value="<?php echo $amount; ?>"/>
</form>
<style>
    .btn-primary {
  color: #fff !important;
  background-color: #ff6a00 !important;
  border-color: #ff6a00 !important;
}
</style>

<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
  var razorpay_options = {
    key: "<?php echo $key_id; ?>",
    amount: "<?php echo $total; ?>",
    name: "<?php echo $name; ?>",
    description: "Order # <?php echo $merchant_order_id; ?>",
    netbanking: true,
    currency: "<?php echo $currency_code; ?>",
    prefill: {
      name:"",
      email: "",
      contact: ""
    },
    notes: {
      soolegal_order_id: "<?php echo $merchant_order_id; ?>",
    },
    handler: function (transaction) {
        document.getElementById('razorpay_payment_id').value = transaction.razorpay_payment_id;
        document.getElementById('razorpay-form').submit();
    },
    "modal": {
        "ondismiss": function(){
            location.reload()
        }
    }
  };
  var razorpay_submit_btn, razorpay_instance;

  function razorpaySubmit(el){
    if(typeof Razorpay == 'undefined'){
      setTimeout(razorpaySubmit, 200);
      if(!razorpay_submit_btn && el){
        razorpay_submit_btn = el;
        el.disabled = true;
        el.value = 'Please wait...';  
      }
    } else {
      if(!razorpay_instance){
        razorpay_instance = new Razorpay(razorpay_options);
        if(razorpay_submit_btn){
          razorpay_submit_btn.disabled = false;
          razorpay_submit_btn.value = "Pay Now";
        }
      }
      razorpay_instance.open();
    }
  }  
</script>
