<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.table-wrapper {
        background: #fff;
        padding: 20px 25px;
        margin: 30px auto;
		border-radius: 3px;
        box-shadow: 0 1px 1px rgba(0,0,0,.05);
    }
	.table-wrapper .btn {
		float: right;
		color: #333;
    	background-color: #fff;
		border-radius: 3px;
		border: none;
		outline: none !important;
		margin-left: 10px;
	}
	.table-wrapper .btn:hover {
        color: #333;
		background: #f2f2f2;
	}
	.table-wrapper .btn.btn-primary {
		color: #fff;
		background: #03A9F4;
	}
	.table-wrapper .btn.btn-primary:hover {
		background: #03a3e7;
	}
	.table-title .btn {		
		font-size: 13px;
		border: none;
	}
	.table-title .btn i {
		
		font-size: 21px;
		margin-right: 5px;
	}
	.table-title .btn span {
		
		margin-top: 2px;
	}
	.table-title {
		color: #fff;
		background: #4b5366;		
		padding: 16px 25px;
		
		border-radius: 3px 3px 0 0;
    }
    .table-title h2 {
		margin: 5px 0 0;
		font-size: 24px;
	}
	.show-entries select.form-control {        
        width: 60px;
		margin: 0 5px;
	}
	.table-filter .filter-group {
        float: right;
		margin-left: 15px;
    }
	.table-filter input, .table-filter select {
		height: 34px;
		border-radius: 3px;
		border-color: #ddd;
        box-shadow: none;
	}
	.table-filter {
		padding: 5px 0 15px;
		border-bottom: 1px solid #e9e9e9;
		margin-bottom: 5px;
	}
	.table-filter .btn {
		height: 34px;
	}
	.table-filter label {
		font-weight: normal;
		margin-left: 10px;
	}
	.table-filter select, .table-filter input {
		display: inline-block;
		margin-left: 5px;
	}
	.table-filter input {
		width: 200px;
		display: inline-block;
	}
	.filter-group select.form-control {
		width: 110px;
	}
	.filter-icon {
		float: right;
		margin-top: 7px;
	}
	.filter-icon i {
		font-size: 18px;
		opacity: 0.7;
	}	
    table.table tr th, table.table tr td {
        border-color: #e9e9e9;
		padding: 12px 15px;
		vertical-align: middle;
    }
	table.table tr th:first-child {
		width: 60px;
	}
	table.table tr th:last-child {
		width: 80px;
	}
    table.table-striped tbody tr:nth-of-type(odd) {
    	background-color: #fcfcfc;
	}
	table.table-striped.table-hover tbody tr:hover {
		background: #f5f5f5;
	}
    table.table th i {
        font-size: 13px;
        margin: 0 5px;
        cursor: pointer;
    }	
	table.table td a {
		font-weight: bold;
		color: #566787;
		display: inline-block;
		text-decoration: none;
	}
	table.table td a:hover {
		color: #2196F3;
	}
	table.table td a.view {        
		width: 30px;
		height: 30px;
		color: #2196F3;
		border: 2px solid;
		border-radius: 30px;
		text-align: center;
    }
    table.table td a.view i {
        font-size: 22px;
		margin: 2px 0 0 1px;
    }   
	table.table .avatar {
		border-radius: 50%;
		vertical-align: middle;
		margin-right: 10px;
	}
	.status {
		font-size: 30px;
		margin: 2px 2px 0 0;
		display: inline-block;
		vertical-align: middle;
		line-height: 10px;
	}
    .text-success {
        color: #10c469;
    }
    .text-info {
        color: #62c9e8;
    }
    .text-warning {
        color: #FFC107;
    }
    .text-danger {
        color: #ff5b5b;
    }
    .pagination {
        float: right;
        margin: 0 0 5px;
    }
    .pagination li a {
        border: none;
        font-size: 13px;
        min-width: 30px;
        min-height: 30px;
        color: #999;
        margin: 0 2px;
        line-height: 30px;
        border-radius: 2px !important;
        text-align: center;
        padding: 0 6px;
    }
    .pagination li a:hover {
        color: #666;
    }	
    .pagination li.active a {
        background: #03A9F4;
    }
    .pagination li.active a:hover {        
        background: #0397d6;
    }
	.pagination li.disabled i {
        color: #ccc;
    }
    .pagination li i {
        font-size: 16px;
        padding-top: 6px
    }
    .hint-text {
        float: left;
        margin-top: 10px;
        font-size: 13px;
    }    
</style>
<script type="text/javascript">
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
});
</script>


<div class="table-wrapper table-responsive-sm">
    <div id="accordion">
    <div class="card">
        <div class="card-header table-title" id="headingOne">
        <h5 class="mb-0 ">
            <div  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <h2>Order <b>Details</b></h2>
            </div>
        </h5>
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
        <div class="card-body">
        <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Order Date</th>						
                <th>Status</th>						
                <th>Net Amount</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <?php 
            $i = 0;
            foreach ($order as $o){ $i++;?>
            <tr>
                <td><?= $i?></td>
                <td><?php  echo date('d-F-Y',strtotime($o->date));?></td>                        
                <td> <?php  if($o->order_status == 0){
                                $status = 'Pending';
                                $class = 'text-warning';
                            }
                            if($o->order_status == 1){
                                $status = 'Delivered';
                                $class = 'text-success';
                            }
                            if($o->order_status == 2){
                                $status = 'Shipped';
                                $class = 'text-info';
                            }
                            if($o->order_status == 3){
                                $status = 'Cancelled';
                                $class = 'text-danger';
                            }
                    ?>
                <span class="status <?php echo $class;?>">&bull;</span> <?php echo $status?></td>
                <td> &#x20B9;<?php echo $o->total_amt?></td>
                <td><a href="<?php echo base_url('User/vieworder/').$o->invoice_id;?>" class="view" title="View Details"><i class="material-icons">&#xE5C8;</i></a></td>
            </tr>
            <?php }?>
            
        </tbody>
    </table>
        </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header table-title" id="headingTwo">
        <h5 class="mb-0">
            
            <div  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            <h2>Order <b>Appointment Booking</b></h2>
            </div>
        </h5>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
        <div class="card-body">
        <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Order Date</th>						
                <th>Title</th>
            </tr>
        </thead>
        <tbody>
        <?php 
            $i = 0;
            foreach ($order1 as $o){ $i++;?>
            <tr>
                <td><?= $i?></td>
                <td><?php  echo date('d-F-Y H:i',strtotime($o->start));?></td>                        
                
                <td><?php echo $o->title?></td>
               
            </tr>
            <?php }?>
            
        </tbody>
    </table>
        </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header table-title" id="headingThree">
        <h5 class="mb-0">
            <div  data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            <h2>Order <b>Contract & Services</b></h2>
            </div>
        </h5>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
        <div class="card-body">
        <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Order Date</th>						
                <th>Services Name</th>						
                <th>Net Amount</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <?php 
            $i = 0;
            foreach ($order2 as $o){ $i++;?>
            <tr>
                <td><?= $i?></td>
                <td><?php  echo date('d-F-Y',strtotime($o->create_at));?></td>                        
                <td><?php echo $o->pname?></td>                        
               
                <td> &#x20B9;<?php echo $o->price?></td>
                <td><a href="<?php echo base_url('Home/services_contract/').$o->pid;?>" class="view" title="View Details"><i class="material-icons">&#xE5C8;</i></a></td>
            </tr>
            <?php }?>
            
        </tbody>
    </table>
        </div>
        </div>
    </div>
    </div>
</div>
