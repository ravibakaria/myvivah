<!-- ========================= SECTION CONTENT ========================= -->
<?php if(count($this->cart->contents()) != 0){?>
<section class="section-content padding-y">
<div class="container">

<div class="row">

	<main class="col-md-9">
<form action="<?= base_url().'User/update_cart'?>" method  = "post" id = "updatform">
<div class="card">
<div class="table-responsive-sm">
<table class="table table-borderless table-shopping-cart">
<thead class="text-muted">
<tr class="small text-uppercase">
  <th scope="col">Product</th>
  <th scope="col" width="120">Quantity</th>
  <th scope="col" width="120">Price</th>
  <th scope="col" class="text-right" width="200"> </th>
</tr>
</thead>
<tbody>
<?php 
$i = 1;
foreach ($this->cart->contents() as $items): ?>
	<?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>
  <?php echo form_hidden($i.'[id]', $items['id']); ?>
    <?php echo form_hidden($i.'[price]', $items['price']); ?>
	<?php echo form_hidden($i.'[product_name]', $items['name']); ?>
	<?php echo form_hidden($i.'[seller_id]', $items['seller_id']); ?>
        <?php echo form_hidden($i.'[total_amt]', $this->cart->total()); ?>
		<?php 
			if($this->session->userdata('user')){    
                echo form_hidden($i.'[u_id]', $this->session->userdata('userm')->id);
            } else {
                echo form_hidden($i.'[u_id]', '0');
			}
		?>
<tr>
	<td>
		<figure class="itemside">
			<div class="aside"><img src="<?php echo base_url('assets/images/thumbnails/'.$items['image'])?>" class="img-sm"></div>
			<figcaption class="info">
				<a href="#" class="title text-dark"><?php echo $items['name'];?></a>
				
			</figcaption>
		</figure>
	</td>
	<td> 
		
		<?php echo form_input(array('name' => $i.'[qty]', 'value' => $items['qty'], 'maxlength' => '3','class' => 'form-control myqty', 'size' => '5')); ?>	
		
	</td>
	<td> 
		<div class="price-wrap"> 
			<var class="price">&#x20B9;<?php echo $this->cart->format_number($items['subtotal']);?></var>
			<small  class="text-muted">&#x20B9;<?php echo $items['price']?> each </small> 
		</div> <!-- price-wrap .// -->
	</td>
	<td class="text-right"> 
	
	<a href="<?php echo base_url('User/removeitom/').$items['rowid']?>" class="btn btn-light"> Remove</a>
	</td>
</tr>
<?php $i++; ?>

<?php endforeach; ?>

</tbody>
</table>
</div>
<div class="card-body border-top">
	<a href="<?php echo base_url('User/shiping')?>" class="btn btn-primary float-md-right"> Proceed to Buy <i class="fa fa-chevron-right"></i> </a>
	<a href="<?php echo base_url()?>" class="btn btn-light"> <i class="fa fa-chevron-left"></i> Continue shopping </a>

  
</div>	
</div> <!-- card.// -->
</form>
<!-- <div class="alert alert-success mt-3">
	<p class="icontext"><i class="icon text-success fa fa-truck"></i> Delivery within 1-2 weeks</p>
</div> -->

	</main> <!-- col.// -->
	<aside class="col-md-3">
		<div class="card mb-3">
			<div class="card-body">
			<form>
				<div class="form-group">
					<label>Have coupon?</label>
					<div class="input-group">
						<input type="text" class="form-control" name="" placeholder="Coupon code">
						<span class="input-group-append"> 
							<button class="btn btn-primary">Apply</button>
						</span>
					</div>
				</div>
			</form>
			</div> <!-- card-body.// -->
		</div>  <!-- card .// -->
		<div class="card">
			<div class="card-body">
					
					<dl class="dlist-align">
					  <dt>Total:</dt>
					  <dd class="text-right  h5"><strong>&#x20B9;<?php echo $this->cart->format_number($this->cart->total()); ?></strong></dd>
					</dl>
					<hr>
					<p class="text-center mb-3">
						<img src="<?php echo base_url('assets/items/payments.png')?>" height="26">
					</p>
					
			</div> <!-- card-body.// -->
		</div>  <!-- card .// -->
	</aside> <!-- col.// -->
</div>

</div> <!-- container .//  -->
</section>
<?php } ?>
<!-- ========================= SECTION CONTENT END// ========================= -->

<!-- ========================= SECTION  ========================= -->
<!-- <section class="section-name border-top padding-y">
<div class="container">
<h6>Payment and refund policy</h6>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>

</div>
</section> -->
<!-- ========================= SECTION  END// ========================= -->


<style>
    .btn-primary {
  color: #fff !important;
  background-color: #ff6a00 !important;
  border-color: #ff6a00 !important;
}
</style>


<script>
$(".myqty").blur(function(){
  
  $("#updatform").submit();
	
});
</script>
