<!-- ============================ ITEM DETAIL ======================== -->
<div class="row " style = "margin:15px" >
		<aside class="col-md-6">
<div class="card">
<article class="gallery-wrap"> 
	<div class="img-big-wrap">
	  <div class = "showimg"> <img src="<?php echo base_url('assets/images/'.$images[0]->img_name)?>" class="img-fluid"></div>
	</div> <!-- slider-product.// -->
	<div class="thumbs-wrap">
		<?php 
			if(!empty($images)){
                $i = 1;
                foreach($images as $img){?>
					<a  class="item-thumb getimg" dataid="<?php echo $img->img_name?>"> 
						<img src="<?php echo base_url('assets/images/thumbnails/'.$img->img_name)?>" class="img-fluid">
					</a>
					<!-- <a href="#" class="item-thumb"> <img src="<?php echo base_url('assets/items/15-1.jpg')?>"></a>
					<a href="#" class="item-thumb"> <img src="<?php echo base_url('assets/items/15-2.jpg')?>"></a>
					<a href="#" class="item-thumb"> <img src="<?php echo base_url('assets/items/15-1.jpg')?>"></a> -->
					<?php
				}
			}
		?>
	</div> <!-- slider-nav.// -->
</article> <!-- gallery-wrap .end// -->
</div> <!-- card.// -->
		</aside>
		<main class="col-md-6">
<article class="product-info-aside">

<h2 class="title mt-3"><?php echo $product->name ;?></h2>

<!-- <div class="rating-wrap my-3">
	<ul class="rating-stars">
		<li style="width:80%" class="stars-active"> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> 
		</li>
		<li>
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
			<i class="fa fa-star"></i> 
		</li>
	</ul>
	<small class="label-rating text-muted">132 reviews</small>
	<small class="label-rating text-success"> <i class="fa fa-clipboard-check"></i> 154 orders </small>
</div>  -->

<div class="mb-3"> 
	<var class="price h4">&#x20B9; <?php echo $product->pries ;?></var> 
	<span class="text-muted">&#x20B9; <?php echo $product->pries ;?> incl. VAT</span> 
</div> <!-- price-detail-wrap .// -->

<p><?php echo $product->description ;?></p>


<dl class="row">
  <dt class="col-sm-3">Delivery time</dt>
  <?php if($product->delivery !==' '){?>
  <dd class="col-sm-9"><?php echo $product->delivery.' - '.$product->deliveryin;?></dd>
  <?php } ?>
  
  <dt class="col-sm-3">Availabilty</dt>
  <?php if($product->quantity !=''){?>
  <dd class="col-sm-9"><?php echo $product->quantity;?></dd>
  <?php } else{?>
	<dd class="col-sm-9">in Stock</dd>
  <?php } ?>
</dl>

	<div class="form-row  mt-4">
		<form method = "post" action="<?= base_url().'User/add_cart'?>">
			
			<input type="hidden" name="id" value= "<?= $product->id?>">
			
		
		<div class="form-group col-md">
			<div class="input-group mb-3 input-spinner">
			<div class="input-group-append">
			    <button class="btn btn-light" type="button" id="button-minus"> &minus; </button>
			  </div>
			  <input type="text" class="form-control" id = "qty" value="1" name="qty" readonly>
			  <div class="input-group-prepend">
			    <button class="btn btn-light" type="button" id="button-plus"> + </button>
			  </div>
			</div>
		</div> <!-- col.// -->
		<div class="form-group col-md">
		
			<button class="btn btn-primary" type = "submit"> 
				<i class="fa fa-shopping-cart"></i><span class="text">Add to cart</span> 
			</button>
		
		</div> <!-- col.// -->
	</form>
	</div> <!-- row.// -->

</article> <!-- product-info-aside .// -->
		</main> <!-- col.// -->
	</div> <!-- row.// -->

<!-- ================ ITEM DETAIL END .// ================= -->
<style>
    .btn-primary {
  color: #fff !important;
  background-color: #ff6a00 !important;
  border-color: #ff6a00 !important;
}
</style>

<script>
	$('.getimg').click(function () {
		
		var img = $(this).attr('dataid');
		var src = "<?php echo base_url('assets/images/')?>";
		var html = '<img src="'+src+img+'" class="img-fluid">';
		$(".showimg").html(html);
	});



$(document).ready(function(){
	var count = 1;
	$("#button-plus").click(function() {
		if (count >= 1) {
			++count;
			$('#qty').val(count);
		}
	});
	$("#button-minus").click(function() {
		if (count >= 2) {
			--count;
			$('#qty').val(count);
		}
	});
});
</script>