<!-- ========================= SECTION CONTENT ========================= -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<?php if(count($this->cart->contents()) != 0){?>
    <section class="section-content padding-y">
        <div class="container">
            <div class="row">
            <div class = "col-md-8 offset-2 card">
                    <article class="card-body">
                        <header class="mb-4"><h4 class="card-title">Shipping Address</h4></header>
                        <form id = "registerform" method = "post" action = "<?php echo base_url('User/pay')?>">
                            <div class="form-row">
                                <div class="col form-group">
                                    <label>First name</label>
                                    <input type="text" class="form-control" placeholder="" name = "name" id = "name">
                                </div> <!-- form-group end.// -->
                                <div class="col form-group">
                                    <label>Last name</label>
                                    <input type="text" class="form-control" placeholder="" name ="lname" id = "lname">
                                </div> <!-- form-group end.// -->
                            </div> <!-- form-row end.// --> 
                            <div class="form-group">
                                <label>Mobile No</label>
                                <input type="number" class="form-control" placeholder="" name = "phone" id = "phone">
                            </div>                      
                            <div class="form-group">
                                <label>Addresss</label>
                                <textarea name="add" class="form-control" cols="30" rows="5" id = 'add'></textarea>
                            </div> <!-- form-group end.// --> 
                            <div class="form-group">
                                <label>Pin</label>
                                <input type="number" class="form-control" placeholder="" name = "pin" id = 'pin'>
                            </div>
                            <ul class="list-group list-group-flush">  
                                <li class="list-group-item">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input"  name = 'bmodules' value="1" id = "check" >
                                    <label class="custom-control-label" for="check">Billing Address Different From Shipping Dddress</label>
                                    
                                </div>
                                </li>  
                            </ul>
                            <div id= "billaddr">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block"> Save  </button>
                            </div> <!-- form-group// -->            
                        </form>
                    </article><!-- card-body.// -->
                </div>
                
                
                
            </div>
        </div> <!-- container .//  -->
    </section>
<?php } ?>
<!-- ========================= SECTION CONTENT END// ========================= -->


<script>
$('#check').click(function() {
    var html = '<header class="mb-4"><h4 class="card-title">Billing Dddress</h4></header>'+
                '<div class="form-row">'+
                    '<div class="col form-group">'+
                        '<label>First name</label>'+
                        '<input type="text" class="form-control" placeholder="" name = "bname">'+
                    '</div>'+
                    '<div class="col form-group">'+
                        '<label>Last name</label>'+
                        '<input type="text" class="form-control" placeholder="" name ="blname">'+
                    '</div>'+
                '</div>'+   
                '<div class="form-group">'+
                        '<label>Mobile No</label>'+
                            '<input type="number" class="form-control" placeholder="" name = "bphone">'+
                        '</div>'+                    
                '<div class="form-group">'+
                    '<label>Addresss</label>'+
                    '<textarea name="badd" class="form-control" cols="30" rows="5"></textarea>'+
                '</div>'+
                '<div class="form-group">'+
                    '<label>Pin</label>'+
                    '<input type="number" class="form-control" placeholder="" name = "bPin">'+
                '</div>';
    if($('#check').prop('checked')){
        $('#billaddr').html(html);
    }else{
        $('#billaddr').html('');
    }
    
    
    
});
</script>
<!-- <script>
      $(function () {

        $('.pay').on('registerform','click', function (e) {
            // name = $('#name').val();
            // lname = $('#lname').val();
            // phone = $('#phone').val();
            // addr = $('#addr').val();
            // pin = $('#pin').val();
            // if(name =='' || lname == '' || phone == '' || addr =='' || pin ==''){
            //     alert('Please Fill the Shiping Address!');
            //     return false;
            // }
          e.preventDefault();

          $.ajax({
            type: 'post',
            url: "<?php echo base_url('User/pay')?>",
            data: $('registerform').serialize(),
            success: function () {
              alert('form was submitted');
            }
          });

        });

      });
$("#name").blur(function(){
  name = $('#name').val();
  lname = $('#lname').val();
  phone = $('#phone').val();
  addr = $('#addr').val();
  pin = $('#pin').val();
  if(name =='' || lname == '' || phone == '' || addr =='' || pin ==''){
    $(".pay").addClass("disabled");
    $(".pay").attr("disabled");
    $(".pay").removeClass("style");
    $(".pay").css("display","none")
    $(".error").removeClass("style");
    $(".error").css("display","block");
  }else{
    $(".error").css("display","none");
    $(".pay").css("display","block");
    
    $(".pay").removeClass("disabled");
    $(".pay").removeAttr("disabled");
  }
});
$("#lname").blur(function(){
  name = $('#name').val();
  lname = $('#lname').val();
  phone = $('#phone').val();
  addr = $('#addr').val();
  pin = $('#pin').val();
  if(name =='' || lname == '' || phone == '' || addr =='' || pin ==''){
    $(".pay").addClass("disabled");
    $(".pay").attr("disabled");
    $(".pay").removeClass("style");
    $(".pay").css("display","none")
    $(".error").removeClass("style");
    $(".error").css("display","block");
  }else{
    $(".error").css("display","none");
    $(".pay").css("display","block");
    
    $(".pay").removeClass("disabled");
    $(".pay").removeAttr("disabled");
  }
});
$("#phone").blur(function(){
  name = $('#name').val();
  lname = $('#lname').val();
  phone = $('#phone').val();
  addr = $('#addr').val();
  pin = $('#pin').val();
  if(name =='' || lname == '' || phone == '' || addr =='' || pin ==''){
    $(".pay").addClass("disabled");
    $(".pay").attr("disabled");
    $(".pay").removeClass("style");
    $(".pay").css("display","none")
    $(".error").removeClass("style");
    $(".error").css("display","block");
  }else{
    $(".error").css("display","none");
    $(".pay").css("display","block");
   
    $(".pay").removeClass("disabled");
    $(".pay").removeAttr("disabled");
  }
});
$("#addr").blur(function(){
  name = $('#name').val();
  lname = $('#lname').val();
  phone = $('#phone').val();
  addr = $('#addr').val();
  pin = $('#pin').val();
  if(name =='' || lname == '' || phone == '' || addr =='' || pin ==''){
    $(".pay").addClass("disabled");
    $(".pay").attr("disabled");
    $(".pay").removeClass("style");
    $(".pay").css("display","none")
    $(".error").removeClass("style");
    $(".error").css("display","block");
  }else{
    $(".error").css("display","none");
    $(".pay").css("display","block");
   
    $(".pay").removeClass("disabled");
    $(".pay").removeAttr("disabled");
  }
});
$("#pin").blur(function(){
  name = $('#name').val();
  lname = $('#lname').val();
  phone = $('#phone').val();
  addr = $('#addr').val();
  pin = $('#pin').val();
  if(name =='' || lname == '' || phone == '' || addr =='' || pin ==''){
    $(".pay").addClass("disabled");
    $(".pay").attr("disabled");
    $(".pay").removeClass("style");
    $(".pay").css("display","none")
    $(".error").removeClass("style");
    $(".error").css("display","block");
  }else{
    $(".error").css("display","none");
    $(".pay").css("display","block");
    
    $(".pay").removeClass("disabled");
    $(".pay").removeAttr("disabled");
  }
});
</script> -->

<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function() {
    $j("#registerform").validate({
        rules: {
            name: "required",
            lname: "required",
            phone: "required",
            add: "required",
            Pin: {
                required: true,
                minlength: 6,
            }
            
            
        },
        messages: {
          
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long",
                equalTo: "Please enter the same password as above"
            }
        }
    });
});
</script>
