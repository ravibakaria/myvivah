<?php
// login controller 

// function __construct() {
//         parent::__construct();
        
//         // Load facebook library
//         $this->load->library('facebook');
//         $this->load->library('form_validation');
//         //Load user model
//         $this->load->model('user');
//     }

//     public function userlogin(){
        
       
//         if($this->input->post('login') == '1'){
//             $this->form_validation->set_rules('email', 'Email', 'trim|required');
//             $this->form_validation->set_rules('pass', 'Password', 'trim|required');

//             if ($this->form_validation->run() == false) {
//                 $message = validation_errors();
//                 print_r($message);die;
//             } else {

//                 $data = array(
                
//                     'email' => $this->input->post('email'),
//                     'password' => $this->input->post('pass'),
//                 );
               
//                     $email = $this->input->post('email');
//                     $user = $this->user->login($email);
//                    // print_r($user);die;
//                     if(count($user) == 1){
//                         if($user->pass == md5($this->input->post('pass'))){
//                             $this->session->set_userdata('logged_in', $user);
//                             redirect(base_url('Welcome/dashboard'));
//                         }else{
//                             $data['login_url'] = $this->facebook->getLoginUrl(array(
//                                 'redirect_uri' => site_url('login/index'), 
//                                 'scope' => array("email") // permissions here
//                             ));
//                             redirect(base_url('login'));
//                         }
                        
//                     }else{
//                         $data['login_url'] = $this->facebook->getLoginUrl(array(
//                             'redirect_uri' => site_url('login/index') // permissions here
//                         ));
//                         redirect(base_url('login'));
//                     }
               
                 
                

//             }
//         } else{
//             $this->form_validation->set_rules('pass', 'Password', 'required');
//             $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[pass]');
//             $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
//             if ($this->form_validation->run() == false) {
//                 $message = validation_errors();
//                 print_r($message);die;
//             }else{
//                 $data = array(
//                     'email'=> $_POST['email'],
//                     'pass' => md5($_POST['pass'])
//                 );
//                 $this->user->register($data);
//                 redirect(base_url('login'));
//             }
           
//         }
   
// }

// public function logout(){
//     $this->session->unset_userdata('logged_in');
//     $this->session->sess_destroy();
//     redirect(base_url('login'));
// }

// User mode ---------------------------- ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {
    function __construct() {
        $this->tableName = 'users';
        $this->primaryKey = 'id';
    }
    
    public function login($email){
        $this->db->select('*');
        $this->db->from($this->tableName);
        $this->db->where('email',$email);
        $prevQuery = $this->db->get();
        return $prevQuery->row();
    }
    public function register($data){
        $this->db->insert($this->tableName,$data);
        
        return true;
    }
    public function forgetpass($email){
        $this->db->select('*');
        $this->db->from($this->tableName);
        $this->db->where('email',$email);
        $prevQuery = $this->db->get();
        return $prevQuery->row();
    }
}