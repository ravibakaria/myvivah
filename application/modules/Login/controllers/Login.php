<?php

class Login extends MX_Controller
{
    function __construct()
    {
        $this->load->model('User');
        $this->load->library('form_validation');
        parent::__construct();
    }

public  function index()
    {
        if($this->session->userdata('userm') == ''){
            $this->load->userTemplate('login'); 
        }else{
            redirect(base_url('Home'));
        }
        
    }

    public  function register()
    {
        if($this->session->userdata('userm') == ''){
            $this->load->userTemplate('register');
        } else{
            redirect(base_url('Home'));
        }
    }
 
    public function userlogin(){
        if($this->input->post()){
            $email = $this->input->post('email');
            $pass = $this->input->post('password');
            $data = array();
            $var = $this->User->login($email);
        // print_r($var);
            if(!empty($var)){
                if($var->password == md5($pass)){
                    $this->session->set_userdata('userm',$var);
                    redirect(base_url('Home'));
                    
                }else{
                    $this->session->set_flashdata('message_r', 'Password invalid');
                    
                    redirect(base_url('Login'));
                }
            }else{
                $this->session->set_flashdata('message_r', 'invalid Password Or Email');
                redirect(base_url('Login'));
            }
            
        }
    }
    

public function logout(){
    $this->session->unset_userdata('userm');
    $this->session->sess_destroy();
    redirect (base_url());
    
}

public function register_action(){
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('cpassword', 'Password Confirmation', 'required|matches[password]');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
    if ($this->form_validation->run() == false) {
        //echo validation_errors();
        $this->session->set_flashdata('message_r', validation_errors());
        redirect(base_url('Login/register'));
    }else{
        $data = array(
            'email'=> $_POST['email'],
            'name'=> $_POST['name'],
            'password' => md5($_POST['password']),
            'lname' => $_POST['lname'],
            'phone' =>$_POST['phone'],
            'gender' => $_POST['gender'],
            'city' => $_POST['city'],
            'country' => $_POST['country'],
            'pin' => $_POST['pin'],
           
        );
        $this->User->register($data);

        redirect(base_url('Login'));
    }
    

}

public function xyz(){
    // $this->load->library('SendEmail');
     $data = array(

         'userName'=> 'Ravi Kumar Bakaria'
  
           );
     $subject='OTP';
     $name = 'ravi';
     $email = 'ravi.bakaria@codeinsightacademy.com';
     $email_params = array();
     $email_params['to_name'] = $name;
     $email_params['to_email'] = $email;
     $email_params['from_email'] = "";
     $email_params['subject'] = $subject;
           $emailotpmessage = $this->load->view('Login/anillabs.php',$data,TRUE);
     $email_params['message'] = $emailotpmessage;
     $this->mail($email_params);
 }

 function mail($params = array()) {
      $this->load->library('phpmailer');

     $mailer = $this->phpmailer;

     // $mailer

     $mailer->IsSMTP(); // telling the class to use SMTP
     $mailer->SMTPDebug = 1;                // enables SMTP debug information (for testing)
     // 1 = errors and messages
     // 2 = messages only

     $mailer->SMTPAuth = true;                  // enable SMTP authentication
     $mailer->SMTPSecure = "ssl";                 // sets the prefix to the servier
     $mailer->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
     $mailer->Port = 465;                   // set the SMTP port for the GMAIL server

     $mailer->Username = "smtptest@dweb.in";            //edit
     $mailer->Password = "Dynamic@123";
    //  $mailer->Username = "ravibakaria4@gmail.com";            //edit
    //  $mailer->Password = "9220995987";


     $mailer->AltBody = "To view the message, please use an HTML compatible email viewer!";


     $mailer->FromName = "Noreply";
     $mailer->From = "Noreply@gmail.com";  //edit
     $mailer->isHTML = true;

     if (isset($params['from_name'])) {
         $mailer->FromName = $params['from_name'];
     }

     if (isset($params['from_email'])) {
         $mailer->From = $params['from_email'];
     }


     if (!isset($params['subject'])) {
         throw new Exception("Email: Subject is required", 1);
     }
     if (!isset($params['message'])) {
         throw new Exception("Email: Email body is required", 1);
     }

     if (!isset($params['to_email']) || (isset($params['to']) && count($params['to']) == 0)) {
         throw new Exception("Email: Atleast one recipient is required", 1);
     }

     if (isset($params['to_email'])) {
         $params['to_name'] = (isset($params['to_name'])) ? $params['to_name'] : $params['to_email'];
         $mailer->AddAddress($params['to_email'], $params['to_name']);
     } else {
         throw new Exception("Email: Multiple recipient is not implemented yet", 1);
     }

     if (isset($params['addcc']) && !empty($params['addcc'])) {
         foreach ($params['addcc'] as $email => $name)
             $mailer->AddCC($email, $name);
     }
     if (isset($params['addbcc']) && !empty($params['addbcc'])) {
         foreach ($params['addbcc'] as $email => $name)
             $mailer->AddBCC($email, $name);
     }

     if (isset($params['addattachment']) && !empty($params['addattachment'])) {
         foreach ($params['addattachment'] as $key => $path)
             $mailer->AddAttachment($path, $key);
     }

     $mailer->Subject = $params['subject'];
     $mailer->MsgHTML($params['message']);

     $status = 0;

     ob_start();
     $mail_send_status = $mailer->Send();
     $mail_send_errors = ob_get_contents();
     ob_end_clean();
     echo $mail_send_errors;

     if ($mail_send_status) {
         $status = 1;
     }else{
         $mailer->ErrorInfo;
         exit;
     }

     $mailer->ClearAddresses();
     $mailer->ClearAttachments();

     return $status;
 }
}

?>