<?php

class Mymodel extends CI_Model
{
    public $id = 'id'; 
 
    public $order = 'DESC'; 
    function __construct()
    {
          // parent::__construct();
           $this->column_order = array(null, 'name','invoice_no','total_amt', 'date','status');
           $this->column_order1 = array(null, 'name','pries','description','status');
           // Set searchable column fields
          $this->column_search = array('name','invoice_no','date','total_amt');
          $this->column_search1 = array('name','pries','description','status');
           // Set default order
           $this->order = array('name' => 'asc');
    }



function get_all_active($table) 
    { 
    $this->db->where('status', 0); 
    $this->db->order_by($this->id, 'DESC'); 
    return $this->db->get($table)->result(); 
} 

function get_by_id($table,$id) 
{ 
$this->db->where($this->id, $id); 
return $this->db->get($table)->row(); 
} 
function statcity($pin){ 
    $this->db->where("pin", $pin); 
    return $this->db->get("states")->row(); 
} 

function get_by_id1($table,$id) 
{ 
$this->db->where($this->id, $id); 
return $this->db->get($table)->result();  
} 

function get_by_service($table,$id) 
{ 
    $this->db->where("service", $id); 
    $this->db->where("status", 1); 
    $this->db->order_by('id', 'DESC'); 
    return $this->db->get($table)->result(); 
} 

public function popular_category(){  
    $this->db->select(' products.id, products.name, products.pries,products.showpries, products.status, service.name as servicename');
    $this->db->from('products');
   
    $this->db->join('service', 'service.id = products.service', 'left'); 
    $this->db->where('products.status',1);
    $this->db->order_by('products.id', 'DESC');
    $this->db->group_by('products.service');
    $this->db->limit('3');
   
    $query = $this->db->get();
     //echo $this->db->last_query();exit();
     return $query->result();
}

public function get_all_product(){
    $this->db->select(' products.id, products.name, products.status,images.img_name, service.name as servicename');
    $this->db->from('products');
    $this->db->join('images', 'products.id = images.product_id', 'left'); 
    $this->db->join('service', 'service.id = products.service', 'left'); 
    $this->db->where('products.status',1);
    $this->db->order_by('products.id', 'ASC');
    $this->db->group_by('products.id');
    $this->db->limit('8');
    $query = $this->db->get();
     //echo $this->db->last_query();exit();
     return $query->result();
}
public function recommended($sellerid){
    $this->db->select('products.id, products.name, products.pries,products.status');
    $this->db->from('products');
    if(!empty($sellerid)){
        foreach($sellerid as $d){
            $this->db->or_where('products.created_by',$d);
        }
        $this->db->where('products.status',1);
        //$this->db->where('products.service',$id);
    }else{
        $this->db->where('products.created_by',0);
    }
    //$this->db->where('products.status',1);
    $this->db->order_by('rand()');
    $this->db->group_by('products.id');
    $this->db->limit('12');
    $query = $this->db->get();
     //echo $this->db->last_query();exit();
     return $query->result();
}

public function get_product($id,$sellerid){
    $this->db->select('products.id, products.name,products.description,products.pries,products.showpries, products.status');
    $this->db->from('products');
    
    
    if(!empty($sellerid)){
        foreach($sellerid as $d){
            $this->db->or_where('products.created_by',$d);
        }
        $this->db->where('products.status',1);
        $this->db->where('products.service',$id);
    }else{
        $this->db->where('products.created_by',0);
    }
    $this->db->order_by('products.id', 'ASC');
     $this->db->group_by('products.id');
    $this->db->limit('8');
    $query = $this->db->get();
    //echo $this->db->last_query();exit();
     return $query->result();
}
public function get_product1($id){
    
    $this->db->select('products.id, products.created_by, bussines_partner.pin,bussines_partner.aria');
    $this->db->from('products');
    $this->db->join('bussines_partner', 'bussines_partner.id = products.created_by', 'left'); 
    $this->db->where('products.status',1);
    $this->db->where('products.service',$id);
    $this->db->group_by('products.created_by');
    $query = $this->db->get();
     //echo $this->db->last_query();exit();
     return $query->result();
}
public function get_portfolio(){
    $this->db->select('*');
    $this->db->from('portfolio');
    $this->db->where('bussines_module',6);
    $this->db->or_where('bussines_module',7);
    $this->db->limit('8');
    $query = $this->db->get();
     //echo $this->db->last_query();exit();
     return $query->result();
}

public function get_portfolio_by_id($id){
    $this->db->select('*');
    $this->db->from('portfolio');
    $this->db->where('u_id',$id);
    $query = $this->db->get();
     //echo $this->db->last_query();exit();
     return $query->row();
}

public function insert($table,$data) { 
    $this->db->insert($table, $data); 
}

public function get_booking_product($id){
    $this->db->select('*');
    $this->db->from('appointment_product');
    $this->db->where('isdelete',0);
    $this->db->where('status',1);
    $this->db->where('uid',$id);
    $this->db->order_by('id', 'ASC');
    $this->db->limit('8');
    $query = $this->db->get();
     //echo $this->db->last_query();exit();
     return $query->result();
}
public function get_booking_product1($table,$id){
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where('isdelete',0);
    $this->db->where('status',1);
    $this->db->where('id',$id);
    $query = $this->db->get();
     //echo $this->db->last_query();exit();
     return $query->row();
}

function get_all($table) 
    { 
    $this->db->order_by('id', 'DESC'); 
    $this->db->where('isdelete',0);
    $this->db->where('status',1);
    return $this->db->get($table)->result(); 
} 

public function get_product_name($name){
    $this->db->select(' products.id, products.name,products.description,products.pries,products.showpries, products.status');
    $this->db->from('products');
   
    $this->db->where('products.status',1);
    $this->db->where('products.name',$name);
    $this->db->order_by('products.id', 'ASC');
     $this->db->group_by('products.id');
    $this->db->limit('8');
    $query = $this->db->get();
     //echo $this->db->last_query();exit();
     return $query->result();
}

public function get_event($tabel,$id,$date){
    $this->db->select('*,DATE_FORMAT(start, "%h:%i") as time');
    if($id !=''){
        $this->db->where('s_id',$id);
    }
    
    $this->db->where('DATE_FORMAT(end, "%Y-%m-%d")=',$date);
    return $this->db->get($tabel)->result(); 
}

public function get_delevry_aria($uid){
    //$this->db->distinct();
    $this->db->select('delivery_aria.*,states.pin');
    $this->db->from('delivery_aria');
    $this->db->join('states', 'states.city = delivery_aria.city', 'left'); 
    $this->db->where('uid',$uid);
    // foreach($uid as $id){
    //     $this->db->or_where('uid',$id);
    // }
    //$this->db->group_by('uid');
    
    $query = $this->db->get();
     // echo $this->db->last_query();exit();
     return $query->result();
}

function statcity_pin($city){ 
    $this->db->where("city", $city); 
    return $this->db->get("states")->result(); 
} 

function active_blog(){
    $this->db->where("status", 1); 
    return $this->db->get("blogs")->result(); 
}

}