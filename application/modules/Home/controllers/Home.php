<?php

class Home extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mymodel');
        $this->load->library('pagination');
    }

    public  function index(){ 
        $id = $this->checkDeleviry('55');
        $popular_category = $this->Mymodel->popular_category();
        // echo $this->db->last_query();die;
        //  $allproducts = $this->Mymodel->get_all_product();
        $id = $this->checkDeleviry('55');
        $clothes = $this->Mymodel->get_product('55',$id);
        $id = $this->checkDeleviry('26');
        $electronics = $this->Mymodel->get_product('26',$id);
        $recommendeds = $this->Mymodel->recommended($id);
        $banners = $this->db->get_where('banner', array('status' => 1))->result_array();
        $data = [
            'popular_category'=>$popular_category,
            'banners'=>$banners,
            'clothes' =>$clothes,
            'electronics' =>$electronics,
            'recommendeds' =>$recommendeds,
        ];
        $this->load->userTemplate('index',$data);
        
    }

    public function product_service($id){
        
        $products = $this->Mymodel->get_by_service('products',$id);
        $data = [
            'products' =>$products 
        ];
        $this->load->userTemplate('products',$data);
    }

    public function show_all_doctors(){
        
        // $products = $this->Mymodel->get_by_service('products',$id);
        // $data = [
        //     'products' =>$products 
        // ];
        $this->load->userTemplate('list_of_doctors');
    }

    public function show_all_produuct_categories($id){
        
        //$delivery_uid = $this->Mymodel->get_delevry_aria($seller_ids);
        $seller_ids = $this->checkDeleviry($id);
        $products = $this->Mymodel->get_product($id,$seller_ids);
        //echo $this->db->last_query();die;
        $recommendeds = $this->Mymodel->recommended($seller_ids);
       
        $data = [
            'products' =>$products ,
            'recommendeds' =>$recommendeds,
        ];
        // echo "<pre>";
        // print_r($products);die;
        $this->load->userTemplate('list_of_products',$data);
    }
    public function show_booking(){
        $countDates = 0;
        $date = Date('Y-m-d', strtotime("+".$countDates." days"));
        $products = $this->Mymodel->get_portfolio();
        $id = $this->checkDeleviry('55');
        $recommendeds = $this->Mymodel->recommended($id);
        
        //echo $this->db->last_query();die;
        $data = [
            'products' =>$products ,
           
            'recommendeds' =>$recommendeds,
        ];
    //     echo "<pre>";
    //    print_r($data);die;
        $this->load->userTemplate('list_of_booking',$data);
    }

public function userbooking($id){
    $products = $this->Mymodel->get_portfolio_by_id($id);
    
    $data = [
        'products' =>$products ,
        
    ];
    $this->load->userTemplate('booking_by_user',$data);
}
public function addbooking(){
   
    $this->session->set_userdata('booking',$_POST);
    $post = $this->session->userdata('booking');
   // print_r($post);
    
 
}

public function addbooking1(){
   if($this->session->userdata('userm') !=''){
        $data['return_url'] = base_url().'Home/callback';
        $data['surl'] = base_url().'Home/success';
        $data['furl'] = base_url().'Home/failed';
        $data['currency_code'] = 'INR';
        $this->load->userTemplate('makepay',$data);
   }else{
        redirect(base_url('Login'));
   }
 
}
public function addbooking11(){
    $id = $this->input->post('uid');
    $date = $this->input->post('date');
    $events = $this->Mymodel->get_event('events',$id,$date);
   // echo $this->db->last_query();die;
    $products = $this->Mymodel->get_portfolio_by_id($id);
        $data = [
            'product' =>$products ,
            'date'=>$date,
            'events' =>$events
        ];
    //     echo "<pre>";
    //    print_r($data);die;
        $this->load->view('ajax_list_of_booking',$data);


}

private function get_curl_handle($payment_id, $amount)  {
    $url = 'https://api.razorpay.com/v1/payments/'.$payment_id.'/capture';
    $key_id = RAZOR_KEY_ID;
    $key_secret = RAZOR_KEY_SECRET;
    $fields_string = "amount=$amount";
    //cURL Request
    $ch = curl_init();
    //set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERPWD, $key_id.':'.$key_secret);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
   // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__).'/ca-bundle.crt');
    return $ch;
}   

public function callback() {        
    if (!empty($this->input->post('razorpay_payment_id')) && !empty($this->input->post('merchant_order_id'))) {
        $razorpay_payment_id = $this->input->post('razorpay_payment_id');
        $merchant_order_id = $this->input->post('merchant_order_id');
        $currency_code = 'INR';
        $amount = $this->input->post('merchant_total');
        $success = false;
        $error = '';
        try {                
            $ch = $this->get_curl_handle($razorpay_payment_id, $amount);
            //execute post
            $result = curl_exec($ch);
            $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($result === false) {
                $success = false;
                $error = 'Curl error: '.curl_error($ch);
            } else {
                $response_array = json_decode($result, true);
              // echo "<pre>";print_r($response_array);exit;

                    //Check success response
                    if ($http_status === 200 and isset($response_array['error']) === false) {
                        $success = true;
                       //$this->insert_oder_items($merchant_order_id);
                       $id = $this->session->userdata('userm')->id;
                       $this->booking($merchant_order_id);
                       $this->add_event($id);
                       $this->payment_order($response_array,$merchant_order_id);
                    } else {
                        $success = false;
                        if (!empty($response_array['error']['code'])) {
                            $error = $response_array['error']['code'].':'.$response_array['error']['description'];
                        } else {
                            $error = 'RAZORPAY_ERROR:Invalid Response <br/>'.$result;
                        }
                    }
            }
            //close connection
            curl_close($ch);
        } catch (Exception $e) {
            $success = false;
            $error = 'OPENCART_ERROR:Request to Razorpay Failed';
        }
        if($success === true) {
            if(!empty($this->session->userdata('ci_subscription_keys'))) {
                $this->session->unset_userdata('ci_subscription_keys');
             }
            if (!$order_info['order_status_id']) {
                redirect($this->input->post('merchant_surl_id'));
            } else {
                redirect($this->input->post('merchant_surl_id'));
            }

        } else {
            redirect($this->input->post('merchant_furl_id'));
        }
    } else {
        echo 'An error occured. Contact site administrator, please!';
    }
} 
public function success() {
    $this->load->userTemplate('paydone'); 

    //$this->load->Template('success.php',$data);
}  
public function failed() {
    $data['title'] = 'Razorpay Failed | TechArise';            
    echo "payment failed";
    //$this->load->Template('failed.php',$data);
}
public function payment_order($data,$invoice){
    $data1 = array(
        'ORDERID'=>$invoice,
        'TXNID'=>$data['id'],
        'TXNAMOUNT'=>$data['amount']/100,
        'PAYMENTMODE'=>$data['method'],
        'TXNDATE'=>$data['created_at'],
        'STATUS'=>$data['status'],
        
        'email'=>$data['email'],
        'contact'=>$data['contact'],
        'BANKNAME'=>$data['bank'],
       
        'DATA'=> serialize($data)
    );
    $this->db->insert('payments', $data1);
}
public function add_event($id){

    $uid =$this->session->userdata('userm')->id;
    $sid ='';
    $title ='Booking';
    $start = '';
    $end ='';
    if(isset($this->session->userdata('booking')['uid'])){
        $post = $this->session->userdata('booking');
        $sid= $post['uid'];
       
        $start = $post['date'].' '.$post['time'].':00';
        $end = $post['date'];
    }
    $data = [
        's_id' =>$sid,
        'title' => $title,
        'start' => $start,
        'end' => $end,
        'u_id' =>$uid
    ];
     $this->Mymodel->insert('events',$data);
   
}

public function booking($invoice){
    $uid =$this->session->userdata('userm')->id;
    $title ='Booking 50';
    if(isset($this->session->userdata('booking')['uid'])){
        $post = $this->session->userdata('booking');
        $sid= $post['uid'];
    }
    $data = [
        'sid' =>$sid,
        'name' => $title,
        'booking_no' => $invoice,
        'pay_status' => 1,
        'uid' =>$uid
    ];
     $this->Mymodel->insert('booking',$data);
}

public function ubooking($id){
    $products = $this->Mymodel->get_booking_product($id);
    //echo $this->db->last_query();die;
    $ids = $this->checkDeleviry('55');
    $recommendeds = $this->Mymodel->recommended($ids);
    // $products = $this->Mymodel->get_by_service('products',$id);
    $data = [
        'products' =>$products ,
        'recommendeds' =>$recommendeds,
    ];
    $this->load->userTemplate('booking_product',$data); 
}

public function bookproduct($id){
    if($this->session->userdata('userm') !=''){
        $products = $this->Mymodel->get_booking_product1('appointment_product',$id);
        $fname =$this->session->userdata('userm')->name;
        $lname =$this->session->userdata('userm')->lname;
        $uid =$this->session->userdata('userm')->id;
        $phone =$this->session->userdata('userm')->phone;
        $data = [
            'cname' => $fname.' '.$lname,
            'phone' =>$phone,
            'uid' =>$uid,
            'pname' => $products->name,
            'price' =>$products->pries,
            'sid' =>$products->uid,
            'create_at'=> date('Y-m-d H:i:s'),
            'img_name'=> $products->img_name,
        ];
        $this->Mymodel->insert('appointment_order',$data);
        redirect(base_url());
    }else{
        redirect(base_url('Login'));
}
}

public function services_contract($id =''){
    if($id ==''){
        $service = $this->Mymodel->get_all('service_contract');
        $data = array( 
            'services' =>$service,
        );
    }else{
        $service = $this->Mymodel->get_by_id1('service_contract',$id);
        $data = array( 
            'services' =>$service,
        );
    }


    $this->load->userTemplate('services_contract',$data);
}

public function book_Services($id){
    if($this->session->userdata('userm') !=''){
        $products = $this->Mymodel->get_booking_product1('service_contract',$id);
        $fname =$this->session->userdata('userm')->name;
        $lname =$this->session->userdata('userm')->lname;
        $uid =$this->session->userdata('userm')->id;
        $phone =$this->session->userdata('userm')->phone;
        $data = [
            'cname' => $fname.' '.$lname,
            'phone' =>$phone,
            'uid' =>$uid,
            'pname' => $products->name,
            'pid' => $id,
            'price' =>$products->pries,
            'sid' =>$products->created_by,
            'create_at'=> date('Y-m-d H:i:s'),
            
        ];
        $this->Mymodel->insert('service_contract_order',$data);
        redirect(base_url());
    }else{
        redirect(base_url('Login'));
    }
}

    public function setpin(){
        $this->session->set_userdata('setpin',$_POST);
        $post = $this->session->userdata('setpin');
        // echo "<pre>";
        // echo $this->session->userdata('setpin')['pin'];
    }
    public function fullsearch(Type $var = null){
       $word = $_GET['search_keyword'];
       $this->db->select('name');
       $this->db->from('products');
       $this->db->like('name',$word);
       $this->db->group_by('name');
       $this->db->limit('8');
       $query = $this->db->get();
       // echo $this->db->last_query();exit();
        $re=  $query->result();
       echo json_encode($re);
      
    }
    public function Search_product(Type $var = null){
        $name = $_GET['Search'];
        $products = $this->Mymodel->get_product_name($name);
       // echo $this->db->last_query();exit();
       $id = $this->checkDeleviry('55');
        $recommendeds = $this->Mymodel->recommended($id);
        // $products = $this->Mymodel->get_by_service('products',$id);
        $data = [
            'products' =>$products ,
            'recommendeds' =>$recommendeds,
        ];
       // print_r($data);die;
        $this->load->userTemplate('list_of_products',$data);
    }

    public function checkDeleviry($id){
        $pin = [];
        if($this->session->userdata('userm') !=''){
            $pin = [];
            $pin1 = $this->session->userdata('userm')->pin;
            $pin2 = $pin1-1;
            $pin3 = $pin1+1;
            $pin []= $pin2; 
            $pin []= $pin1; 
            $pin []= $pin3; 
        }
        
        if($this->session->userdata('setpin') !=''){
            $pin = [];
            $pin1 = $this->session->userdata('setpin')['pin'];
            $pin2 = $pin1-1;
            $pin3 = $pin1+1;
            $pin []= $pin2; 
            $pin []= $pin1; 
            $pin []= $pin3;
        }
        
       
        $seller_id = $this->Mymodel->get_product1($id);
        //echo $this->db->last_query();die;
        $seller_ids = [];
        foreach($seller_id as $d){
            if($d->aria == 1){
                if(in_array($d->pin,$pin)){
                    $seller_ids[]= $d->created_by;
                }
            }else if($d->aria == 3){
                
                $delivery_uid = $this->Mymodel->get_delevry_aria($d->created_by);
                foreach($delivery_uid as $u){
                    //$pins = $this->Mymodel->statcity_pin($u->city);
                    //foreach($pins as $p){
                        if(in_array($u->pin,$pin)){
                            $seller_ids[]= $d->created_by;
                        } 
                    //}
                    //echo $this->db->last_query();exit();
                }
                // if(in_array($d->pin,$pin)){
                //     $seller_ids[]= $d->created_by;
                // }  
            }else{
                $seller_ids[]= $d->created_by;
            }
            
        }
        // echo "<pre>";
        // print_r($pin);die;
        return $seller_ids;
    }

    public function blog(){
        $blogs = $this->Mymodel->active_blog();
        $data = ['blogs' =>$blogs];
        $this->load->userTemplate('blogs',$data);
    }
}

?>