<style>
ul.scrollmenu {
  overflow: auto;
  white-space: nowrap;
}
ul.scrollmenu li {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px;
  text-decoration: none;
}
.booktime{
    white-space: nowrap;
    margin: 5px;
}
</style>
<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y">
    <div class="container">
       
        <?php foreach($products as $product){
             $countDates = 0;
             $date = Date('Y-m-d', strtotime("+".$countDates." days"));
            $events = $this->Mymodel->get_event('events',$product->u_id,$date);
            ?>
            <article class="card card-product-list">
                <div class="row no-gutters">
                    <aside class="col-md-3">
                        <a href="#" class="img-wrap">
                            <!-- <span class="badge badge-danger"> </span> -->
                            <img src="<?php echo base_url('assets/images/'.$product->img_name);?>">
                        </a>
                    </aside> <!-- col.// -->
                    <div class="col-md-6">
                        <div class="info-main">
                            <?php $this->db->select('shopname,shopadd,pin');
                            $this->db->from('bussines_partner');
                            $this->db->where('id',$product->u_id);
                            $name =  $this->db->get()->row(); 
                            ?>
                            
                            <a href="#" class="h5 title"> <?php echo ucfirst($name->shopname)?></a>
                            <p> <?php echo ucfirst(readMoreHelper($product->discription))?></p>
                        </div> <!-- info-main.// -->
                    </div> <!-- col.// -->
                    <aside class="col-sm-3">
                        <div class="info-aside">
                        
                            <?php if($product->bussines_module == 7){?>
                            <div _ngcontent-c38="" class="consultation-fee ">Amount <span _ngcontent-c38="" class="text-info">Rs
                                50</span>
                            </div>
                            <?php } ?>
                            <div class="price-wrap">
                                <p class="h5 title">Timeing</p>
                                <span class=" price"><?php if(!empty($product->opendays)){ echo $product->opendays." " ;}?></span> <br>
                                <i class="fas fa-map-marker-alt" aria-hidden="true"></i><?php echo ucfirst($name->shopadd).' '.$name->pin?>
                                 
                            </div> <!-- price-wrap.// -->
                            <p class="mt-3">
                                <?php if($product->bussines_module == '7'){?>
                                    <button data-id  = "<?php echo 'myid_'.$product->u_id?>"  class="btn btn-primary mybullton" > 
                                       </i><span class="text">Book Appointment </span> 
                                    </button>
                                <?php } else{ ?>
                                    <a href ="<?php echo base_url('Home/ubooking/').$product->u_id?>" class="btn btn-primary" type = "submit"> 
                                       </i><span class="text">Our Services</span>  
                                    </a>
                                <?php } ?>
                            </p>
                        </div> <!-- info-aside.// -->
                    </aside> <!-- col.// -->
                </div> <!-- row.// -->
               
            </article> <!-- card-product .// -->
            <div class="row no-gutters" style = "display:none; margin-bottom:10px" id  = "<?php echo 'myid_'.$product->u_id?>">
                <?php
                if($product->bussines_module == '7'){?>
                <div class = "card col-sm-12">
                    <div class="card-header">
                       <div class = "">
                            <ul class="scrollmenu">
                                <li class="nav-item">
                                    <a class="navbar-brand" >Booking Date</a>  
                                </li>
                                <?php
                                    $weekday = $product->opendays;
                                    if($product->opendays ==''){
                                        $weekday = [];
                                    }else{
                                        $weekday = explode(" ",$product->opendays);
                                        $weekday = explode((','),$weekday[0]);
                                    }
                                    $countDates = 0;                                 
                                    $max_dates = 11;
                                    
                                    while ($countDates < $max_dates) {
                                        $NewDate=Date('F d', strtotime("+".$countDates." days")); 
                                        $buttonvalue=Date('Y-m-d', strtotime("+".$countDates." days")); 
                                        $nameOfDay = date('l', strtotime($NewDate));
                                        if(in_array($nameOfDay, $weekday)){
                                        ?>
                                        <li class="">
                                            <button data-id = "<?php echo $product->u_id;?>" class="btn btn-outline-success my-2 my-sm-0 bookdate <?php if($countDates == 0){echo 'active';}?>" value = "<?php echo $buttonvalue;?>"><?php echo $NewDate; ?></button>
                                        </li>
                                        <?php 
                                        }
                                        $countDates += 1;
                                    }
                                ?>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="bookingsloat_<?php echo $product->u_id?>" >
                                <?php $countDates =0;
                                $y=Date('Y-m-d', strtotime("+".$countDates." days")); ?>
                                <div class="alert alert-info" >
                                    <div class="row">
                                        <div class="col-sm-2 text-center">
                                            <i class="fa fa-coffee" aria-hidden="true"></i><br>
                                                Morning
                                        </div>
                                        <div class="col-sm-10">
                                            <?php 
                                            $mb1=$product->morning_booking;
                                            $mb = preg_replace('/\D/', '', $mb1);
                                            $mt1 = $product->morning_time;
                                            $mt = preg_replace('/\D/', '', $mt1);
                                            $md = ($mt*60) /$mb;
                                            if($product->morning_time =="m4"){
                                                $selectedTime = "08:00";
                                            }
                                            if($product->morning_time =="m3"){
                                                $selectedTime = "09:00";
                                            }
                                            if($product->morning_time =="m2"){
                                                $selectedTime = "10:00";
                                            }
                                            if($product->morning_time =="m1"){
                                                $selectedTime = "11:00";
                                            }
                                            $start = [];
                                            foreach($events as $event){
                                                $start[]= $event->time;
                                            }
                                            
                                            for($i = 0; $i < $mb; $i++){
                                                $endTime = strtotime("+".$md. "minutes", strtotime($selectedTime));
                                                $e =  date('h:i', $endTime);
                                                if(in_array($selectedTime,$start)){}else{
                                                echo '<button data-date = "'.$y.'" data-id = "'.$product->u_id.'"class="btn btn-outline-success my-2 my-sm-0 booktime" value = '.$selectedTime.'>'.$selectedTime.'-'.$e.'</button>';
                                                }
                                                $selectedTime = $e;
                                            }  
                                            ?>
                                        
                                        </div>
                                    </div>
                               
                                </div>
                                <div class="alert alert-warning" >
                                    <div class="row">
                                        <div class="col-sm-2 text-center">
                                        <i class="fa fa-sun-o" aria-hidden="true"></i><br>
                                            Afternoon
                                        </div>
                                        <div class="col-sm-10">
                                            <?php 
                                            $ab1=$product->after_booking;
                                            $ab = preg_replace('/\D/', '', $ab1);
                                            $at1 = $product->after_time;
                                            $at = preg_replace('/\D/', '', $at1);
                                            $ad = ($at*60) /$ab;
                                            if($product->after_time =="a4"){
                                                $selectedTime = "01:00";
                                            }
                                            if($product->after_time =="a5"){
                                                $selectedTime = "02:00";
                                            }
                                            if($product->after_time =="a2"){
                                                $selectedTime = "01:00";
                                            }
                                            if($product->morning_time =="a3"){
                                                $selectedTime = "02:00";
                                            }
                                            $start = [];
                                            foreach($events as $event){
                                                $start[]= $event->time;
                                            }
                                            for($i = 0; $i < $ab; $i++){
                                                $endTime = strtotime("+".$ad. "minutes", strtotime($selectedTime));
                                                $e =  date('h:i', $endTime);
                                                if(in_array($selectedTime,$start)){}else{
                                                echo '<button data-date = "'.$y.'" data-id = "'.$product->u_id.'"class="btn btn-outline-success my-2 my-sm-0 booktime" value = '.$selectedTime.'>'.$selectedTime.'-'.$e.'</button>';
                                                }
                                                $selectedTime = $e;
                                            }  
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="alert alert-dark" >
                                    <div class="row">
                                        <div class="col-sm-2 text-center">
                                        <i class="fa fa-moon-o" aria-hidden="true"></i></i><br>
                                        Evening
                                        </div>
                                        <div class="col-sm-10">
                                            <?php 
                                            $eb1=$product->evning_booking;
                                            $eb = preg_replace('/\D/', '', $eb1);
                                            $et1 = $product->evning_time;
                                            $et = preg_replace('/\D/', '', $et1);
                                            $ed = ($et*60) /$eb;
                                            if($product->evning_time =="e4" || $product->evning_time =="e5" || $product->evning_time =="e6"){
                                                $selectedTime = "04:00";
                                            }
                                            if($product->evning_time =="e3" || $product->evning_time =="ee5" || $product->evning_time =="ee4"){
                                                $selectedTime = "05:00";
                                            }
                                            if($product->evning_time =="e2" || $product->evning_time =="ee3" || $product->evning_time =="eee4" ){
                                                $selectedTime = "06:00";
                                            }
                                            
                                            $start = [];
                                            foreach($events as $event){
                                                $start[]= $event->time;
                                            }
                                            for($i = 0; $i < $eb; $i++){
                                                $endTime = strtotime("+".$ed. "minutes", strtotime($selectedTime));
                                                $e =  date('h:i', $endTime);
                                                if(in_array($selectedTime,$start)){}else{
                                                echo '<button data-date = "'.$y.'" data-id = "'.$product->u_id.'"class="btn btn-outline-success my-2 my-sm-0 booktime" value = '.$selectedTime.'>'.$selectedTime.'-'.$e.'</button>';
                                                }
                                                $selectedTime = $e;
                                            }  
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </div>
                  <?php  }
                ?>
                </div>
        <?php } ?>
        
    </div> <!-- container .//  -->
</section>
<!-- =============== SECTION ITEMS =============== -->
<!-- <section  class="padding-bottom-sm">

<header class="section-heading heading-line">
	<h4 class="title-section text-uppercase">Recommended items</h4>
</header>

<div class="row row-sm">
	<?php
		foreach($recommendeds as $recommend){?>
        <?php $images = $this->db->get_where('images',['product_id'=>$recommend->id])->row(); ?>
			<div class="col-xl-2 col-lg-3 col-md-4 col-6">
				<div  class="card card-sm card-product-grid">
					<a href="<?php echo base_url('User/product_details/'.$recommend->id)?>" class="img-wrap"> <img src="<?php echo base_url('assets/images/thumbnails/'.$images->img_name);?>"> </a>
					<figcaption class="info-wrap">
						<a href="<?php echo base_url('User/product_details/'.$recommend->id)?>" class="title"><?php echo $recommend->name;?></a>
						<div class="price mt-1">&#x20B9;<?php echo $recommend->pries;?></div> 
					</figcaption>
				</div>
			</div> 
			<?php
		}
	?>
	
</div> 
</section> -->
<!-- =============== SECTION ITEMS .//END =============== -->
<!-- <nav class="mb-4" aria-label="Page navigation sample">
  <ul class="pagination">
    <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
    <li class="page-item active"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">4</a></li>
    <li class="page-item"><a class="page-link" href="#">5</a></li>
    <li class="page-item"><a class="page-link" href="#">Next</a></li>
  </ul>
</nav> -->
<?php
function readMoreHelper($story_desc, $chars = 500) {
    $story_desc = substr($story_desc,0,$chars);  
    $story_desc = substr($story_desc,0,strrpos($story_desc,' '));  
    $story_desc = $story_desc."......";  
    return $story_desc;  
}

?>
<script>

    $(".mybullton").click(function(){
     p = $(this).data("id") 
      $("#"+p).toggle();
    });
    
    $(".bookdate").click(function(){
        p = $(this).val() 
        v = $(this).data("id")
      
        Url = "<?php echo base_url('Home/addbooking11'); ?>";
         $(".bookdate").removeClass("active");
        $( this ).addClass( "active" );
        $.ajax({
            type: "POST",
            url: Url,
            data: {uid:v,date: p},
            success: function (response) {
           
              // alert(response);
               $(".bookingsloat_"+v  ).html(response);
            
            }
        });
       
        
    });

    $(document).on('click','.booktime',function(){
    //$(".booktime").click(function(){
     time = $(this).val() 
     uid = $(this).data("id")
     date = $(this).data("date")
    
     
      $(".booktime").removeClass("active");
      $( this ).addClass( "active" );
     Url = "<?php echo base_url('Home/addbooking'); ?>";

    $.ajax({
        type: "POST",
        url: Url,
        data: {time : time, uid:uid, date: date},
        success: function (response) {
           
               //alert(response);
               location.href= "<?php echo base_url('Home/addbooking1');?>"
            
        }
    });

    });


</script>
