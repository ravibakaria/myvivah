<style>
.section-footer ul a {
    color: inherit !important;
}
.padding-y-lg {
    padding-left: 20px;
}
</style>
<div class="add-to">
				<button class="add-to-btn">Add to home screen</button>
			</div>
</div><!-- //container -->
<!-- ========================= FOOTER ========================= -->
<footer class="section-footer bg-secondary">
	<div class="container">
		<section class="footer-top padding-y-lg text-white">
			<div class="row justify-content-md-center">
      
				<!-- <aside class="col-md col-6">
					<h6 class="title">Brands</h6>
					<ul class="list-unstyled">
						<li> <a href="#">Adidas</a></li>
						<li> <a href="#">Puma</a></li>
						<li> <a href="#">Reebok</a></li>
						<li> <a href="#">Nike</a></li>
					</ul>
				</aside> -->
        <aside class="col-md-3">
					<h6 class="title">Social</h6>
					<ul class="list-unstyled">
						<li><a href="https://www.facebook.com/quests.company/"> <i class="fab fa-facebook"></i> Facebook </a></li>
						<li><a href="#"> <i class="fab fa-twitter"></i> Twitter </a></li>
						<li><a href="https://www.instagram.com/the_quests_company/"> <i class="fab fa-instagram"></i> Instagram </a></li>
						<li><a href="https://www.youtube.com/channel/UC7TixzPyhWXF33RsWDhX5SA"> <i class="fab fa-youtube"></i> Youtube </a></li>
					</ul>
				</aside>
				<aside class="col-md-3">
					<h6 class="title">Company</h6>
					<ul class="list-unstyled">
						<li> <a href="#">About us</a></li>
            <li> <a href="#">Contact us</a></li>
						<li> <a href="#">Career</a></li>
            <li> <a href="<?php echo base_url('Home/blog')?>">Blog</a></li>
						<!-- <li> <a href="#">Find a store</a></li>
						<li> <a href="#">Rules and terms</a></li>
						<li> <a href="#">Sitemap</a></li> -->
					</ul>
				</aside>
				
				<aside class="col-md-3">
					<h6 class="title">Account</h6>
					<ul class="list-unstyled">
						<li> <a href="<?php echo base_url('Business_partner/register')?>"> Sell With Us </a></li>
						<li> <a href="<?php echo base_url('Referral/register')?>"> Refer And Earn </a></li>
						<!-- <li> <a href="#"> Account Setting </a></li> -->
						<li> <a href="<?php echo base_url('Login')?>"> User Login </a></li>
					</ul>
				</aside>
				
			</div> <!-- row.// -->
		</section>	<!-- footer-top.// -->

		<section class="footer-bottom text-center">
		
				<p class="text-white">Privacy Policy - Terms of Use - User Information Legal Enquiry Guide</p>
				<p class="text-white"> &copy 2020 quests.co.in, All rights reserved </p>
				<br>
		</section>
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->

  <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script src=" <?php echo base_url('assets/js/app.js');?>"></script>
  <script src=" <?php echo base_url("assets/js/service-worker.js");?>"></script>
</body>
<script type="text/javascript">
//  if ('serviceWorker' in navigator) {
//     console.log("Will the service worker register?");
//     navigator.serviceWorker.register('<?php echo base_url("assets/js/service-worker.js");?>')
//       .then(function(reg){
//         console.log("Yes, it did.");
//      }).catch(function(err) {
//         console.log("No it didn't. This happened:", err)
//     });
//  }
</script>
<script type="text/javascript">
	if ('serviceWorker' in navigator) {
	  window.addEventListener('load', function() {
	    navigator.serviceWorker.register('<?php echo base_url("assets/js/service-worker.js");?>').then(function(registration) {
	      // Registration was successful
	      console.log('ServiceWorker registration successful with scope: ', registration.scope);
	    }, function(err) {
	      // registration failed :(
	      console.log('ServiceWorker registration failed: ', err);
	    });
	  });
	}

	let deferredPrompt;
	var div = document.querySelector('.add-to');
	var button = document.querySelector('.add-to-btn');
	div.style.display = 'none';
 
	// window.addEventListener('beforeinstallprompt', (e) => {
  //   console.log(button);
  // console.log(div);
  //   console.log('beforeinstallprompt');
	//   // Prevent Chrome 67 and earlier from automatically showing the prompt
	//   e.preventDefault();
	//   // Stash the event so it can be triggered later.
	//   deferredPrompt = e;
	//   div.style.display = 'block';

	//   button.addEventListener('click', (e) => {
	//   // hide our user interface that shows our A2HS button
	// //  div.style.display = 'none';
  //   // Show the prompt
	//   deferredPrompt.prompt();
	//   // Wait for the user to respond to the prompt
	//   deferredPrompt.userChoice
	//     .then((choiceResult) => {
	//       if (choiceResult.outcome === 'accepted') {
	//         console.log('User accepted the A2HS prompt');
	//       } else {
	//         console.log('User dismissed the A2HS prompt');
	//       }
	//       deferredPrompt = null;
	//     });
	// });
  // });
  
  window.addEventListener('beforeinstallprompt', function(event) {
      e.preventDefault();
      deferredPrompt = e;
    });
    button.addEventListener('click', (e) => {
      button.style.display = 'none';
      deferredPrompt.prompt();
      deferredPrompt.userChoice
        .then((choiceResult) => {
          if (choiceResult.outcome === 'accepted') {
            console.log('User accepted the A2HS prompt');
          } else {
            console.log('User dismissed the A2HS prompt');
          }
          deferredPrompt = null;
        });
    });

</script>
<script>
$(document).ready(function(){
	$(".setpin").click(function(){

		var id = $('#setpin').val();
		if(id ==''){
			alert("Enter pin code");
			return false;
		}
		//alert(id);
		$.ajax({ 
			url: "<?= base_url('Home/setpin')?>",
			data: {'pin':id},
			type: 'post',
			success: function(data){
				//alert(data);
				location.reload();
			}
		});
	});
});
</script>
<script>
function autocomplete(inp, arr) {
  
  var currentFocus;
  
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
     
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
    
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
     
      this.parentNode.appendChild(a);
     
      for (i = 0; i < arr.length; i++) {
       
        //if (arr[i]['name'].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
			
          b = document.createElement("DIV");
          
         // b.innerHTML = "<strong>" + arr[i]['name']+ "</strong>";
          b.innerHTML += arr[i]['name'];
        
          b.innerHTML += "<input type='hidden' value='" + arr[i]['name'] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
         
        
      }
     
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      }
      //  else if (e.keyCode == 13) {
      //   /*If the ENTER key is pressed, prevent the form from being submitted,*/
      //   e.preventDefault();
      //   if (currentFocus > -1) {
      //     /*and simulate a click on the "active" item:*/
      //     if (x) x[currentFocus].click();
          
      //   }
      // }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
    
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
      document.getElementById("myInput").focus();
  });
}

/*An array containing all the country names in the world:*/
var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/

$("#myInput").keyup(function(){
	var minlength = 0;
	value = $(this).val();
	if (value.length >= minlength ) {
		$.ajax({
			type: "GET",
			url: "<?php echo base_url('Home/fullsearch')?>",
			data: {'search_keyword' : value},
			dataType: "text",
			success: function(msg)
			{ data = JSON.parse(msg);
				
				autocomplete(document.getElementById("myInput"), data);
			}
		});
	}
	
});


var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };
</script>

</html>