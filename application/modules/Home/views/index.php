
<style>
@media only screen and (max-width: 600px)  {
	#carousel1_indicator{
		display: none;
	}
}
.popularbtn{
        background-color: #FF4501;
        margin-top: 23px;
}

.btn-outline-primary {
    border-color: #FF4501!important;
}
.btn-outline-primary:hover{
    background-color: #FF4501!important;
}
</style>
<div class="row">
<div class="col-lg-12">
<div id="carousel1_indicator" class="slider-home-banner carousel slide" data-ride="carousel">
<!-- <ol class="carousel-indicators">
<li data-target="#carousel1_indicator" data-slide-to="0" class="active"></li>
<li data-target="#carousel1_indicator" data-slide-to="1"></li>
<li data-target="#carousel1_indicator" data-slide-to="2"></li>
</ol> -->
<div class="carousel-inner">
<?php 
if(!empty($banners)){
$i = 1;
foreach($banners as $banner){?>
<div class="carousel-item <?php if($i==1){echo 'active';}?>">
<img src="<?php echo base_url('assets/images/'.$banner['name'])?>" class="img-fluid">
</div>
<?php $i++; 
}
}
?>

</div>
<a class="carousel-control-prev" href="#carousel1_indicator" role="button" data-slide="prev">
<span class="carousel-control-prev-icon" aria-hidden="true"></span>
<span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carousel1_indicator" role="button" data-slide="next">
<span class="carousel-control-next-icon" aria-hidden="true"></span>
<span class="sr-only">Next</span>
</a>
</div> 
</div>
</div>

	<!-- ========================= SECTION MAIN  ========================= -->
	<section class="section-main padding-y">
		<main class="card">
			<div class="card-body">
				<div class="row">
					<!-- <aside class="col-lg col-md-3 ">
						<h6>MY MARKETS</h6>
						<nav class="nav-home-aside">
							<ul class="menu-category">
								<li><a href="<?php echo base_url('Home/show_all_produuct_categories/55')?>">Fashion and clothes</a></li>
								<li><a href="#">Automobile and motors</a></li>
								<li><a href="#">Gardening and agriculture</a></li>
								<li><a href="<?php echo base_url('Home/show_all_produuct_categories/26')?>">Electronics and tech</a></li>
								<li><a href="#">Packaginf and printing</a></li>
								<!-- <li><a href="#">Home and kitchen</a></li> -->
							<!--	<li><a href="#">Digital goods</a></li>
								<li><a href="#">Home Interior</a></li>
								<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Accounts</a>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="<?php echo base_url('Referral')?>">Referrals</a>
									<a class="dropdown-item" href="<?php echo base_url('Business_partner')?>">Sellars</a>
									
								</div>
								</li>
							</ul>
						</nav>
					</aside> -->
					<aside class="special-home-right col-lg col-md-3 ">
						<h6 class="bg-blue text-center text-white mb-0 p-2">Popular category</h6>
						<?php 
							if(!empty($popular_category)){
								foreach($popular_category as $category){?>
								<?php $images = $this->db->get_where('images',['product_id'=>$category->id])->row(); ?>
									<div class="card-banner border-bottom">
										<div class="py-3" style="width:80%">
											<h6 class="card-title"><?php echo $category->servicename ?></h6>
											<a href="<?php echo base_url('User/product_details/'.$category->id)?>" class="btn popularbtn btn-sm"> Source now </a>
										</div> 
										<img src="<?php echo base_url('assets/images/thumbnails/'.$images->img_name)?>" height="80" width ="80" class="img-bg">
									</div>
									<?php 
								}
							}
						?>

					</aside>
					<div class="col-md-9 col-xl-7 col-lg-7">

						<!-- ================== COMPONENT SLIDER  BOOTSTRAP  ==================  -->
						<div id="carousel1_indicator" class="slider-home-banner carousel slide" data-ride="carousel">
						<div class="carousel-inner">
						<?php 
							if(!empty($popular_category)){
								$i = 1;
								foreach($popular_category as $category){?>
								<?php $images = $this->db->get_where('images',['product_id'=>$category->id])->row(); ?>
									<div class="carousel-item <?php if($i==1){echo 'active';}?>">
										<img src="<?php echo base_url('assets/images/'.$images->img_name)?>" class="img-fluid" height="80" width ="80">
										<div class = "text-center" style="margin-top: -127px;">
										<h4><?php echo $category->name?></h4>
										<p class="item-price"><strike>&#x20B9;<?php echo $category->showpries?>.00</strike> <b><span>&#x20B9;<?php echo $category->pries?>.00</span></b></p>
										<form method = "post" action="<?= base_url().'User/add_cart'?>">
			
											<input type="hidden" name="id" value= "<?= $category->id?>">
											<input type="hidden" class="form-control" value="1" name="qty">
											<div class="form-group col-md">
												<button class="btn btn-primary" type = "submit"> 
													<i class="fa fa-shopping-cart"></i><span class="text">Add to cart</span> 
												</button>
											</div> 
										</form>
										</div>
									</div>
									
									<?php $i++; 
								}
							}
						?>
							
						</div>
						
						</div> 
					<!-- ==================  COMPONENT SLIDER BOOTSTRAP end.// ==================  .// -->	

				</div>
				<div class="col-md d-none d-lg-block flex-grow-1">
					<aside class="special-home-right">
						<h6 class="bg-blue text-center text-white mb-0 p-2">Popular category</h6>
						<?php 
							if(!empty($popular_category)){
								foreach($popular_category as $category){?>
								<?php $images = $this->db->get_where('images',['product_id'=>$category->id])->row(); ?>
									<div class="card-banner border-bottom">
										<div class="py-3" style="width:80%">
											<h6 class="card-title"><?php echo $category->servicename ?></h6>
											<a href="<?php echo base_url('User/product_details/'.$category->id)?>" class="btn popularbtn btn-sm"> Source now </a>
										</div> 
										<img src="<?php echo base_url('assets/images/thumbnails/'.$images->img_name)?>" height="80" width ="80" class="img-bg">
									</div>
									<?php 
								}
							}
						?>

					</aside>
				</div> <!-- col.// -->
			</div> <!-- row.// -->
		</div> <!-- card-body.// -->
	</main> <!-- card.// -->

</section>

<!-- ========================= SECTION MAIN END// ========================= -->
<!-- =============== SECTION DEAL =============== -->
<section class="padding-bottom">
 <div class="card card-deal">
 <div class="row no-gutters items-wrap">
     <div class=" col-md col-3 col-heading content-body text-center">
      <header class="section-heading">
       <h3 class="section-title">Deals and offers</h3>
       <p>Hygiene equipments</p>
     </header>
     <div class="timer">
       <div> <span id ="days" class="num">04</span> <small>Days</small></div>
       <div> <span id = "hours" class="num">12</span> <small>Hours</small></div>
       <div> <span id = "min" class="num">58</span> <small>Min</small></div>
       <div> <span id = "sec" class="num">02</span> <small>Sec</small></div>
     </div>
   </div> 
   
    <div class="col-md col-6">
     <figure class="card-product-grid card-sm">
      <a href="#" class="img-wrap"> 
       <img src="<?php echo base_url('assets/items/4.jpg')?>"> 
      </a>
      <div class="text-wrap p-3">
       	<a href="#" class="title">Summer clothes</a>
       	<span class="badge badge-danger"> -20% </span>
      </div>
   </figure>
 </div> 
 <div class="col-md col-6">
   <figure class="card-product-grid card-sm">
    <a href="#" class="img-wrap"> 
     <img src="<?php echo base_url('assets/items/4.jpg')?>"> 
   </a>
   <div class="text-wrap p-3">
     <a href="#" class="title">Some category</a>
     <span class="badge badge-danger"> -5% </span>
   </div>
 </figure>
</div> 
<div class="col-md col-6">
 <figure class="card-product-grid card-sm">
  <a href="#" class="img-wrap"> 
   <img src="<?php echo base_url('assets/items/5.jpg')?>"> 
 </a>
 <div class="text-wrap p-3">
   <a href="#" class="title">Another category</a>
   <span class="badge badge-danger"> -20% </span>
 </div>
</figure>
</div>
<div class="col-md col-6">
 <figure class="card-product-grid card-sm">
  <a href="#" class="img-wrap"> 
   <img src="<?php echo base_url('assets/items/6.jpg')?>"> 
 </a>
 <div class="text-wrap p-3">
   <a href="#" class="title">Home apparel</a>
   <span class="badge badge-danger"> -15% </span>
 </div>
</figure>
</div> <!-- col.// -->
<div class="col-md col-6">
 <figure class="card-product-grid card-sm">
  <a href="#" class="img-wrap"> 
   <img src="<?php echo base_url('assets/items/7.jpg')?>"> 
 </a>
 <div class="text-wrap p-3">
   <a href="#" class="title text-truncate">Smart watches</a>
   <span class="badge badge-danger"> -10% </span>
 </div>
</figure>
</div> <!-- col.// -->
</div>
</div>

</section>
<!-- =============== SECTION DEAL // END =============== -->
<!-- =============== SECTION 1 =============== -->
<section class="padding-bottom">
<header class="section-heading heading-line">
	<h4 class="title-section text-uppercase">Apparel</h4>
</header>

<div class="card card-home-category">
<div class="row no-gutters">
	<div class="col-md-3">
	
	<div class="home-category-banner bg-light-orange">
		<h5 class="title">Best trending clothes</h5>
		<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
		<a href="#" class="btn btn-outline-primary rounded-pill">Source now</a>
		<img src="<?php echo base_url('assets/items/2.jpg')?>" class="img-bg">
	</div>

	</div> <!-- col.// -->
	<div class="col-md-9">
<ul class="row no-gutters bordered-cols">
	<?php if(!empty($clothes)){
			foreach($clothes as $product){?>
			<?php $images = $this->db->get_where('images',['product_id'=>$product->id])->row(); ?>
				<li class="col-6 col-lg-3 col-md-4">
					<a href="<?php echo base_url('User/product_details/'.$product->id)?>" class="item"> 
						<div class="card-body">
							<h6 class="title"><?php echo $product->name;?> </h6>
							<img class="img-sm float-right" src="<?php echo base_url('assets/images/thumbnails/'.$images->img_name)?>"> 
						</div>
					</a>
				</li>
				<?php 
			}
		}else{ ?>
				<li class="col-6 text-center">
						<div class="card-body">
							<h2>Products CommingSoon </h2>
						</div>
				</li>
		<?php }
	?>
	
	

</ul>
	</div> <!-- col.// -->
</div> <!-- row.// -->
</div> <!-- card.// -->
</section>
<!-- =============== SECTION 1 END =============== -->

<!-- =============== SECTION 2 =============== -->
<section class="padding-bottom">
<header class="section-heading heading-line">
	<h4 class="title-section text-uppercase">Electronics</h4>
</header>

<div class="card card-home-category">
<div class="row no-gutters">
	<div class="col-md-3">
	
	<div class="home-category-banner bg-light-orange">
		<h5 class="title">Machinery items for manufacturers</h5>
		<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
		<a href="#" class="btn btn-outline-primary rounded-pill">Source now</a>
		<img src="<?php echo base_url('assets/items/14.jpg')?>" class="img-bg">
	</div>

	</div> <!-- col.// -->
<div class="col-md-9">
	<ul class="row no-gutters bordered-cols">
		<?php if(!empty($electronics)){
			foreach($electronics as $product){?>
			<?php $images = $this->db->get_where('images',['product_id'=>$product->id])->row(); ?>
				<li class="col-6 col-lg-3 col-md-4">
					<a href="<?php echo base_url('User/product_details/'.$product->id)?>" class="item"> 
						<div class="card-body">
							<h6 class="title"><?php echo $product->name;?> </h6>
							<img class="img-sm float-right" src="<?php echo base_url('assets/images/thumbnails/'.$images->img_name)?>"> 
						</div>
					</a>
				</li>
				<?php 
			}
		}else{ ?>
				<li class="col-6 text-center">
						<div class="card-body">
							<h2>Products CommingSoon </h2>
						</div>
				</li>
		<?php }
	?>
		
	</ul>
</div> <!-- col.// -->
</div> <!-- row.// -->
</div> <!-- card.// -->
</section>
<!-- =============== SECTION 2 END =============== -->

<!-- =============== SECTION ITEMS =============== -->
<section  class="padding-bottom-sm">

<header class="section-heading heading-line">
	<h4 class="title-section text-uppercase">Recommended items</h4>
</header>

<div class="row row-sm">
	<?php
		foreach($recommendeds as $recommend){?>
		
		<?php $images = $this->db->get_where('images',['product_id'=>$recommend->id])->row(); ?>
			<div class="col-xl-2 col-lg-3 col-md-4 col-6">
				<div  class="card card-sm card-product-grid">
					<a href="<?php echo base_url('User/product_details/'.$recommend->id)?>" class="img-wrap"> <img src="<?php echo base_url('assets/images/thumbnails/'.$images->img_name);?>"> </a>
					<figcaption class="info-wrap">
						<a href="<?php echo base_url('User/product_details/'.$recommend->id)?>" class="title"><?php echo $recommend->name;?></a>
						<div class="price mt-1">&#x20B9;<?php echo $recommend->pries;?></div> <!-- price-wrap.// -->
					</figcaption>
				</div>
			</div> 
			<?php
		}
	?>
	
</div> <!-- row.// -->
</section>
<!-- =============== SECTION ITEMS .//END =============== -->


<!-- =============== SECTION SERVICES =============== -->
<section  class="padding-bottom">

<header class="section-heading heading-line" id = "Services">
	<h4 class="title-section text-uppercase">Trade services</h4>
</header>

<div class="row">
	<div class="col-md-3 col-sm-6">
	<a href="<?php echo base_url('Home/show_all_doctors');?>">
		<article class="card card-post">
		  <img src="<?php echo base_url('assets/items/doctor.jpg')?>" class="card-img-top">
		  <div class="card-body">
		    <h6 class="title">Doctors</h6>
		    <p class="small text-uppercase text-muted">Order protection</p>
		  </div>
		</article> <!-- card.// -->
		</a>
	</div> <!-- col.// -->
	<div class="col-md-3 col-sm-6">
	
		<article class="card card-post">
		  <img src="<?php echo base_url('assets/items/plumber.jpg')?>" class="card-img-top">
		  <div class="card-body">
		    <h6 class="title">Plumbers</h6>
		    <p class="small text-uppercase text-muted">Plumber Plumbing Drain Home repair </p>
		  </div>
		</article> <!-- card.// -->
		
	</div> <!-- col.// -->
	<div class="col-md-3 col-sm-6">
		<article class="card card-post">
		  <img src="<?php echo base_url('assets/img/3.jpg')?>" class="card-img-top">
		  <div class="card-body">
		    <h6 class="title">Inspection solution</h6>
		    <p class="small text-uppercase text-muted">Easy Inspection</p>
		  </div>
		</article> <!-- card.// -->
	</div> <!-- col.// -->
	<div class="col-md-3 col-sm-6">
		<article class="card card-post">
		  <img src="<?php echo base_url('assets/img/4.jpg')?>" class="card-img-top">
		  <div class="card-body">
		    <h6 class="title">Ocean and Air Shipping</h6>
		    <p class="small text-uppercase text-muted">Logistic services</p>
		  </div>
		</article> <!-- card.// -->
	</div> <!-- col.// -->
</div> <!-- row.// -->

</section>
<!-- =============== SECTION SERVICES .//END =============== -->

<script>
    function get_time_diff( datetime )
{
    var datetime = typeof datetime !== 'undefined' ? datetime : "2020-05-31 01:02:03.123456";

    var datetime = new Date( datetime ).getTime();
    var now = new Date().getTime();

    if( isNaN(datetime) )
    {
        return "";
    }

    

    if (datetime < now) {
        var milisec_diff = now - datetime;
    }else{
        var milisec_diff = datetime - now;
    }

    var days = Math.floor(milisec_diff / 1000 / 60 / (60 * 24));

    var date_diff = new Date( milisec_diff );
	hours = date_diff.getHours();
	min = date_diff.getMinutes();
	sec = date_diff.getSeconds();
    // hatml =  days + " D "+ date_diff.getHours() + " H " + date_diff.getMinutes() + " M " + date_diff.getSeconds() + " S";
    document.getElementById('days').innerHTML=days;
    document.getElementById('hours').innerHTML=hours;
    document.getElementById('min').innerHTML=min;
    document.getElementById('sec').innerHTML=sec;
}


    setInterval(get_time_diff, 1000);

  </script>