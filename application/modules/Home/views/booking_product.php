<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y">
    <div class="container">
        
        <?php foreach($products as $product){?>
            <article class="card card-product-list">
                <div class="row no-gutters">
                    <aside class="col-md-3">
                        <a href="#" class="img-wrap">
                            <!-- <span class="badge badge-danger"> </span> -->
                            <img src="<?php echo base_url('assets/images/'.$product->img_name);?>">
                        </a>
                    </aside> <!-- col.// -->
                    <div class="col-md-6">
                        <div class="info-main">
                            <a href="#" class="h5 title"> <?php echo ucfirst($product->name)?></a>
                            <p> <?php echo ucfirst($product->discription)?></p>
                        </div> <!-- info-main.// -->
                    </div> <!-- col.// -->
                    <aside class="col-sm-3">
                        <div class="info-aside">
                            <div class="price-wrap">
                                <del><span class="h5 price"><?php if(!empty($product->showpries)){ echo "&#x20B9;".$product->showpries." " ;}?></span> </del>
                                <span class="h5 price"><?php echo "&#x20B9;".$product->pries?></span>
                                <small class="text-muted">/per <?php echo $product->priceonper?></small>
                            </div> <!-- price-wrap.// -->
                            <p class="mt-3">
                            <!-- <form method = "post" action="<?= base_url().'User/add_cart'?>">
			
                                <input type="hidden" name="id" value= "<?= $product->id?>">
                                <input type="hidden" class="form-control" value="1" name="qty">-->
                                <div class="form-group col-md"> 
                                    <a href ="<?php echo base_url('Home/bookproduct/').$product->id?>" class="btn btn-primary" type = "submit"> 
                                        <span class="text">Get Now</span> 
                                    </a>
                                </div> 
                            <!-- </form> -->
                            </p>
                        </div> <!-- info-aside.// -->
                    </aside> <!-- col.// -->
                </div> <!-- row.// -->
            </article> <!-- card-product .// -->
        <?php } ?>
       
    </div> <!-- container .//  -->
</section>
<!-- =============== SECTION ITEMS =============== -->
<section  class="padding-bottom-sm">

<header class="section-heading heading-line">
	<h4 class="title-section text-uppercase">Recommended items</h4>
</header>

<div class="row row-sm">
	<?php
		foreach($recommendeds as $recommend){?>
        <?php $images = $this->db->get_where('images',['product_id'=>$recommend->id])->row(); ?>
			<div class="col-xl-2 col-lg-3 col-md-4 col-6">
				<div  class="card card-sm card-product-grid">
					<a href="<?php echo base_url('User/product_details/'.$recommend->id)?>" class="img-wrap"> <img src="<?php echo base_url('assets/images/thumbnails/'.$images->img_name);?>"> </a>
					<figcaption class="info-wrap">
						<a href="<?php echo base_url('User/product_details/'.$recommend->id)?>" class="title"><?php echo $recommend->name;?></a>
						<div class="price mt-1">&#x20B9;<?php echo $recommend->pries;?></div> <!-- price-wrap.// -->
					</figcaption>
				</div>
			</div> 
			<?php
		}
	?>
	
</div> <!-- row.// -->
</section>
<!-- =============== SECTION ITEMS .//END =============== -->
<nav class="mb-4" aria-label="Page navigation sample">
  <ul class="pagination">
    <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
    <li class="page-item active"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">4</a></li>
    <li class="page-item"><a class="page-link" href="#">5</a></li>
    <li class="page-item"><a class="page-link" href="#">Next</a></li>
  </ul>
</nav>
