<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<?php 
$class = $this->uri->segment(2);
$class1 = $this->uri->segment(1);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3">
    <title>Admin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/admin.css');?>" rel="stylesheet">
</head>
<body>
<div id = "mysidedive" class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <a href="<?php echo base_url('Admin')?>">Quests.co.in</a>
        <div id="close-sidebar">
          <i class="fas fa-times"></i>
        </div>
      </div>
      <div class="sidebar-header">
      <a href="<?php echo base_url('Admin/profile/'.$this->session->userdata('user')->id)?>">
        <div class="user-pic">
          <img class="img-responsive img-rounded" src="<?php echo base_url('assets/images/user.jpg')?>"
            alt="User picture">
        </div>
        <div class="user-info">
          <span class="user-name"><b> <?php echo $this->session->userdata('user')->name ; ?></b> 
           
          </span>
          <span class="user-role"></span>
          <span class="user-status">
            <i class="fa fa-circle"></i>
            <span>Online</span>
          </span>
        </div>
        </a>
      </div>
      <!-- sidebar-header  -->
      <!-- <div class="sidebar-search">
        <div>
          <div class="input-group">
            <input type="text" class="form-control search-menu" placeholder="Search...">
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="fa fa-search" aria-hidden="true"></i>
              </span>
            </div>
          </div>
        </div>
      </div> -->
      <!-- sidebar-search  -->
      <div class="sidebar-menu">
        <ul>
          <li class="header-menu">
            <span>General</span>
          </li>
          <li>
            <a href="<?php echo base_url('Admin')?>">
              <i class="fa fa-Home"></i>
              <span>Home</span>
             
            </a>
          </li>
          <li>
            <a href="<?php echo base_url('Admin/Blog')?>">
              <i class="fa fa-Home"></i>
              <span>Blog</span>
             
            </a>
          </li>
          <li class="sidebar-dropdown <?php if(($class1 == 'Admin' && $class == 'bannersetting') || ($class1 == 'Admin' && $class == 'role') || ($class1 == 'Vendors' && $class == 'add_sub_service')){ echo "Active";} ?>">
            <a href="#">
              <i class="fa fa-tachometer-alt"></i>
              <span>Dashboard</span>
              <!-- <span class="badge badge-pill badge-warning">New</span> -->
            </a>
            <div class="sidebar-submenu" <?php if(($class1 == 'Admin' && $class == 'bannersetting') || ($class1 == 'Admin' && $class == 'role') || ($class1 == 'Vendors' && $class == 'add_sub_service')){ echo "style='display: block;'";} ?>>
              <ul>
                <li>
                <a href="<?php echo base_url('Admin/bannersetting')?>">Site Banner
                    <span class="badge badge-pill badge-success">Pro</span>
                  </a>
                </li>
                <!-- <li>
                  <a href="<?php //echo base_url('Product/gst')?>">GST And Margin</a>
                </li> -->
                <li>
                  <a href="<?php echo base_url('Admin/role')?>">Roles</a>
                </li>
                <li>
                  <a href="<?php echo base_url('Vendors/add_sub_service')?>">Sub Service</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="sidebar-dropdown <?php if(($class1 == 'Product' && $class == 'Gst') || ($class1 == 'Product' && $class == 'order_list') ){ echo "Active";} ?>">
            <a href="#">
              <i class="fa fa-shopping-cart"></i>
              <span>E-commerce</span>
              <!-- <span class="badge badge-pill badge-danger">3</span> -->
            </a>
            <div class="sidebar-submenu" <?php if(($class1 == 'Product' && $class == 'Gst') || ($class1 == 'Product' && $class == 'order_list') ){ echo "style='display: block;'";} ?>>
              <ul>
                <li>
                  <a href="<?php echo base_url('Product/Gst')?>">Activation Charge

                  </a>
                </li>
                <li>
                  <a href="<?php echo base_url('Product/order_list')?>">Orders </a>
                </li>
                
              </ul>
            </div>
          </li>
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="far fa-gem"></i>
              <span>Accounts Payable</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="<?php echo base_url('Admin/referrals')?>">Referrals</a>
                </li>
                <li>
                  <a href="#">Shopkeeper</a>
                </li>
                <!-- <li>
                  <a href="#">Tables</a>
                </li>
                <li>
                  <a href="#">Icons</a>
                </li>
                <li>
                  <a href="#">Forms</a>
                </li> -->
              </ul>
            </div>
          </li>
          <li class="sidebar-dropdown <?php if(($class1 == 'Vendors' && $class == 'service') || ($class1 == 'Vendors' && $class == 'business') ){ echo "Active";} ?>">
            <a href="#">
              <i class="fa fa-globe"></i>
              <span>Business</span>
            </a>
            <div class="sidebar-submenu" <?php if(($class1 == 'Vendors' && $class == 'service') || ($class1 == 'Vendors' && $class == 'business') ){ echo "style='display: block;'";} ?>>
              <ul>
                <li>
                  <a href="<?php echo base_url('Vendors/service')?>">
                    Business Type
                  </a>
                </li>
                <li>
                <a href="<?php echo base_url('Vendors/business')?>">
                  Business Modules
                </a>
                </li>
              </ul>
            </div>
          </li>
          <li class="sidebar-dropdown <?php if(($class1 == 'Vendors' && $class == 'Vendors') || ($class1 == 'Admin' && $class == 'shopkeepers') || ($class1 == 'Admin' && $class == 'getreferrals') ){ echo "Active";} ?>">
            <a href="#">
              <i class="fa fa-globe"></i>
              <span>Management</span>
            </a>
            <div class="sidebar-submenu" <?php if(($class1 == 'Vendors' && $class == 'Vendors') || ($class1 == 'Admin' && $class == 'shopkeepers') || ($class1 == 'Admin' && $class == 'getreferrals') ){ echo "style='display: block;'";} ?>>
              <ul>
                <li>
                  <a href="<?php echo base_url('Vendors/Vendors')?>">Staff Management</a>
                </li>
                <li>
                  <a href="<?php echo base_url('Admin/shopkeepers')?>">
                    Shopkeeper
                  </a>
                </li>
                <li>
                  <a href="<?php echo base_url('Admin/getreferrals')?>">
                    Referrals
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <li class="sidebar-dropdown <?php if(($class1 == 'Product' && $class == 'list_product') || ($class1 == 'Admin' && $class == 'loadRecord') || ($class1 == 'Admin' && $class == 'loadRecord2') ){ echo "Active";} ?>">
            <a href="#">
            <i class="fa fa-folder" aria-hidden="true"></i>
              <span>Live Products</span>
            </a>
            <div class="sidebar-submenu" <?php if(($class1 == 'Product' && $class == 'list_product') || ($class1 == 'Admin' && $class == 'loadRecord') || ($class1 == 'Admin' && $class == 'loadRecord2') ){ echo "style='display: block;'";} ?>>
              <ul>
                <li>
                  <a href="<?php echo base_url('Product/list_product')?>">Direct Selling Products</a>
                </li>
                <li>
                <a href="<?php echo base_url('Admin/service_contract')?>">Service Contract Products</a>
                </li>
                <li>
                <a href="<?php echo base_url('Admin/advertisement')?>">Advertisement Products</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="sidebar-dropdown <?php if(($class1 == 'Product' && $class == 'list_pending_product') || ($class1 == 'Admin' && $class == 'loadRecord1') || ($class1 == 'Admin' && $class == 'loadRecord3') ){ echo "Active";} ?>">
            <a href="#">
            <i class="fa fa-folder" aria-hidden="true"></i>
              <span>Pending Products</span>
            </a>
            <div class="sidebar-submenu" <?php if(($class1 == 'Product' && $class == 'list_pending_product') || ($class1 == 'Admin' && $class == 'loadRecord1') || ($class1 == 'Admin' && $class == 'loadRecord3') ){ echo "style='display: block;'";} ?>>
              <ul>
                <li>
                <a href="<?php echo base_url('Product/list_pending_product')?>">Direct Selling Products Pending</a>
                </li>
                <li>
                <a href="<?php echo base_url('Admin/list_pending_product')?>">Service Contract Products Pending</a>
                </li>
                <li>
                <a href="<?php echo base_url('Admin/list_pending_advertisement')?>">Advertisement Products Pending</a>
                </li>
              </ul>
            </div>
          </li>
        
          <li class="header-menu">
            <span>Extra</span>
          </li>
          <li>
            <a href="<?php echo base_url('Report')?>">
              <i class="fa fa-book"></i>
              <span>Report</span>
              <span class="badge badge-pill badge-primary">Beta</span>
            </a>
          </li>
          <!-- <li>
            <a href="<?php echo base_url('Vendors/Vendors')?>">
              <i class="fa fa-calendar"></i>
              <span>Staff Management</span>
            </a>
          </li> -->
         
          <!-- <li>
            <a href="#">
              <i class="fa fa-folder"></i>
              <span>Examples</span>
            </a>
          </li> -->
        </ul>
      </div>
      <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->
    <div class="sidebar-footer">
      <!-- <a href="#">
        <i class="fa fa-bell"></i>
        <span class="badge badge-pill badge-warning notification">3</span>
      </a>
      <a href="#">
        <i class="fa fa-envelope"></i>
        <span class="badge badge-pill badge-success notification">7</span>
      </a> -->
      <a href="<?php echo base_url('Admin/changepassword')?>">
        <i class="fa fa-cog"></i>
        <span class="badge-sonar"></span>
      </a>
      <a href="<?php echo base_url('Admin/adminlogout')?>">
        <i class="fa fa-power-off"></i>
      </a>
    </div>
  </nav>
  <!-- sidebar-wrapper  -->
<script>

$(document).ready(function(){
    if ($(this).width() < 600) {
     
      $("#mysidedive").removeClass("toggled");
    } else {
      
      $("#mysidedive").addClass("toggled");
    }
  });

</script>
  <main class="page-content">
    <div class="container-fluid">