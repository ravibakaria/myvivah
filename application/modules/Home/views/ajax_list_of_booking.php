<?php $countDates =0;
$y= $date; ?>
<div class="alert alert-info" >
    <div class="row">
        <div class="col-sm-2 text-center">
        <i class="fa fa-coffee" aria-hidden="true"></i><br>
        Morning
        </div>
        <div class="col-sm-10">
        <?php 
            $mb1=$product->morning_booking;
            $mb = preg_replace('/\D/', '', $mb1);
            $mt1 = $product->morning_time;
            $mt = preg_replace('/\D/', '', $mt1);
            $md = ($mt*60) /$mb;
            if($product->morning_time =="m4"){
                $selectedTime = "08:00";
            }
            if($product->morning_time =="m3"){
                $selectedTime = "09:00";
            }
            if($product->morning_time =="m2"){
                $selectedTime = "10:00";
            }
            if($product->morning_time =="m1"){
                $selectedTime = "11:00";
            }
            $start = [];
            foreach($events as $event){
                $start[]= $event->time;
            }
            //print_r($start);
            for($i = 0; $i < $mb; $i++){
                
                   
                $endTime = strtotime("+".$md. "minutes", strtotime($selectedTime));
                $e =  date('h:i', $endTime);
                if(in_array($selectedTime,$start)){}else{
                    echo '<button data-date = "'.$y.'" data-id = "'.$product->u_id.'"class="btn btn-outline-success my-2 my-sm-0 booktime" value = '.$selectedTime.'>'.$selectedTime.'-'.$e.'</button>';
                }
                    $selectedTime = $e;
                
                    
            }
                 
        ?>
        
        </div>
    </div>

</div>
<div class="alert alert-warning" >
    <div class="row">
        <div class="col-sm-2 text-center">
        <i class="fa fa-sun-o" aria-hidden="true"></i><br>
            Afternoon
        </div>
        <div class="col-sm-10">
        <?php 
            $ab1=$product->after_booking;
            $ab = preg_replace('/\D/', '', $ab1);
            $at1 = $product->after_time;
            $at = preg_replace('/\D/', '', $at1);
            $ad = ($at*60) /$ab;
            if($product->after_time =="a4"){
                $selectedTime = "01:00";
            }
            if($product->after_time =="a5"){
                $selectedTime = "02:00";
            }
            if($product->after_time =="a2"){
                $selectedTime = "01:00";
            }
            if($product->morning_time =="a3"){
                $selectedTime = "02:00";
            }
            $start = [];
            foreach($events as $event){
                $start[]= $event->time;
            }
            for($i = 0; $i < $ab; $i++){
                $endTime = strtotime("+".$ad. "minutes", strtotime($selectedTime));
                $e =  date('h:i', $endTime);
                if(in_array($selectedTime,$start)){}else{
                echo '<button data-date = "'.$y.'" data-id = "'.$product->u_id.'"class="btn btn-outline-success my-2 my-sm-0 booktime" value = '.$selectedTime.'>'.$selectedTime.'-'.$e.'</button>';
                }
                $selectedTime = $e;
            }  
        ?>
        </div>
    </div>
</div>
<div class="alert alert-dark" >
    <div class="row">
        <div class="col-sm-2 text-center">
        <i class="fa fa-moon-o" aria-hidden="true"></i></i><br>
        Evening
        </div>
        <div class="col-sm-10">
        <?php 
            $eb1=$product->evning_booking;
            $eb = preg_replace('/\D/', '', $eb1);
            $et1 = $product->evning_time;
            $et = preg_replace('/\D/', '', $et1);
            $ed = ($et*60) /$eb;
            if($product->evning_time =="e4" || $product->evning_time =="e5" || $product->evning_time =="e6"){
                $selectedTime = "04:00";
            }
            if($product->evning_time =="e3" || $product->evning_time =="ee5" || $product->evning_time =="ee4"){
                $selectedTime = "05:00";
            }
            if($product->evning_time =="e2" || $product->evning_time =="ee3" || $product->evning_time =="eee4" ){
                $selectedTime = "06:00";
            }
            
            $start = [];
            foreach($events as $event){
                $start[]= $event->time;
            }
            for($i = 0; $i < $eb; $i++){
                $endTime = strtotime("+".$ed. "minutes", strtotime($selectedTime));
                $e =  date('h:i', $endTime);
                if(in_array($selectedTime,$start)){}else{
                echo '<button data-date = "'.$y.'" data-id = "'.$product->u_id.'"class="btn btn-outline-success my-2 my-sm-0 booktime" value = '.$selectedTime.'>'.$selectedTime.'-'.$e.'</button>';
                }
                $selectedTime = $e;
            }  
        ?>
        </div>
    </div>
</div>