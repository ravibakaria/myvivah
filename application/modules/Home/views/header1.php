<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Quests.co.in: Online Shopping for Electronics, Apparel, Computers, Books, DVDs &amp; more</title>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!-- <link rel="stylesheet" href="css/style.css"/> -->
  <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
</head>

<body>
  <!-- <a id="banner" href="#"></a> -->
  <!-- Main body -->

  <header>
    <a id="nav-top"></a>

    <div id="nav-belt">
      <div class="nav-left">
        <div id="nav-logo">
          <a href="#" class="nav-logo-link"></a>
          <a href="#" class="nav-logo-tagline"></a>
        </div>
      </div>
      <div class="nav-right">
        <div id="nav-holiday">
          <a href="#"></a>
        </div>
      </div>
      <div class="nav-fill">
        <div id="nav-search">
          <form>
            <div class="nav-left">
             
            </div>
            <div class="nav-right">
              <i class="fa fa-search" aria-hidden="true"></i>
              <input type="submit">
            </div>
            <div class="nav-fill">
              <input type="text" autocomplete="off">
            </div>
          </form>
        </div>
      </div>
    </div>

    <nav id="nav-main">
      <div class="nav-left">
        <div class="nav-shop">
          <a class="nav-a" href="#">
            Departments
            <i class="fa fa-caret-down" aria-hidden="true"></i>
          </a>
        </div>
      </div>
      <div class="nav-right">
        <!-- <a class="nav-a" href="#">
          <span>EN</span>
          <i class="fa fa-globe" aria-hidden="true"></i>
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </a> -->
        <?php if($this->session->userdata('userm') !=''){ 
          $username1 = $this->session->userdata('userm')->name;
          // $username = substr($username1, 0, strpos($username1, '@'));
          ?>
          <a  href="#" style="text-transform: capitalize;" class="nav-link dropdown-toggle" data-toggle="dropdown">
            <span>Hello. <?php echo $username1 ; ?></span>
            Accounts &amp; Lists
            <i class="fa fa-caret-down" aria-hidden="true"></i>
          </a>
          <div class="dropdown-menu profile-menu">
          <ul class="">
            <li><a href="#" class="dropdown-item">
                 Profile
              </a></li>
            <li> <a href="#" class="dropdown-item">
                Change Password
              </a></li>
            <li> <a href="<?php echo base_url('Login/logout');?>" class="dropdown-item">
                Logout
              </a></li>
          </ul>
              
             
            </div>
        <?php } else{ ?>
          <a class="nav-a" href="<?php echo base_url('Login');?>">
            Sign in
          </a>

        <?php } ?>

        <a class="nav-a" href="#">
          Orders 
        </a>

        <a class="nav-a" href="#">
          Try Prime
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </a>

        <a class="nav-a cart" href="#">
          <span>0</span>
          Cart
        </a>
      </div>
      <div class="nav-fill">
        <ul>
          <li><a href="#">Quests.co.in</a></li>
          <li><a href="#">Today's Deals</a></li>
          <li><a href="#">Gift Cards &amp; Registry</a></li>
          <li><a href="#">Sell</a></li>
          <li><a href="#">Help</a></li>
        </ul>
      </div>
    </nav>
  </header>
  