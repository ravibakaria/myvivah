<br>
<?php foreach($services as $s){?>
<article class="card card-product-list">
	<div class="row no-gutters">
		<aside class="col-md-3"> 
        <?php $images = $this->db->get_where('service_contract_img',['service_contract_id'=>$s->id])->row();
        // echo "<pre>";
        // print_r();die;
        ?>
			<p class="img-wrap">
				<?php if(empty($images)){ ?>
					<img src="<?= base_url().'assets/items/demo.png'?>">
				<?php } else { ?>
					<img src="<?= base_url().'assets/images/thumbnails/'.$images->img_name?>">
				<?php } ?>
				
			</p>
		</aside> <!-- col.// -->
		<div class="col-md-6">
			<div class="info-main">
				<p class="h5 title"> <?php echo $s->name;?></p>
				<p> <?php echo $s->discription ?></p>
				

			</div> <!-- info-main.// --> 
		</div> <!-- col.// -->
		<aside class="col-sm-3">
			<div class="info-aside">
				<div class="price-wrap">
                    <var class="price h4"> &#x20B9;<?php echo $s->pries; ?></var>
                    <del>   <span class="text-muted price1"> &#x20B9;<?php echo $s->showpries; ?></span> 
                    <span class="badge badge-danger"><?php echo $s->showpries - $s->pries;?>-/ OFF</span>
				</div> <!-- price-wrap.// -->
                <small class="text-warning"><?php echo $s->timesservice; ?> Times Service</small>
				
				<p class="text-muted mt-3">Pack Type: <?php echo $s->packtype; ?></p>
				<p class="mt-3">
				
					<a href="<?php echo base_url('Home/book_Services/').$s->id; ?>" class="btn btn-primary" onclick="return confirm('Are You Sure ')">Book Now</a>
				</p>
			</div> <!-- info-aside.// -->
		</aside> <!-- col.// -->
	</div> <!-- row.// -->
</article> <!-- card-product .// -->
<?php } ?>