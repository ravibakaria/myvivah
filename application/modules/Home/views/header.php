<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8"> 
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="manifest" href="<?php echo base_url('manifest.json');?>">
  	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo base_url('assets/images/logo.png');?>">
	<meta name="theme-color" content="#ffffff"> 
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Quests.co.in: Online Shopping for Electronics, Apparel, Computers, Books, DVDs &amp; more">
	<meta name="author" content="Quests">
	<meta property="og:site_name" content="www.Quests.co.in" /> <!-- website name -->
	<meta property="og:site" content="www.Quests.co.in" /> <!-- website link -->
	<meta property="og:title" content="Online Shopping for Electronics, Apparel, Computers, Books, DVDs &amp; more"/> <!-- title shown in the actual shared post -->
	<meta property="og:description" content="Quests.co.in: Online Shopping for Electronics, Apparel, Computers, Books, DVDs &amp; more" /> <!-- description shown in the actual shared post -->
	<meta property="og:image" content="<?php echo base_url('assets/images/logo.png');?>" /> <!-- image link, make sure it's jpg -->
	<meta property="og:url" content="www.Quests.co.in" /> <!-- where do you want your post to link to -->
	<meta property="og:type" content="article" />
  <title>Quests.co.in: Online Shopping for Electronics, Apparel, Computers, Books, DVDs &amp; more</title>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
    crossorigin="anonymous"> 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <link href="<?php echo base_url('assets/css/ui.css');?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/responsive.css');?>" rel="stylesheet">
 <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-169309543-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-169309543-1');
</script>
</head>
<style>
.btn-primary {
  color: #fff !important;
  background-color: #ff6a00 !important;
  border-color: #ff6a00 !important;
}
.error{
	color:red;
}
@media (min-width: 992px){
.container, .container-lg, .container-md, .container-sm {
    max-width: 1270px !important;
}
}
.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  
  top: 0;
  left: 0;
  /* background-color: #111; */
  overflow-x: hidden;
  transition: 0.5s;
  
  z-index: 9;
}

/* .sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
} */

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav .closebtn {
	left: 122px;
    position: relative;
    top: -60px;
  font-size: 36px;
  margin-left: 50px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
.nav-home-aside .menu-category li {

    border-bottom:none !important;
}

.divider.a-divider-break:after {
    content: "";
    width: 100%;
    background-color: transparent;
    display: block;
    height: 1px;
    border-top: 1px solid #e7e7e7;
    position: absolute;
    top: 50%;
    margin-top: -1px;
    z-index: 1;
}
.or-seperator {
        margin-top: 20px;
        text-align: center;
        border-top: 1px solid #ccc;
        }
	.or-seperator i {
		padding: 0 10px;
		background: #f7f7f7;
		position: relative;
		top: -11px;
		z-index: 1;
	}
.autocomplete {
  position: relative;
  display: inline-block;
}
.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}

.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}

/*when hovering an item:*/
.autocomplete-items div:hover {
  background-color: #e9e9e9; 
}

/*when navigating through the items using the arrow keys:*/
.autocomplete-active {
  background-color: DodgerBlue !important; 
  color: #ffffff; 
}
.widget-header .text {
    color:white;
}
.header-main {
   
    background: #3a3a3a;
}
.small, small {
    font-size: 90% ;
    font-weight: 600 ;
}


.widget-header:hover i {
    color: #fff;
}

.heading-line .title-section {
    border-radius: 5px;
}
.carousel-inner{
	border: 1px solid black;
}
.heading-line::before {
    border: 3px solid steelblue !important;  
}
</style>
<body>
<div id="mySidenav" class="sidenav card">
	<div class="card-header">
		<h6>MY MARKETS</h6>
		<button type="button" class="close" onclick="closeNav()">
			<span aria-hidden="true">&times;</span>
		</button>
      </div>
	<div class="card-body">
		<div class="row">
			<aside class="col-lg col-md-3 ">
				
				<nav class="nav-home-aside">
					<ul class="menu-category">
						<?php 
							 $this->db->from('service');
							$this->db->order_by("name", "asc");
							$query = $this->db->get(); 
							$services = $query->result();
						?>
						<?php foreach($services as $s){ ?>
						<li><a href="<?php echo base_url('Home/show_all_produuct_categories/').$s->id;?>"><?php echo $s->name?></a></li>
						<?php } ?>
							<hr>
						<li>
						<a class="nav-link" href="#">Accounts</a>
						</li>
						<li>
							<a class="" href="<?php echo base_url('Referral')?>">Referrals</a>
							</li>
							<li>
							<a class="" href="<?php echo base_url('Business_partner')?>">Sellars</a>
							
						
						</li>
					</ul>
				</nav>
			</aside> 
		</div>
	</div>

 
  <!-- <a href="#">About</a>
  <a href="#">Services</a>
  <a href="#">Clients</a>
  <a href="#">Contact</a> -->
</div>
<header class="section-header">
<section class="header-main border-bottom">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-xl-2 col-lg-3 col-md-12">
				<div class="widget-header mr-3">
					<a href="javascript:void(0)" class="widget-view" onclick="openNav()">
						<div class="icon-area">
						<i class="fas fa-bars" aria-hidden="true"></i>
						</div>
					</a>
				</div>
				<div class="widget-header mr-3">
				<a href="<?php echo base_url();?>" class="brand-wrap">
					<img class="logo" src="<?php echo base_url('assets/images/logo.png');?>">
				</a> <!-- brand-wrap.// -->
				</div>
				
			</div>
			<div class="col-xl-6 col-lg-5 col-md-6">
				<form action="<?php echo base_url('Home/Search_product')?>" class="search-header">
					<div class="input-group w-100">
						<!-- <select class="custom-select border-right"  name="category_name">
								<option value="">All type</option><option value="codex">Special</option>
								<option value="comments">Only best</option>
								<option value="content">Latest</option>
						</select> -->
						
					    	<input id="myInput" type="text" name = "Search" class="form-control" placeholder="Search" autocomplete ="off" value ="<?php if(isset($_GET['Search'])){ echo $_GET['Search'];}?>">
							
					    
							<div class="input-group-append">
							<button class="btn" type="submit">
								<i class="fa fa-search"></i> Search
							</button>
							</div>
							
				    </div>
				</form> <!-- search-wrap .end// -->
			</div> <!-- col.// -->
			<div class="col-xl-4 col-lg-4 col-md-6">
				<div class="widgets-wrap float-md-right">
					<?php if($this->session->userdata('userm') !=''){ ?>
					<div class="widget-header mr-3">
						<a href="#" class="widget-view" data-toggle="dropdown">
							<div class="icon-area">
								<i class="fa fa-user"></i>
								
							</div>
							<small class="text"> My profile </small>
						</a>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="#">Profile</a>
							<a class="dropdown-item" href="#">Change Password</a>
							<a class="dropdown-item" href="<?php echo base_url('Login/logout');?>">Log Out</a>
							
						</div>
					</div>
					<?php } else{?>
						<div class="widget-header mr-3">
						<a href="<?php echo base_url('Login');?>" class="widget-view">
							<div class="icon-area">
								<i class="fa fa-user"></i>
								
							</div>
							<small class="text"> Login </small>
						</a>
					</div>
						<?php 
					} ?>
					<!-- <div class="widget-header mr-3">
						<a href="#" class="widget-view">
							<div class="icon-area">
								<i class="fa fa-comment-dots"></i>
								<span class="notify">1</span>
							</div>
							<small class="text"> Message </small>
						</a>
					</div> -->
					<div class="widget-header mr-3">
						<a href="<?php echo base_url('User/myorder')?>" class="widget-view">
							<div class="icon-area">
							<i class="fas fa-store" aria-hidden="true"></i>
							</div>
							<small class="text"> Orders </small>
						</a>
					</div>
					<div class="widget-header">
						<a href="<?php echo base_url('User/cart')?>" class="widget-view">
							<div class="icon-area">
								<i class="fa fa-shopping-cart"></i>
								<span class="notify"><?= count($this->cart->contents())?></span>
							</div>
							<small class="text"> Cart </small>
						</a>
					</div>
				</div> <!-- widgets-wrap.// -->
			</div> <!-- col.// -->
		</div> <!-- row.// -->
	</div> <!-- container.// -->
</section> <!-- header-main .// -->


<nav class="navbar navbar-main navbar-expand-lg border-bottom" style="
    background-color: cornsilk;
">
  <div class="container">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="fas fa-bars"></span>
    </button>

    <div class="collapse navbar-collapse" id="main_nav">
      <ul class="navbar-nav">
      	<li class="nav-item ">
		<?php if($this->session->userdata('userm') == ''){?>
			<?php if($this->session->userdata('setpin') == ''){?>
			<a class="nav-link" href="#" data-toggle="modal" data-target="#myModal"> <i class="fas fa-map-marker-alt text-muted mr-2"></i> Select Your Address </a>
			<?php } else{?>
				<a class="nav-link" href="#" data-toggle="modal" data-target="#myModal"> <i class="fas fa-map-marker-alt text-muted mr-2"></i> <?php echo 'Delivery Address'.' '.$this->session->userdata('setpin')['pin']?> </a>
			<?php } ?>
		<?php }else{?>
			<?php if($this->session->userdata('setpin') == ''){?>
			<a class="nav-link" href="#" data-toggle="modal" data-target="#myModal"> <i class="fas fa-map-marker-alt text-muted mr-2"></i> <?php echo "Your Address ".$this->session->userdata('userm')->pin?></a>
			<?php } else{?>
				<a class="nav-link" href="#" data-toggle="modal" data-target="#myModal"> <i class="fas fa-map-marker-alt text-muted mr-2"></i> <?php echo 'Delivery Address'.' '.$this->session->userdata('setpin')['pin']?> </a>
			<?php } ?>
		<?php } ?>
          
        </li>
      	<li class="nav-item">
           <a class="nav-link" href="#">Ready to ship</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('Home/show_booking')?>">Booking Services</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('Home/services_contract')?>">Services & Contract</a>
        </li>
        <li class="nav-item">
			
				<a href="<?php echo base_url('Business_partner')?>" class="nav-link typewrite" data-period="2000" data-type='[ "Sell with us !" ]' style = "font-size: 24px;margin-top:-7px">
				<span class="wrap" ></span>
			</a>
			
          <!-- <a class="nav-link" href="#">Sell with us</a> -->
        </li>
        <!-- <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Demo pages</a>
          <div class="dropdown-menu">
			<a class="dropdown-item" href="page-index.html">Main</a>
			<a class="dropdown-item" href="page-category.html">All category</a>
			<a class="dropdown-item" href="page-listing-large.html">Listing list</a>
			<a class="dropdown-item" href="page-listing-grid.html">Listing grid</a>
			<a class="dropdown-item" href="page-shopping-cart.html">Shopping cart</a>
			<a class="dropdown-item" href="page-detail-product.html">Item detail</a>
			<a class="dropdown-item" href="page-content.html">Info content</a>
			<a class="dropdown-item" href="page-user-login.html">Page login</a>
			<a class="dropdown-item" href="page-user-register.html">Page register</a>
			<a class="dropdown-item disabled text-muted" href="#">More components</a>
          </div>
        </li> -->
      </ul>
      <!-- <ul class="navbar-nav ml-md-auto">
      	  <li class="nav-item">
            <a class="nav-link" href="#">Get the app</a>
          </li>
	      <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="http://example.com" data-toggle="dropdown">English</a>
	        <div class="dropdown-menu dropdown-menu-right">
	          <a class="dropdown-item" href="#">Russian</a>
	          <a class="dropdown-item" href="#">French</a>
	          <a class="dropdown-item" href="#">Spanish</a>
	          <a class="dropdown-item" href="#">Chinese</a>
	        </div>
	      </li>
	   </ul> -->
    </div> <!-- collapse .// -->
  </div> <!-- container .// -->
</nav>
</header> <!-- section-header.// -->

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>
<div class="container">
<?php if($this->session->flashdata('message')){?>
  	<div class="alert alert-success">
		<strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
	</div>
	<?php } ?>
<?php if($this->session->flashdata('message_e')){?>
  	<div class="alert alert-danger">
		<strong>Error!</strong> <?php echo $this->session->flashdata('message_e');?>.
	</div>
	<?php } ?>

<div class="modal" id="myModal">
    <div class="modal-dialog">
    	<div class="modal-content">
      
        	<!-- Modal Header -->
			<div class="modal-header">
			<h4 class="modal-title">Choose your location</h4>
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			
			<!-- Modal body -->
			<div class="modal-body">
			<p>Select a delivery location to see product availability and delivery options</p>
			<a href = "<?php echo base_url('Login')?>" class="btn btn-primary btn-block">Sign in to see your addresses</a>
			
			<div class="or-seperator"><i style = " padding: 0 10px;
            background: #f7f7f7;
            position: relative;
            top: -11px;
            z-index: 1;">or enter a pincode</i></div>
			</div>
			
				<div class="form-row justify-content-center">
					<div class="col-6 form-group">
					  	<input type="number" class="form-control" id = "setpin" name = "pin" maxlength="6">
					</div> <!-- form-group end.// -->
					<div class="col-4 form-group">
					  	<button type="submit" class="form-control setpin">Apply</button>
					</div> <!-- form-group end.// -->
				</div> <!-- form-row end.// -->
		</div>
    </div>
</div>

  