
<?php

class Business_partner extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mymodel');
        $this->load->library('form_validation');
    }

    public  function index()
    {
        if($this->session->userdata('user') ==''){
            $this->load->view('login'); 
        }else{
            redirect(base_url('Business_partner/Home'));
        }
    }
    public  function test()
    {
        if($this->session->userdata('user') ==''){
            $this->load->Business_partner('test'); 
        }else{
            redirect(base_url('Business_partner/Home'));
        }
    }

    public  function register()
    {   
        $service = $this->Mymodel->get_all('service');
        $data = array( 
            'services' =>$service,
        );
         $this->load->view('register',$data); 
    }
 
    public function userlogin(){
        if($this->input->post()){
            $email = $this->input->post('email');
            $pass = $this->input->post('password');
            $data = array();
            $var = $this->Mymodel->login('bussines_partner',$email);
        // print_r($var);
            if(!empty($var)){
                if($var->password == md5($pass)){
                    $this->session->set_userdata('user',$var);
                    redirect(base_url('Business_partner/select_bussnise_modules'));
                    
                }else{
                    $this->session->set_flashdata('message_r', 'Password invalid');
                    
                    redirect(base_url('Business_partner'));
                }
            }else{
                $this->session->set_flashdata('message_r', 'invalid Password Or Email');
                redirect(base_url('Business_partner'));
            }
            
        }
    }
    

public function logout(){
    $this->session->unset_userdata('user');
    $this->session->sess_destroy();
    redirect (base_url('Business_partner'));
    
}

public function register_action(){
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('cpassword', 'Password Confirmation', 'required|matches[password]');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[bussines_partner.email]');
    if ($this->form_validation->run() == false) {
        //echo validation_errors();
        $this->session->set_flashdata('message_r', validation_errors());
        redirect(base_url('Business_partner/register'));
    }else{
        $data = array(
            'email'=> $_POST['email'],
            'name'=> $_POST['name'],
            'password' => md5($_POST['password']),
            'pass1' => $_POST['password'],
            'lname' => $_POST['lname'],
            'phone' =>$_POST['phone'],
            'gender' => $_POST['gender'],
            'city' => $_POST['city'],
            'shopname' => $_POST['shopname'],
            'shopno' => $_POST['shopno'],
            'shopadd' => $_POST['shopadd'],
            'bussines_type' => $_POST['bussines'],
            'country' => $_POST['country'],
            'pin' => $_POST['pin'],
            'referral_code' => $_POST['referral_code'],
            'latitude' => $_POST['latitude'],
            'longitude' => $_POST['longitude'],
            'create_at' => date('Y-m-d H:i:s') , 
           
        );
        $this->Mymodel->register('bussines_partner',$data);
        
        redirect(base_url('Business_partner'));
    }  
}

public function imgupload($file,$fname){
    $config['upload_path'] = 'assets/images/users/'; 
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['max_size']             = 4800;
    $this->load->library('upload', $config);
    if($this->upload->do_upload($fname)){
        $fileData = $this->upload->data();
        $name = $fileData['file_name'];
        return $name;
    } 
}
public function profile_action(){
    $this->form_validation->set_rules('name', 'Name', 'required');
    if ($this->form_validation->run() == false) {
        //echo validation_errors();
        $this->session->set_flashdata('message_r', validation_errors());
        redirect(base_url('Business_partner/register'));
    }else{
        //echo "<pre>";
        $data = [];
       $img1 = '';
       $img2 = '';
       $img3 = '';
       $img4 = '';
        if(!empty($_FILES['aadhar']['name'])){  
          $img1 = $this->imgupload($_FILES['aadhar'],'aadhar');
         
          //$data['img1']=$img1;
          $data = array_merge($data, array("img1"=>"$img1"));
        }
        if(!empty($_FILES['voter']['name'])){  
          $img2 = $this->imgupload($_FILES['voter'],'voter');
          //$data['img2']=$img2;  
        
          $data = array_merge($data, array("img2"=>"$img2")); 
        }
        if(!empty($_FILES['dl']['name'])){  
          $img3 = $this->imgupload($_FILES['dl'],'dl');
         // $data['img3']=$img3;
        
          $data = array_merge($data, array("img3"=>"$img3")); 
        }
        if(!empty($_FILES['pan']['name'])){  
          $img4 = $this->imgupload($_FILES['pan'],'pan');
         // $data['img4']=$img4;
        
          $data = array_merge($data, array("img4"=>"$img4")); 
        }
         

		$id = $this->session->userdata('user')->id;
        $data = array_merge($data,array(
            'email'=> $_POST['email'],
            'name'=> $_POST['name'],
            'lname' => $_POST['lname'],
            'phone' =>$_POST['phone'],
            'gender' => $_POST['gender'],
            'city' => $_POST['city'],
            'shopname' => $_POST['shopname'],
            'shopno' => $_POST['shopno'],
            'shopadd' => $_POST['shopadd'],
            'bussines_type' => $_POST['bussines'],
            // 'bussines_type1' => $_POST['bussines1'],
            // 'bussines_type2' => $_POST['bussines2'],
            'country' => $_POST['country'],
            'pin' => $_POST['pin'],
            'longitude' => $_POST['longitude'],
            'latitude' => $_POST['latitude'],
            'aria' => $_POST['delivery'],
            'create_at' => date('Y-m-d H:i:s'),
            'holdername' => $_POST['holdername'],
            'bankname' => $_POST['bankname'],
            'accountno' => $_POST['accountno'],
            'ifsc' => $_POST['ifsc'],
            'ifsc' => $_POST['ifsc'],
            'upi' => $_POST['upi'], 
        ));
        $data1 = [];
        if($_POST['delivery'] == 1){
            $data1 = [
                'uid' => $id,
                'aria' =>$_POST['aria'],
                'state'=>'',
                'city' =>''
            ];
        }
        if($_POST['delivery'] == 2){
            $data1 = [
                'uid' => $id,
                'aria' =>'all',
                'state'=>'',
                'city' =>''
            ];
        }
        
        if($_POST['delivery'] == 3){
            if(isset($_POST['dcity'])){
                foreach($_POST['dcity'] as $c){
                    if($c !='all'){
                        $data1[] = ['state' =>  $_POST['dstate'],'city'=>$c,'uid' => $id,'aria' =>''] ;
                    }

                }
            }
            if(isset($_POST['dcity1'])){
                foreach($_POST['dcity1'] as $c1){
                    if($c1 !='all'){
                        $data1[] = ['state' =>  $_POST['dstate1'],'city'=>$c1,'uid' => $id,'aria' =>''] ;
                    }

                }
            }
            if(isset($_POST['dcity2'])){
                foreach($_POST['dcity2'] as $c2){
                    if($c2 !='all'){
                        $data1[] = ['state' =>  $_POST['dstate2'],'city'=>$c2,'uid' => $id,'aria' =>''] ;
                    }

                }
            }
            
        }
       
        $this->Mymodel->delivery_aria($id,$data1);
    //     echo "<pre>";
    //     print_r($_POST);
    //    print_r($data1);die;
        $this->Mymodel->update('bussines_partner',$id,$data);
		
		redirect(base_url('Business_partner/Home'));
    }  
}


    public function Home(){
        if($this->session->userdata('user') !=''){
			if($this->session->userdata('user')->status == 0){
				redirect(base_url('Business_partner/profileactivate'));
            }
            $id = $this->session->userdata('user')->id;
            $var = $this->Mymodel->get_by_id('bussines_partner', $id);
            $this->session->set_userdata('user',$var);
            // if($this->session->userdata('user')->bussines_module == 4){
            //     redirect(base_url('Business_partner/Products'));
            // }
            // if($this->session->userdata('user')->bussines_module == 11){
            //     redirect(base_url('Business_partner/service_contract'));
            // }
            // if($this->session->userdata('user')->bussines_module == 5){
            //     redirect(base_url('Business_partner/advertise'));
            // }
            // if($this->session->userdata('user')->bussines_module == 7){
            //     redirect(base_url('Business_partner/myportfolio'));
            // }
            // if($this->session->userdata('user')->bussines_module == 6){
            //     redirect(base_url('Business_partner/portfolio'));
            // }
            $this->load->Business_partner('test'); 
        }else{
            redirect(base_url('Business_partner'));
        }
    }
    public function Products(){
        if($this->session->userdata('user') !=''){
            if($this->session->userdata('user')->bussines_module != '4'){
                $this->load->Business_partner('not404');
            }else{
                $id = $this->session->userdata('user')->id;
                $data['products'] = $this->Mymodel->get_all_myproduct('products', $id);
                $this->load->Business_partner('products/productslist.php',$data);
            }
        }else{
            redirect(base_url('Business_partner'));
        }
    }

    public function prodgetLists(){
        $data = $row = array();
       // print_r($_POST);exit();
        $memData = $this->Mymodel->getRows1($_POST);
        
        $i = $_POST['start'];
        foreach($memData as $member){
			$i++;
			$st ='';
			if($member->status == 1){
				$st ='Active';
			}else{
				$st ='InActive';
			}
           
            $status = $st;
		   $img = "<img src='".base_url()."/assets/images/thumbnails/".$member->img_name."' width='100' height='70'>";
		   $action = "<a href='".base_url()."Business_partner/edit_product/".$member->id."' class='btn btn-primary'>Edit</a>
		   <a href='".base_url()."Business_partner/delete_product/".$member->id."' class='btn btn-danger'>Delete</a>";
			
            $data[] = array($i, $member->name, $member->pries,$this->readMoreHelper($member->description),$member->servicename, $img, $member->remark,$status, $action);
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mymodel->countAll1(),
            "recordsFiltered" => $this->Mymodel->countFiltered1($_POST),
            "data" => $data,
        );
        
        // Output to JSON format
        echo json_encode($output);
	}


    public function edit_product($id){
        if($this->session->userdata('user')->bussines_module != '4'){
            $this->load->Business_partner('not404');
        }else{
            $product = $this->Mymodel->get_by_id('products',$id);
            $subservices = $this->Mymodel->sub_get_by_id('sub_service',$product->service);
            $service = $this->Mymodel->get_all('service');
            $gsts = $this->Mymodel->get_all('gst');
                $data = array( 
                    'services' =>$service,
                    'product'  =>$product,
                    'subservices' => $subservices,
                    'gsts' => $gsts,
                );
            if(empty($data['product'])){
                    $this->load->Business_partner('not404');
            }else{
                if($product->status == 1){
                    $this->load->Business_partner('not404');
                }else{
                $this->load->Business_partner('products/product_edit',$data);
                }
            }
        }
	}

    public function add_product(){
        if($this->session->userdata('user')->bussines_module != '4'){
            $this->load->Business_partner('not404');
        }else{
            $sid = $this->session->userdata('user')->bussines_type;
			$service = $this->Mymodel->get_all('service');
			$subservices = $this->Mymodel->sub_get_by_id('sub_service',$sid);
			$gsts = $this->Mymodel->get_all('gst');
			$data = array( 
                'services' =>$service,
                'subservices' =>$subservices,
            );
            $this->load->Business_partner('products/add_product',$data);
        }
		
    }
    public function add_product_action(){
		if($this->session->userdata('user')){
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('pries', 'Pries', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('service', 'Service', 'required');
			
			$count = count($_FILES['fname']['size']);
				for($s=0; $s<=$count-1; $s++) {
					if (empty($_FILES['fname']['name'][$s]))
					{
					    $this->form_validation->set_rules('fname', 'Image', 'required');
					} 
			}
	        if ($this->form_validation->run() == FALSE)
	        {
	            $this->add_product(); 

	        }
	        else
	        {	
                $uid =$this->session->userdata('user')->id;
	        	$userData = array(
	                'name' => $this->input->post('name'),
	                'pries' => $this->input->post('pries'),
	                'service' => $this->input->post('service'),
	                'description' => $this->input->post('description'),
					'quantity' => $this->input->post('quantity'),
					'share' => $this->input->post('share'),
					'showpries' => $this->input->post('showpries'),
					'delivery' => $this->input->post('delivery'),
					'deliveryin' => $this->input->post('deliveryin'),
	                'created_by' => $uid,
	                'color' => $this->input->post('color'),
	                'size' => $this->input->post('size'),
	                'status' => 0,
            		);
            	$insertUserData = $this->Mymodel->insert('products',$userData);
            	$last_id = $this->db->insert_id();
	        	if(!empty($_FILES['fname']['name'])){
	                
					$count = count($_FILES['fname']['size']);
				        foreach($_FILES as $key=>$value){
					        for($s=0; $s<=$count-1; $s++) {
						        $_FILES['fname']['name']     = $value['name'][$s];
						        $_FILES['fname']['type']     = $value['type'][$s];
						        $_FILES['fname']['tmp_name'] = $value['tmp_name'][$s];
						        $_FILES['fname']['error']    = $value['error'][$s];
						        $_FILES['fname']['size']     = $value['size'][$s]; 
								$image_path = realpath(APPPATH . '../assets/
								/');
                                //$config['upload_path'] = $image_path; 
                                $config['upload_path'] = 'assets/images/'; 
						        $config['allowed_types']        = 'gif|jpg|png|jpeg';
				                $config['max_size']             = 4800;
				                // $config['max_width']            = 1024;
				                // $config['max_height']           = 768;
						        $this->load->library('upload', $config);
						        if($this->upload->do_upload('fname')){
				                    // Uploaded file data
				                    $fileData = $this->upload->data();
				                    $name = $fileData['file_name'];
				                  
				                    $upload_data = $this->upload->data();
						            $data['filename'] = $upload_data['file_name'];
						            //print_r($data['filename']);exit();
						            $this->resize_image($data['filename']);
						           
				                    $userData = array(
							                'img_name' => $name,
							                'product_id' =>$last_id
						            		);
				                    $this->Mymodel->insert('images',$userData);
			                	} else{
			                		 $error = array('error' => $this->upload->display_errors());
			                		print_r($error);
			                	}
						       
					        }
					    }
				        //$picture= implode(',', $name_array);
            	}else{
               	 $picture = '';
            	}
            
            	$this->session->set_flashdata('message', 'product added successfully');
            	redirect(base_url('Business_partner/add_product'));
            
        	}
        }	

    }
    
    function resize_image($filename)
    {	
    	$image_path = realpath(APPPATH . '../assets/images/');
        $img_source = $image_path .'/'. $filename;
        $img_target = $image_path .'/thumbnails/'.$filename;
        echo $img_source."<br>";
        echo $img_target."<br>";
        // image lib settings
        $this->load->library('image_lib');
        $config['image_library']    = "gd2";      
	    $config['source_image']     = $img_source;
	    $config['new_image']        = $img_target;
	    //$config['create_thumb']     = TRUE;      
	    $config['maintain_ratio']   = TRUE;      
	    $config['width'] = "128";      
	    $config['height'] = "128";
      
        $this->image_lib->initialize($config);
        if(!$this->image_lib->resize()){
            echo $this->image_lib->display_errors();
        }
        $this->image_lib->clear();
    }

    public function image_delete_ap_service(){
		$id = $this->input->post('id');
        //echo "ravi";
        $data  = ['img_name' =>''];
		$this->Mymodel->update('appointment_product', $id,$data);
    }
    
    public function image_delete(){
		$id = $this->input->post('id');
		$this->Mymodel->delete('service_contract_img', $id);
	}

    public function portfolioimg(){
		$id = $this->input->post('id');
        //echo "ravi";
        $data  = ['img_name' =>''];
		$this->Mymodel->update('portfolio', $id,$data);
	}
    public function myportfolioimg(){
		$id = $this->input->post('id');
        //echo "ravi";
        $data  = ['img_name' =>''];
		$this->Mymodel->update('portfolio', $id,$data);
	}

    public function delete_product($id){
        if($this->session->userdata('user')->bussines_module != '4'){
            $this->load->Business_partner('not404');
        }else{
            if($this->session->userdata('user')){
                $this->db->where('id', $id);
                $this->db->delete('products');
                $this->db->where('product_id',$id);
                $this->db->delete('images');
                redirect(base_url('Business_partner/Products'));
            }
        }
	}

	public function getsubservice(){
		$id = $this->input->post('service');
		$sub_services = $this->Mymodel->sub_get_by_id('sub_service',$id);
		echo json_encode($sub_services);
	}

	public function update_product($id){
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('pries', 'Pries', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('service', 'Service', 'required');
			
			
	        if ($this->form_validation->run() == FALSE){
	            $this->edit_product($id);
				//  $product['product'] = $this->db->get_where('product', array('id' => $id))->row();
				//  $this->load->Business_partner('add_product');
				// $this->load->view('admin/product_edit',$product);
	        }
	        else{
                // $size = implode(',', $_POST['size']); 
                // $color = implode(',', $_POST['color']); 
	        	$userData = array(
                        'name' => $this->input->post('name'),
                        'pries' => $this->input->post('pries'),
                        'service' => $this->input->post('service'),
                        'description' => $this->input->post('description'),
                        'quantity' => $this->input->post('quantity'),
                        'share' => $this->input->post('share'),
                        'showpries' => $this->input->post('showpries'),
                        'delivery' => $this->input->post('delivery'),
                        'deliveryin' => $this->input->post('deliveryin'),
                        'color'=>$this->input->post('color'),
                        'size'=> $this->input->post('size'),
            			);
            			 $insertUserData = $this->Mymodel->update('products',$id,$userData);

	        	if(!empty($_FILES['fname']['name'])){
					$count = count($_FILES['fname']['size']);
				        foreach($_FILES as $key=>$value){
					        for($s=0; $s<=$count-1; $s++) {
						        $_FILES['fname']['name']     = $value['name'][$s];
						        $_FILES['fname']['type']     = $value['type'][$s];
						        $_FILES['fname']['tmp_name'] = $value['tmp_name'][$s];
						        $_FILES['fname']['error']    = $value['error'][$s];
						        $_FILES['fname']['size']     = $value['size'][$s]; 
						        $image_path = realpath(APPPATH . '../assets/images/');
                                //$config['upload_path'] = $image_path; 
                                $config['upload_path'] = 'assets/images/'; 
						        $config['allowed_types']        = 'gif|jpg|png|jpeg';
				                $config['max_size']             = 4800;
				                // $config['max_width']            = 1024;
				                // $config['max_height']           = 768;
						        $this->load->library('upload', $config);
						        if($this->upload->do_upload('fname')){
				                    // Uploaded file data
				                    $fileData = $this->upload->data();
				                    $name = $fileData['file_name'];
				                  
				                    $upload_data = $this->upload->data();
						            $data['filename'] = $upload_data['file_name'];
						            //print_r($data['filename']);exit();
						            $this->resize_image($data['filename']);
						           
				                    $userData = array(
							                'img_name' => $name,
							                'product_id' =>$id
                                            );
                                         //   print_r($userData);
				                    $this->Mymodel->insert('images',$userData);
			                	} else{
			                		 $error = array('error' => $this->upload->display_errors());
			                		print_r($error); //die;
			                	}
						       
					        }
					    }
					    $this->session->set_flashdata('message', 'product Update successfully');
            			redirect(base_url('Business_partner/Products'));
            		
	            }
	             $this->session->set_flashdata('message', 'product Update successfully');
				 redirect(base_url('Business_partner/Products'));
	           
	            
	        }     
	}



	public function profileactivate(){
        if($this->session->userdata('user')->bussines_module != 0 || '' ){
            $id  = $this->session->userdata('user')->id;
            $mys  = $this->session->userdata('user')->bussines_type;
            $profile = $this->Mymodel->myprofile($id);
            $service = $this->Mymodel->get_all('service');
            $myservice = $this->Mymodel->myservice('service',$mys);
            $data = [
                'prifile' =>$profile,
                'services' =>$service,
                'myservice' =>$myservice
            ];
            $this->load->Business_partner('profile',$data); 
        }   else{
            redirect(base_url('Business_partner/select_bussnise_modules'));
        }
	}

	public function status(){
        $gst = $this->db->get('gst')->row(); 
        $data = [
            'amt'=>$gst
        ];
		$this->load->Business_partner('status',$data); 
	}

	private function get_curl_handle($payment_id, $amount)  {
        $url = 'https://api.razorpay.com/v1/payments/'.$payment_id.'/capture';
        $key_id = RAZOR_KEY_ID;
        $key_secret = RAZOR_KEY_SECRET;
        $fields_string = "amount=$amount";
        //cURL Request
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $key_id.':'.$key_secret);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
       // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__).'/ca-bundle.crt');
        return $ch;
    }   

	public function callback() {        
        if (!empty($this->input->post('razorpay_payment_id')) && !empty($this->input->post('merchant_order_id'))) {
            $razorpay_payment_id = $this->input->post('razorpay_payment_id');
            $merchant_order_id = $this->input->post('merchant_order_id');
            $currency_code = 'INR';
            $amount = $this->input->post('merchant_total');
            $success = false;
            $error = '';
            try {                
                $ch = $this->get_curl_handle($razorpay_payment_id, $amount);
                //execute post
                $result = curl_exec($ch);
                $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if ($result === false) {
                    $success = false;
                    $error = 'Curl error: '.curl_error($ch);
                } else {
                    $response_array = json_decode($result, true);
                  // echo "<pre>";print_r($response_array);exit;

                        //Check success response
                        if ($http_status === 200 and isset($response_array['error']) === false) {
                            $success = true;
						   //$this->insert_oder_items($merchant_order_id);
						   $id = $this->session->userdata('user')->id;
						   $userData = array(
							 'status' =>1,
							);
							$this->Mymodel->update('bussines_partner',$id,$userData);
							$profile = $this->Mymodel->myprofile($id);
							$this->session->set_userdata('user',$profile);
							
                           $this->payment_order($response_array,$merchant_order_id);
                        } else {
                            $success = false;
                            if (!empty($response_array['error']['code'])) {
                                $error = $response_array['error']['code'].':'.$response_array['error']['description'];
                            } else {
                                $error = 'RAZORPAY_ERROR:Invalid Response <br/>'.$result;
                            }
                        }
                }
                //close connection
                curl_close($ch);
            } catch (Exception $e) {
                $success = false;
                $error = 'OPENCART_ERROR:Request to Razorpay Failed';
            }
            if($success === true) {
                if(!empty($this->session->userdata('ci_subscription_keys'))) {
                    $this->session->unset_userdata('ci_subscription_keys');
                 }
                if (!$order_info['order_status_id']) {
                    redirect($this->input->post('merchant_surl_id'));
                } else {
                    redirect($this->input->post('merchant_surl_id'));
                }
 
            } else {
                redirect($this->input->post('merchant_furl_id'));
            }
        } else {
            echo 'An error occured. Contact site administrator, please!';
        }
    } 
    public function success() {
		$this->load->Business_partner('paydone'); 

        //$this->load->Template('success.php',$data);
    }  
    public function failed() {
        $data['title'] = 'Razorpay Failed | TechArise';            
        echo "payment failed";
        //$this->load->Template('failed.php',$data);
    }

	public function payment_order($data,$invoice){
        $data1 = array(
            'ORDERID'=>$invoice,
            'TXNID'=>$data['id'],
            'TXNAMOUNT'=>$data['amount']/100,
            'PAYMENTMODE'=>$data['method'],
            'TXNDATE'=>$data['created_at'],
            'STATUS'=>$data['status'],
            
            'email'=>$data['email'],
            'contact'=>$data['contact'],
            'BANKNAME'=>$data['bank'],
           
            'DATA'=> serialize($data)
        );
        $this->db->insert('payments', $data1);
    }


    public function Orders(){
        if($this->session->userdata('user') !=''){
            $id = $this->session->userdata('user')->id;
            $mid = $this->session->userdata('user')->bussines_module;
            
            if($mid == 11){
                $data['order'] = $this->Mymodel->get_all_myborder($id);
                $this->load->Business_partner('products/service_order',$data);
               // echo $this->db->last_query();die;
            }
            if($mid == 7){
               
                $this->load->Business_partner('appointment/myapp');
               // echo $this->db->last_query();die;
            }
            if($mid == 6){
                $data['order'] = $this->Mymodel->get_all_myborder($id);
                $this->load->Business_partner('products/app_order',$data);
               // echo $this->db->last_query();die;
            }if($mid == 4){
                $data['order'] = $this->Mymodel->get_all_myorder($id);
           // echo $this->db->last_query();die;
            $this->load->Business_partner('products/orderlist.php',$data);
            }
        }else{
            redirect(base_url('Business_partner'));
        }
    }
  

    public function getOrders(){
        $data = $row = array();
       // print_r($_POST);exit();
        $memData = $this->Mymodel->getRows($_POST);
       // echo $this->db->last_query();die;
        $i = $_POST['start'];
        foreach($memData as $member){
			$i++;
			$st ='';
			if($member->paystatus == 1){
				$st ='Done';
			}else{
				$st ='pending';
			}
			if($member->delivarystua == 1){
                $stt ='Done';
                $action1 = "done";
                $action = "<a href='#' class='btn btn-primary'>$action1</a>";
			}else{
                $stt ='pending';
                $action1 = "Deliver";
                $action = "<a href='".base_url()."Business_partner/changestatus/".$member->id."' class='btn btn-primary'>$action1</a>";
			}
           
            $status = $st;
            $status1 = $stt;
           $img = "<img src='".base_url()."/assets/images/thumbnails/".$member->img_name."' width='100' height='70'>";
           
		  
			
            $data[] = array($i, $member->product_name, $member->price,$member->qty,$member->total_amt, $img,$status, $status1, $action);
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mymodel->countAll(),
            "recordsFiltered" => $this->Mymodel->countFiltered($_POST),
            "data" => $data,
        );
        
        // Output to JSON format
        echo json_encode($output);
    }
    public function getappOrders(){
        $data = $row = array();
       // print_r($_POST);exit();
        $memData = $this->Mymodel->getRows11($_POST);
       // echo $this->db->last_query();die;
        $i = $_POST['start'];
        foreach($memData as $member){
			$i++;
			
           $img = "<img src='".base_url()."assets/images/".$member->img_name."' width='100' height='70'>";
            $data[] = array($i, $member->cname, $member->pname,$member->phone,$member->price, $img,$member->create_at );
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mymodel->countAll11(),
            "recordsFiltered" => $this->Mymodel->countFiltered11($_POST),
            "data" => $data,
        );
        
        // Output to JSON format
        echo json_encode($output);
    }
    public function changestatus($id){
        $data = array(
            'delivarystua'=> 1
           
        );
        $this->Mymodel->update('orders_items',$id,$data);
        redirect(base_url('Business_partner/Orders'));
    }
    public function select_bussnise_modules(){
        if($this->session->userdata('user')->bussines_module == 0 || '' || $this->session->userdata('user')->email == 'admin@gmail.com' ){
            $bc = $this->session->userdata('user')->bussines_type;
            $module = $this->Mymodel->get_all_business_module('business_module',$bc);
           // echo $this->db->last_query();die;
            $data = [
                'module' => $module
            ];
            $this->load->Business_partner('bussnes_module',$data);
        }else{
            redirect(base_url('Business_partner/Home'));
        }
    }

    public function busniess_update_action(){
       
        $this->form_validation->set_rules('bmodules', 'Busniess Module', 'required');
   
    if ($this->form_validation->run() == false) {
        //echo validation_errors();
        $this->session->set_flashdata('message_r', validation_errors());
        redirect(base_url('Business_partner/select_bussnise_modules'));
    }else{
		$id = $this->session->userdata('user')->id;
        $data = array(
            'bussines_module'=> $_POST['bmodules']
           
        );
        $this->Mymodel->update('bussines_partner',$id,$data);
        $var = $this->Mymodel->get_by_id('bussines_partner',$id);
        $this->session->set_userdata('user',$var);
		redirect(base_url('Business_partner/Home'));
    }  
    }

    public function service_contract(){
        if($this->session->userdata('user')->bussines_module != '11'){
            $this->load->Business_partner('not404');
        }else{
            $id = $this->session->userdata('user')->id;
            $service = $this->Mymodel->get_all_my('service_contract',$id,'created_by');
            $data = array( 
                'services' =>$service,
            );
            $this->load->Business_partner('business_module/service_contract',$data);
        }
    }
    public function add_Services(){
        if($this->session->userdata('user')->bussines_module != '11'){
            $this->load->Business_partner('not404');
        }else{
            $service = $this->Mymodel->get_all('service');
            $data = array( 
                'services' =>$service,
            );
            $this->load->Business_partner('business_module/add_services',$data);
        }
    }
public function add_services_action(){
    if($this->session->userdata('user')){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('pries', 'Pries', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('service', 'Service', 'required');
        
        $count = count($_FILES['fname']['size']);
            for($s=0; $s<=$count-1; $s++) {
                if (empty($_FILES['fname']['name'][$s]))
                {
                    $this->form_validation->set_rules('fname', 'Image', 'required');
                } 
        }
        if ($this->form_validation->run() == FALSE)
        {
            $this->add_Services(); 

        }
        else
        {	
            $uid =$this->session->userdata('user')->id;
            $mid = $this->session->userdata('user')->bussines_module;
            $userData = array(
                'name' => $this->input->post('name'),
                'pries' => $this->input->post('pries'),
                'service' => $this->input->post('service'),
                'discription' => $this->input->post('description'),
                'share' => $this->input->post('share'),
                'showpries' => $this->input->post('showpries'),
                'timesservice' => $this->input->post('timesservice'),
                'packtype' => $this->input->post('packtype'),
                'created_by' => $uid,
                'status' => 0,
                'bussines_module' =>$mid,
                );
            $insertUserData = $this->Mymodel->insert('service_contract',$userData);
            $last_id = $this->db->insert_id();
            if(!empty($_FILES['fname']['name'])){
                
                $count = count($_FILES['fname']['size']);
                    foreach($_FILES as $key=>$value){
                        for($s=0; $s<=$count-1; $s++) {
                            $_FILES['fname']['name']     = $value['name'][$s];
                            $_FILES['fname']['type']     = $value['type'][$s];
                            $_FILES['fname']['tmp_name'] = $value['tmp_name'][$s];
                            $_FILES['fname']['error']    = $value['error'][$s];
                            $_FILES['fname']['size']     = $value['size'][$s]; 
                            $image_path = realpath(APPPATH . '../assets/
                            /');
                            //$config['upload_path'] = $image_path; 
                            $config['upload_path'] = 'assets/images/'; 
                            $config['allowed_types']        = 'gif|jpg|png|jpeg';
                            $config['max_size']             = 4800;
                            // $config['max_width']            = 1024;
                            // $config['max_height']           = 768;
                            $this->load->library('upload', $config);
                            if($this->upload->do_upload('fname')){
                                // Uploaded file data
                                $fileData = $this->upload->data();
                                $name = $fileData['file_name'];
                              
                                $upload_data = $this->upload->data();
                                $data['filename'] = $upload_data['file_name'];
                                //print_r($data['filename']);exit();
                                $this->resize_image($data['filename']);
                               
                                $userData = array(
                                        'img_name' => $name,
                                        'service_contract_id' =>$last_id
                                        );
                                $this->Mymodel->insert('service_contract_img',$userData);
                            } else{
                                 $error = array('error' => $this->upload->display_errors());
                                print_r($error);
                            }
                           
                        }
                    }
                    //$picture= implode(',', $name_array);
            }else{
                $picture = '';
            }
        
            $this->session->set_flashdata('message', 'product added successfully');
            redirect(base_url('Business_partner/add_Services'));
        
        }
    }	
}

public function edit_Services($id){
    if($this->session->userdata('user')->bussines_module != '11'){
        $this->load->Business_partner('not404');
    }else{
        $Service1 = $this->Mymodel->get_by_id('service_contract',$id);
    
        $service = $this->Mymodel->get_all('service');
    
            $data = array( 
                'services' =>$service,
                'Services'  =>$Service1,
            );
        if(empty($data['Services'])){
                $this->load->Business_partner('not404');
        }else{
            if($Service1->status == 1){
                $this->load->Business_partner('not404');
            }else{
                $this->load->Business_partner('business_module/service_edit',$data);
            }
           
        }
    }
}

public function update_Services($id){
    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('pries', 'Pries', 'required');
    $this->form_validation->set_rules('description', 'Description', 'required');
    $this->form_validation->set_rules('service', 'Service', 'required');
    
    
    if ($this->form_validation->run() == FALSE){
        $this->edit_product($id);
        //  $product['product'] = $this->db->get_where('product', array('id' => $id))->row();
        //  $this->load->Business_partner('add_product');
        // $this->load->view('admin/product_edit',$product);
    }
    else{
        $userData = array(
            'name' => $this->input->post('name'),
            'pries' => $this->input->post('pries'),
            'service' => $this->input->post('service'),
            'discription' => $this->input->post('description'),
            'share' => $this->input->post('share'),
            'showpries' => $this->input->post('showpries'),
            'timesservice' => $this->input->post('timesservice'),
            'packtype' => $this->input->post('packtype'),
            
            );
        $insertUserData = $this->Mymodel->update('service_contract',$id,$userData);

        if(!empty($_FILES['fname']['name'])){
            $count = count($_FILES['fname']['size']);
                foreach($_FILES as $key=>$value){
                    for($s=0; $s<=$count-1; $s++) {
                        $_FILES['fname']['name']     = $value['name'][$s];
                        $_FILES['fname']['type']     = $value['type'][$s];
                        $_FILES['fname']['tmp_name'] = $value['tmp_name'][$s];
                        $_FILES['fname']['error']    = $value['error'][$s];
                        $_FILES['fname']['size']     = $value['size'][$s]; 
                        $image_path = realpath(APPPATH . '../assets/images/');
                        //$config['upload_path'] = $image_path; 
                        $config['upload_path'] = 'assets/images/'; 
                        $config['allowed_types']        = 'gif|jpg|png|jpeg';
                        $config['max_size']             = 4800;
                        // $config['max_width']            = 1024;
                        // $config['max_height']           = 768;
                        $this->load->library('upload', $config);
                        if($this->upload->do_upload('fname')){
                            // Uploaded file data
                            $fileData = $this->upload->data();
                            $name = $fileData['file_name'];
                          
                            $upload_data = $this->upload->data();
                            $data['filename'] = $upload_data['file_name'];
                            //print_r($data['filename']);exit();
                            $this->resize_image($data['filename']);
                           
                            $userData = array(
                                'img_name' => $name,
                                'service_contract_id' =>$last_id
                                );
                        $this->Mymodel->insert('service_contract_img',$userData);
                        } else{
                             $error = array('error' => $this->upload->display_errors());
                            print_r($error);
                        }
                       
                    }
                }
                $this->session->set_flashdata('message', 'Record Update successfully');
                redirect(base_url('Business_partner/service_contract'));
            
        }
         $this->session->set_flashdata('message', 'Record Update successfully');
         redirect(base_url('Business_partner/service_contract'));
       
        
    }     
}

public function delete_Services($id){
    if($this->session->userdata('user')->bussines_module != '11'){
        $this->load->Business_partner('not404');
    }else{
        $this->Mymodel->delete('service_contract',$id);
        redirect(base_url('Business_partner/service_contract'));
    }
}

// public function appointment(){
//     $service = $this->Mymodel->get_all('service_contract');
//     $data = array( 
//         'services' =>$service,
//     );
//     $this->load->Business_partner('business_module/appointment',$data);
// }

public function advertise(){
    if($this->session->userdata('user')->bussines_module != '5'){
        $this->load->Business_partner('not404');
    }else{
        $id = $this->session->userdata('user')->id;
        $service = $this->Mymodel->get_all_my('advertise',$id,'u_id');
        $data = array( 
            'services' =>$service,
        );
        $this->load->Business_partner('business_module/advertise',$data);
    }
}

public function add_advertise(){
    if($this->session->userdata('user')->bussines_module != '5'){
        $this->load->Business_partner('not404');
    }else{
        $this->load->Business_partner('business_module/add_advertise');
    }
}

public function add_advertise_action(){
    if($this->session->userdata('user')){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $count = count($_FILES['fname']['size']);
            for($s=0; $s<=$count-1; $s++) {
                if (empty($_FILES['fname']['name'][$s])){
                    $this->form_validation->set_rules('fname', 'Image', 'required');
                } 
            }
        if ($this->form_validation->run() == FALSE) {
            $this->add_advertise(); 
        }else{	
            $uid =$this->session->userdata('user')->id;
            // $insertUserData = $this->Mymodel->insert('service_contract',$userData);
            // $last_id = $this->db->insert_id();
            if(!empty($_FILES['fname']['name'])){
                //$config['upload_path'] = $image_path; 
                $config['upload_path'] = 'assets/images/'; 
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 4800;
                $this->load->library('upload', $config);
                if($this->upload->do_upload('fname')){
                    $fileData = $this->upload->data();
                    $name = $fileData['file_name'];
                    $userData = array(
                        'name' => $this->input->post('name'),
                        // 'service' => $this->input->post('service'),
                        'discription' => $this->input->post('description'),
                        'u_id' => $uid,
                        'status' => 0,
                        'created_at' => date('Y-m-d H:i:s'),
                        'img_name' =>$name
                        );
                    $insertUserData = $this->Mymodel->insert('advertise',$userData);
                }else{
                    $error = array('error' => $this->upload->display_errors());
                    print_r($error);
                }
            }else{
                $picture = '';
            }
        
            $this->session->set_flashdata('message', 'Record added successfully');
            redirect(base_url('Business_partner/add_advertise'));
        
        }
    }	
}

public function edit_advertise($id){
    if($this->session->userdata('user')->bussines_module != '5'){
        $this->load->Business_partner('not404');
    }else{
        $advertise = $this->Mymodel->get_by_id('advertise',$id);
    
    
            $data = array( 
                'advertise' =>$advertise,
            );
        if(empty($data['advertise'])){
                $this->load->Business_partner('not404');
        }else{
            if($advertise->status == 1 ){
                $this->load->Business_partner('not404');
            }else{
                $this->load->Business_partner('business_module/advertise_edit',$data);
            }
            
        }
    }
}

public function update_advertise_action($id){
    if($this->session->userdata('user')){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        if(!empty($_FILES['fname']['name'])){
            $count = count($_FILES['fname']['size']);
            for($s=0; $s<=$count-1; $s++) {
                if (empty($_FILES['fname']['name'][$s])){
                    $this->form_validation->set_rules('fname', 'Image', 'required');
                } 
            }
        }
        if ($this->form_validation->run() == FALSE) {
            $this->add_advertise(); 
        }else{	
            $uid =$this->session->userdata('user')->id;
            // $insertUserData = $this->Mymodel->insert('service_contract',$userData);
            // $last_id = $this->db->insert_id();
            if(!empty($_FILES['fname']['name'])){
                //$config['upload_path'] = $image_path; 
                $config['upload_path'] = 'assets/images/'; 
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 4800;
                $this->load->library('upload', $config);
                if($this->upload->do_upload('fname')){
                    $fileData = $this->upload->data();
                    $name = $fileData['file_name'];
                    $userData = array(
                        'name' => $this->input->post('name'),
                        // 'service' => $this->input->post('service'),
                        'discription' => $this->input->post('description'),
                        'u_id' => $uid,
                        'status' => 0,
                        'created_at' => date('Y-m-d H:i:s'),
                        'img_name' =>$name
                        );
                   // $insertUserData = $this->Mymodel->insert('advertise',$userData);
                    $insertUserData = $this->Mymodel->update('advertise',$id,$userData);
                }else{
                    $error = array('error' => $this->upload->display_errors());
                    print_r($error);
                }
            }else{
                $userData = array(
                    'name' => $this->input->post('name'),
                    // 'service' => $this->input->post('service'),
                    'discription' => $this->input->post('description'),
                    'u_id' => $uid,
                    'status' => 0,
                    );
                $insertUserData = $this->Mymodel->update('advertise',$id,$userData);
            }
        
            $this->session->set_flashdata('message', 'Record added successfully');
            redirect(base_url('Business_partner/advertise'));
        
        }
    }	
}

public function delete_advertise($id){
    if($this->session->userdata('user')->bussines_module != '5'){
        $this->load->Business_partner('not404');
    }else{
        $this->Mymodel->delete('advertise',$id);
        redirect(base_url('Business_partner/advertise'));
    }
}
public function portfolio(){
    if($this->session->userdata('user')->bussines_module != '6'){
        $this->load->Business_partner('not404');
    }else{
        $id= $this->session->userdata('user')->id;
        $advertise = $this->Mymodel->get_by_id_portfolio('portfolio',$id);
        $products = $this->Mymodel->get_by_id_portfolio_service('appointment_product',$id);
        if(empty($advertise)){
            $advertise = [
                'discription' =>'',
                'img_name' =>'',
                'fb' =>'',
                'insta' =>'',
                'youtube' =>'',
                'opendays' =>'',
                'opentime' =>'',
                'morning_time' => '',
                'morning_booking' =>'',
                'after_time' =>'',
                'evning_time' => '',
                'after_booking' => '',
                'evning_booking' =>'',

            ];
            $advertise = (object)$advertise;
        }
        $data = [
            'portpolio'=>$advertise,
            'products'=>$products,
        ];
        $this->load->Business_partner('appointment/portfolio',$data);
    }
}
public function myportfolio(){
    if($this->session->userdata('user')->bussines_module != '7'){
        $this->load->Business_partner('not404');
    }else{
        $id= $this->session->userdata('user')->id;
        $advertise = $this->Mymodel->get_by_id_portfolio('portfolio',$id);
        if(empty($advertise)){
            $advertise = [
                'discription' =>'',
                'img_name' =>'',
                'fb' =>'',
                'insta' =>'',
                'youtube' =>'',
                'opendays' =>'',
                'opentime' =>'',
                'morning_time' => '',
                'morning_booking' =>'',
                'after_time' =>'',
                'evning_time' => '',
                'after_booking' => '',
                'evning_booking' =>'',

            ];
            $advertise = (object)$advertise;
        }
        $data = [
            'portpolio'=>$advertise,
        ];
        $this->load->Business_partner('appointment/myportfolio',$data);
    }
}
public function edit_portfolio(){
    if($this->session->userdata('user')->bussines_module != '6'){
        $this->load->Business_partner('not404');
    }else{
        $id= $this->session->userdata('user')->id;
        $advertise = $this->Mymodel->get_by_id_portfolio('portfolio',$id);
        $data = [];
        if(!empty($advertise)){
            $data = array( 
                'advertise' =>$advertise,
            );
        }else{
            $advertise = [
                'discription' =>'',
                'img_name' =>'',
                'fb' =>'',
                'insta' =>'',
                'youtube' =>'',
                'opendays' =>'',
                'opentime' =>'',
                'morning_time' => '',
                'morning_booking' =>'',
                'after_time' =>'',
                'evning_time' => '',
                'after_booking' => '',
                'evning_booking' =>'',
               

            ];
            $data = array( 
                'advertise' =>(object)$advertise,
            );
        }
        
        $this->load->Business_partner('appointment/edit_portfolio',$data);
    }
}
public function edit_myportfolio(){
    if($this->session->userdata('user')->bussines_module != '7'){
        $this->load->Business_partner('not404');
    }else{
        $id= $this->session->userdata('user')->id;
        $advertise = $this->Mymodel->get_by_id_portfolio('portfolio',$id);
        $data = [];
        if(!empty($advertise)){
            $data = array( 
                'advertise' =>$advertise,
            );
        }else{
            $advertise = [
                'discription' =>'',
                'img_name' =>'',
                'fb' =>'',
                'insta' =>'',
                'youtube' =>'',
                'opendays' =>'',
                'opentime' =>'',
                'morning_time' => '',
                'morning_booking' =>'',
                'after_time' =>'',
                'evning_time' => '',
                'after_booking' => '',
                'evning_booking' =>'',
               

            ];
            $data = array( 
                'advertise' =>(object)$advertise,
            );
        }
        
        $this->load->Business_partner('appointment/edit_myportfolio',$data);
    }
}

public function update_portfolio_action(){
    if($this->session->userdata('user')){
        $uid =$this->session->userdata('user')->id;
        $opendays = '';
        foreach($this->input->post('weekdays') as $c){
            if($c =='All' || $c ==''){     
            }else{
                $opendays .= $c.',';
            }
        }
        $bussines_module =$this->session->userdata('user')->bussines_module;
        if(!empty($_FILES['fname']['name'])){
            $config['upload_path'] = 'assets/images/'; 
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 4800;
            $this->load->library('upload', $config);
            if($this->upload->do_upload('fname')){
                $fileData = $this->upload->data();
                $name = $fileData['file_name'];
                $userData = array(
                    'discription' => $this->input->post('description'),
                    'u_id' => $uid,
                    'fb' => $this->input->post('fb'),
                    'insta' => $this->input->post('insta'),
                    'youtube' => $this->input->post('youtube'),
                    'opendays' => $opendays,
                    'opentime' => $this->input->post('opentime'),
                    'morning_time' => $this->input->post('morning_time'),
                    'morning_booking' => $this->input->post('morning_booking'),
                    'after_time' => $this->input->post('after_time'),
                    'evning_time' => $this->input->post('evning_time'),
                    'after_booking' => $this->input->post('after_booking'),
                    'evning_booking' => $this->input->post('evning_booking'),
                    'bussines_module'=>$bussines_module,
                    //'pertime' => $this->input->post('pertime'),
                    'img_name' =>$name
                    );
                $advertise = $this->Mymodel->get_by_id_portfolio('portfolio',$uid);
                if(empty($advertise)){
                    $insertUserData = $this->Mymodel->insert('portfolio',$userData);
                }else{
                    $insertUserData = $this->Mymodel->update_portfolio('portfolio',$uid,$userData);
                }
            }else{
                $error = array('error' => $this->upload->display_errors());
                print_r($error);
            }
        }else{
            $userData = array(
                'discription' => $this->input->post('description'),
                'u_id' => $uid,
                'fb' => $this->input->post('fb'),
                'insta' => $this->input->post('insta'),
                'youtube' => $this->input->post('youtube'),
                'opendays' => $opendays,
                'opentime' => $this->input->post('opentime'),
                'morning_time' => $this->input->post('morning_time'),
                'morning_booking' => $this->input->post('morning_booking'),
                'after_time' => $this->input->post('after_time'),
                'evning_time' => $this->input->post('evning_time'),
                'after_booking' => $this->input->post('after_booking'),
                'evning_booking' => $this->input->post('evning_booking'),
                'bussines_module'=>$bussines_module,
               // 'pertime' => $this->input->post('pertime'),
            );
            $advertise = $this->Mymodel->get_by_id_portfolio('portfolio',$uid);
            if(empty($advertise)){
                $insertUserData = $this->Mymodel->insert('portfolio',$userData);
            }else{
                $insertUserData = $this->Mymodel->update_portfolio('portfolio',$uid,$userData);
            }
        }
        //echo $this->db->last_query();die;
        $this->session->set_flashdata('message', 'Record added successfully');
        redirect(base_url('Business_partner/portfolio'));
        
    }
}	
public function update_myportfolio_action(){
    //echo "<pre>";
    
    if($this->session->userdata('user')){
        $uid =$this->session->userdata('user')->id;
        $opendays = '';
        foreach($this->input->post('weekdays') as $c){
            if($c =='All' || $c ==''){
                
            }else{
                $opendays .= $c.',';
            }

        }
       // $opendays = implode(",",$openday);
       // print_r($openday);die;
        $bussines_module =$this->session->userdata('user')->bussines_module;
        if(!empty($_FILES['fname']['name'])){
            $config['upload_path'] = 'assets/images/'; 
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 4800;
            $this->load->library('upload', $config);
          
            if($this->upload->do_upload('fname')){
                $fileData = $this->upload->data();
                $name = $fileData['file_name'];
                //$opendays = $this->input->post('opendays');
               
                $userData = array(
                    'discription' => $this->input->post('description'),
                    'u_id' => $uid,
                    'fb' => $this->input->post('fb'),
                    'insta' => $this->input->post('insta'),
                    'youtube' => $this->input->post('youtube'),
                    'opendays' => $opendays,
                    'opentime' => $this->input->post('opentime'),
                    'morning_time' => $this->input->post('morning_time'),
                    'morning_booking' => $this->input->post('morning_booking'),
                    'after_time' => $this->input->post('after_time'),
                    'evning_time' => $this->input->post('evning_time'),
                    'after_booking' => $this->input->post('after_booking'),
                    'evning_booking' => $this->input->post('evning_booking'),
                    'bussines_module'=>$bussines_module,
                    //'pertime' => $this->input->post('pertime'),
                    'img_name' =>$name
                    );
                   
                $advertise = $this->Mymodel->get_by_id_portfolio('portfolio',$uid);
                if(empty($advertise)){
                    $insertUserData = $this->Mymodel->insert('portfolio',$userData);
                }else{
                    $insertUserData = $this->Mymodel->update_portfolio('portfolio',$uid,$userData);
                }
            }else{
                $error = array('error' => $this->upload->display_errors());
                print_r($error);
            }
        }else{
            $userData = array(
                'discription' => $this->input->post('description'),
                'u_id' => $uid,
                'fb' => $this->input->post('fb'),
                'insta' => $this->input->post('insta'),
                'youtube' => $this->input->post('youtube'),
                'opendays' => $opendays,
                'opentime' => $this->input->post('opentime'),
                'morning_time' => $this->input->post('morning_time'),
                'morning_booking' => $this->input->post('morning_booking'),
                'after_time' => $this->input->post('after_time'),
                'evning_time' => $this->input->post('evning_time'),
                'after_booking' => $this->input->post('after_booking'),
                'evning_booking' => $this->input->post('evning_booking'),
                'bussines_module'=>$bussines_module,
               // 'pertime' => $this->input->post('pertime'),
            );
            //print_r($userData);die;
            $advertise = $this->Mymodel->get_by_id_portfolio('portfolio',$uid);
            if(empty($advertise)){
                $insertUserData = $this->Mymodel->insert('portfolio',$userData);
            }else{
                $insertUserData = $this->Mymodel->update_portfolio('portfolio',$uid,$userData);
            }
        }
        
        $this->session->set_flashdata('message', 'Record added successfully');
       redirect(base_url('Business_partner/myportfolio'));
        
    }
}	

public function add_appointment_services(){
    if($this->session->userdata('user')->bussines_module != '6'){
        $this->load->Business_partner('not404');
    }else{
        $service = $this->Mymodel->get_all('service');
        $data = array( 
            'services' =>$service,
        );
        $this->load->Business_partner('appointment/add_appointment_services',$data);
    }
}

public function add_appointment_action(){
    if($this->session->userdata('user')){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('pries', 'Pries', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('service', 'Service', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->add_appointment_services();
        }
        else
        {	
            $uid =$this->session->userdata('user')->id;
            $mid =$this->session->userdata('user')->bussines_module;
            if(!empty($_FILES['fname']['name'])){
                $config['upload_path'] = 'assets/images/'; 
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 4800;
                $this->load->library('upload', $config);
                if($this->upload->do_upload('fname')){
                    $fileData = $this->upload->data();
                    $name = $fileData['file_name'];
                    $userData = array(
                        'name' => $this->input->post('name'),
                        'pries' => $this->input->post('pries'),
                        'service_id' => $this->input->post('service'),
                        'discription' => $this->input->post('description'),
                        'share' => $this->input->post('share'),
                        'showpries' => $this->input->post('showpries'),
                        'img_name' =>$name,
                        'uid' => $uid,
                        'bussines_module' =>$mid,
                        'priceonper' => $this->input->post('priceonper'),
                        'status' => 0,
                        );
                        // echo "<pre>";
                        // print_r($userData);die;
                    $insertUserData = $this->Mymodel->insert('appointment_product',$userData);
                } else{
                        $error = array('error' => $this->upload->display_errors());
                    print_r($error);
                }
            }else{
                $userData = array(
                    'name' => $this->input->post('name'),
                    'pries' => $this->input->post('pries'),
                    'service_id' => $this->input->post('service'),
                    'discription' => $this->input->post('description'),
                    'share' => $this->input->post('share'),
                    'showpries' => $this->input->post('showpries'),
                    'img_name' =>'',
                    'uid' => $uid,
                    'status' => 0,
                    );
                    // echo "<pre>";
                    // print_r($userData);die;
                $insertUserData = $this->Mymodel->insert('appointment_product',$userData);
            }
            
            $this->session->set_flashdata('message', 'product added successfully');
            redirect(base_url('Business_partner/add_appointment_services'));
        
        }
    }
}

public function edit_appointment_services($id){
    if($this->session->userdata('user')->bussines_module != '6'){
        $this->load->Business_partner('not404');
    }else{
        $Service1 = $this->Mymodel->get_by_id('appointment_product',$id);
        $service = $this->Mymodel->get_all('service');
            $data = array( 
                'services' =>$service,
                'Services'  =>$Service1,
            );
        if(empty($data['Services'])){
                $this->load->Business_partner('not404');
        }else{
            if($Service1->status == 1){
                $this->load->Business_partner('not404');
            }else{
                $this->load->Business_partner('appointment/appointment_service_edit',$data);
            }
        }
    }
}

public function update_ap_Services($id){
    if($this->session->userdata('user')){
        $uid =$this->session->userdata('user')->id;
        $mid =$this->session->userdata('user')->bussines_module;
        if(!empty($_FILES['fname']['name'])){
            $config['upload_path'] = 'assets/images/'; 
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 4800;
            $this->load->library('upload', $config);
            if($this->upload->do_upload('fname')){
                $fileData = $this->upload->data();
                $name = $fileData['file_name'];
                $userData = array(
                    'name' => $this->input->post('name'),
                    'pries' => $this->input->post('pries'),
                    'service_id' => $this->input->post('service'),
                    'discription' => $this->input->post('description'),
                    'share' => $this->input->post('share'),
                    'showpries' => $this->input->post('showpries'),
                    'bussines_module' =>$mid,
                    'priceonper' => $this->input->post('priceonper'),
                    
                    'img_name' =>$name,
                    'uid' => $uid,
                    'status' => 0,
                    );
               
                    $insertUserData = $this->Mymodel->update('appointment_product',$id,$userData);
            }else{
                $error = array('error' => $this->upload->display_errors());
                print_r($error);
            }
        }else{
            $userData = array(
                'name' => $this->input->post('name'),
                'pries' => $this->input->post('pries'),
                'service_id' => $this->input->post('service'),
                'discription' => $this->input->post('description'),
                'share' => $this->input->post('share'),
                'showpries' => $this->input->post('showpries'),
                'bussines_module' =>$mid,
                'priceonper' => $this->input->post('priceonper'),
                'uid' => $uid,
                'status' => 0,
                );
            
           
                $insertUserData = $this->Mymodel->update('appointment_product',$id,$userData);
        }
        
        $this->session->set_flashdata('message', 'Record added successfully');
        redirect(base_url('Business_partner/portfolio'));
        
    }
}

public function delete_ap_Services($id){
    if($this->session->userdata('user')->bussines_module != '6'){
        $this->load->Business_partner('not404');
    }else{
        $this->Mymodel->delete('appointment_product',$id);
        //echo $this->db->last_query();die;
        redirect(base_url('Business_partner/portfolio'));
    }
}

function readMoreHelper($story_desc, $chars = 100) {
    $story_desc = substr($story_desc,0,$chars);  
    $story_desc = substr($story_desc,0,strrpos($story_desc,' '));  
    $story_desc = $story_desc."......";  
    return $story_desc;  
}

public function fetch_event(){
    $uid =$this->session->userdata('user')->id;
    $event = $this->Mymodel->get_event('events',$uid);
    echo json_encode($event);
}
public function fetch_event_for_users($id){
    //$uid =$this->session->userdata('user')->id;
    $event = $this->Mymodel->get_event('events',$id);
    echo json_encode($event);
}
public function add_event(){
    $uid =$this->session->userdata('user')->id;
    $data = [
        's_id' =>$uid,
        'title' => $_POST['title'],
        'start' => $_POST['start'],
        'end' => $_POST['start'],
        'color' => 'red',
    ];
     $this->Mymodel->insert('events',$data);
   
}
public function delete_event(){
    $id = $_POST['id'];
    $this->Mymodel->delete('events',$id);
    echo 1;
   
}

public function getssOrders(){
    $data = $row = array();
   // print_r($_POST);exit();
    $memData = $this->Mymodel->getRows22($_POST);
   // echo $this->db->last_query();die;
    $i = $_POST['start'];

    foreach($memData as $member){
        $i++;
        $imge = $this->db->get_where('service_contract_img',['service_contract_id'=>$member->pid])->row();
       $img = "<img src='".base_url()."assets/images/".$imge->img_name."' width='100' height='70'>";
        $data[] = array($i, $member->cname, $member->pname,$member->phone,$member->price, $img,$member->create_at );
    }
    
    $output = array(
        "draw" => $_POST['draw'],
        "recordsTotal" => $this->Mymodel->countAll22(),
        "recordsFiltered" => $this->Mymodel->countFiltered22($_POST),
        "data" => $data,
    );
    
    // Output to JSON format
    echo json_encode($output);
}

// public function exel(){
//     $this->load->Business_partner('excel');
// }

// public function importdata(){ 	
// 	if(isset($_POST["submit"])){
//         $file = $_FILES['file']['tmp_name'];
//         $insertCount = $updateCount = $rowCount = $notAddCount = 0;
//         $this->load->library('CSVReader');
//         $csvData = $this->csvreader->parse_csv($_FILES['file']['tmp_name']);
//         // echo "<pre>";
//         //         print_r($csvData);die;
//         if(!empty($csvData)){
//             foreach($csvData as $row){ $rowCount++; 
//                 $memData = array(
//                     'state' => $row['State'],
//                     'city' => $row['City'],
//                     'pin' => $row['Pincode'],
//                 );
//                 // echo "<pre>";
//                 // print_r($memData);die;
//                 $insert = $this->Mymodel->insert('states',$memData);
                                
//                 if($insert){
//                     $insertCount++;
//                 }
//             }	
//         }
//         echo $insertCount;
// 	}

// }

public function getcity(){
    $state = $this->input->post('state');
    $city = $this->Mymodel->city_get_by_state('states',$state);
    //echo $this->db->last_query();die;
    echo json_encode($city);
}

public function xyz(){
    // $this->load->library('SendEmail');
     $data = array(

         'userName'=> 'Ravi Kumar Bakaria'
  
           );
     $subject='OTP';
     $name = 'ravi';
     $email = 'ravi.bakaria4@gmail.com';
     $email_params = array();
     $email_params['to_name'] = $name;
     $email_params['to_email'] = $email;
     $email_params['from_email'] = "";
     $email_params['subject'] = $subject;
    $emailotpmessage = $this->load->view('Login/anillabs.php',$data,TRUE);
     $email_params['message'] = $emailotpmessage;
     $this->mail($email_params);
 }

 function mail($params = array()) {
      $this->load->library('phpmailer');

     $mailer = $this->phpmailer;

     // $mailer

     $mailer->IsSMTP(); // telling the class to use SMTP
     $mailer->SMTPDebug = 1;                // enables SMTP debug information (for testing)
     // 1 = errors and messages
     // 2 = messages only

     $mailer->SMTPAuth = true;                  // enable SMTP authentication
     $mailer->SMTPSecure = "ssl";                 // sets the prefix to the servier
     $mailer->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
     $mailer->Port = 465;                   // set the SMTP port for the GMAIL server

    //  $mailer->Username = "smtptest@dweb.in";            //edit
    //  $mailer->Password = "Dynamic@123";
     $mailer->Username = "ravibakaria4@gmail.com";            //edit
     $mailer->Password = "9220995987";


     $mailer->AltBody = "To view the message, please use an HTML compatible email viewer!";


     $mailer->FromName = "Noreply";
     $mailer->From = "Noreply@gmail.com";  //edit
     $mailer->isHTML = true;

     if (isset($params['from_name'])) {
         $mailer->FromName = $params['from_name'];
     }

     if (isset($params['from_email'])) {
         $mailer->From = $params['from_email'];
     }


     if (!isset($params['subject'])) {
         throw new Exception("Email: Subject is required", 1);
     }
     if (!isset($params['message'])) {
         throw new Exception("Email: Email body is required", 1);
     }

     if (!isset($params['to_email']) || (isset($params['to']) && count($params['to']) == 0)) {
         throw new Exception("Email: Atleast one recipient is required", 1);
     }

     if (isset($params['to_email'])) {
         $params['to_name'] = (isset($params['to_name'])) ? $params['to_name'] : $params['to_email'];
         $mailer->AddAddress($params['to_email'], $params['to_name']);
     } else {
         throw new Exception("Email: Multiple recipient is not implemented yet", 1);
     }

     if (isset($params['addcc']) && !empty($params['addcc'])) {
         foreach ($params['addcc'] as $email => $name)
             $mailer->AddCC($email, $name);
     }
     if (isset($params['addbcc']) && !empty($params['addbcc'])) {
         foreach ($params['addbcc'] as $email => $name)
             $mailer->AddBCC($email, $name);
     }

     if (isset($params['addattachment']) && !empty($params['addattachment'])) {
         foreach ($params['addattachment'] as $key => $path)
             $mailer->AddAttachment($path, $key);
     }

     $mailer->Subject = $params['subject'];
     $mailer->MsgHTML($params['message']);

     $status = 0;

     ob_start();
     $mail_send_status = $mailer->Send();
     $mail_send_errors = ob_get_contents();
     ob_end_clean();
     echo $mail_send_errors;

     if ($mail_send_status) {
         $status = 1;
     }else{
         $mailer->ErrorInfo;
         exit;
     }

     $mailer->ClearAddresses();
     $mailer->ClearAttachments();

     return $status;
 }

public function abc(){
    $this->load->library("phpmailer_library");
    $mail = $this->phpmailer_library->load();
    $mail->Host = 'tls://smtp.gmail.com:587';
    $mail->SMTPOptions = array(
       'ssl' => array(
         'verify_peer' => false,
         'verify_peer_name' => false,
         'allow_self_signed' => true
        )
    );
    $mail ->isSMTP();
    //$mail->Host = 'smtp.gmail.com';                  // Specify main and backup server
    //$mail->Port = 587;                                    // Set the SMTP port
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'ravibakaria4@gmail.com';                // SMTP username
    $mail->Password = '9220995987';                  // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable encryption, 'ssl' also accepted
    
    $mail->From = 'no-reply@gmail.com';
    $mail->FromName = 'noreply';
    $mail->AddAddress('ravibakaria4@gmail.com');  // Add a recipient
            // Name is optional
    
    $mail->IsHTML(true);                                  // Set email format to HTML
    
    $mail->Subject = 'New User enter email';
    $mail->Body    = 'The user email is :- 9004206420</br> The user password is :- password ravi';
    
    if(!$mail->Send()) {
       echo 'Message could not be sent.';
       echo 'Mailer Error: ' . $mail->ErrorInfo;
       exit;
    }else {
       echo "mail send";
    }
 }

}

?>