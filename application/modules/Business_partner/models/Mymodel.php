<?php

class Mymodel extends CI_Model
{
    public $id = 'id'; 
 
    public $order = 'DESC'; 
    function __construct()
    {
          // parent::__construct();
           $this->column_order = array(null, 'name','invoice_no','total_amt', 'date','status');
           $this->column_order1 = array(null, 'name','pries','description','status');
           $this->column_order11 = array(null, 'cname','pname','phone','price','create_at');
           $this->column_order22 = array(null, 'cname','pname','phone','price','create_at');
           // Set searchable column fields
          $this->column_search = array('name','invoice_no','date','total_amt');
          $this->column_search1 = array('name','pries','description','status');
          $this->column_search11 = array('cname','pname','phone','price','create_at');
          $this->column_search22 = array('cname','pname','phone','price','create_at');
           // Set default order
           $this->order = array('name' => 'asc');
           $this->order1 = array('name' => 'asc');
           $this->order11 = array('cname' => 'asc');
    }



function get_all_active($table) 
    { 
    $this->db->where('status', 0); 
    $this->db->order_by($this->id, 'DESC'); 
    return $this->db->get($table)->result(); 
} 

function get_by_id($table,$id) 
{ 
$this->db->where($this->id, $id); 
return $this->db->get($table)->row(); 
} 

function get_by_service($table,$id) 
{ 
    $this->db->where("service", $id); 
    $this->db->where("status", 1); 
    $this->db->order_by('id', 'DESC'); 
    return $this->db->get($table)->result(); 
} 

function get_all($table) 
    { 
    $this->db->order_by($this->id, 'DESC'); 
    return $this->db->get($table)->result(); 
} 
function get_all_my($table,$id,$filed) 
    { 
    $this->db->order_by($this->id, 'DESC'); 
    $this->db->where($filed,$id);
    return $this->db->get($table)->result(); 
} 

public function login($table,$email){
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where('email',$email);
    $this->db->where('isdelete',0);
    $prevQuery = $this->db->get();
    return $prevQuery->row();
}
public function myprofile($id){
    $this->db->select('*');
    $this->db->from('bussines_partner');
    $this->db->where('id',$id);
    $prevQuery = $this->db->get();
    return $prevQuery->row();
}
public function register($table,$data){
    $this->db->insert($table,$data);
    
    return true;
}
function insert($table,$data) 
{ 
$this->db->insert($table, $data); 
return true;
} 
function delete($table,$id) 
{ 
$this->db->where($this->id, $id); 
$this->db->delete($table); 
}

function update($table,$id, $data) 
{ 
$this->db->where($this->id, $id); 
$this->db->update($table, $data); 
} 
function update_portfolio($table,$id, $data) 
{ 
$this->db->where('u_id', $id); 
$this->db->update($table, $data); 
} 
public function forgetpass($table,$email){
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where('email',$email);
    $prevQuery = $this->db->get();
    return $prevQuery->row();
}
public function get_all_myproduct($table,$id){
    $this->db->where("created_by", $id); 
    $this->db->order_by('id', 'DESC'); 
    return $this->db->get($table)->result(); 
}

public function getRows1($postData){
    $this->_get_datatables_query1($postData);
    if($postData['length'] != -1){
        $this->db->limit($postData['length'], $postData['start']);
    }
    $query = $this->db->get();
   // echo $this->db->last_query();exit();
    return $query->result();
}

private function _get_datatables_query1($postData){
    $sid = $this->session->userdata('user')->bussines_type;
    $id = $this->session->userdata('user')->id;
    $this->db->select('products.id, products.name,products.pries,products.remark, products.description, products.status,images.img_name, service.name as servicename');
    $this->db->from('products');
    $this->db->join('images', 'products.id = images.product_id', 'left'); 
    $this->db->join('service', 'service.id = products.service', 'left'); 
    $this->db->where('products.service',$sid);
    $this->db->where('products.created_by',$id);
    $this->db->group_by('products.id');
     
    $i = 0;
    // loop searchable columns 
    foreach($this->column_search1 as $item){
        // if datatable send POST for search
        if($postData['search']['value']){
            // first loop
            if($i===0){
                // open bracket
                $this->db->group_start();
                $this->db->like($item, $postData['search']['value']);
            }else{
                $this->db->or_like($item, $postData['search']['value']);
            }
            
            // last loop
            if(count($this->column_search1) - 1 == $i){
                // close bracket
                $this->db->group_end();
            }
        }
        $i++;
    }
     
    if(isset($postData['order'])){
        $this->db->order_by($this->column_order1[$postData['order']['0']['column']], $postData['order']['0']['dir']);
    }else if(isset($this->order)){
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
    }
   
}
public function countAll1(){
    $sid = $this->session->userdata('user')->bussines_type;
    $id = $this->session->userdata('user')->id;
    $this->db->select(' products.id, products.name,products.pries, products.description, products.status,images.img_name, service.name as servicename');
    $this->db->from('products');
    $this->db->join('images', 'products.id = images.product_id', 'left'); 
    $this->db->join('service', 'service.id = products.service', 'left'); 
    $this->db->where('products.service',$sid);
    $this->db->where('products.created_by',$id);
    $this->db->group_by('products.id');
     return $this->db->count_all_results();

}

public function countFiltered1($postData){
    $this->_get_datatables_query1($postData);
    $query = $this->db->get();
    return $query->num_rows();
}

function sub_get_by_id($table,$id) { 
    $this->db->where('service_id', $id); 
    return $this->db->get($table)->result(); 
} 
function get_by_id_portfolio($table,$id) { 
    $this->db->where('u_id', $id); 
    return $this->db->get($table)->row(); 
} 
function get_by_id_portfolio_service($table,$id) { 
    $this->db->where('uid', $id); 
    return $this->db->get($table)->result(); 
} 

function myservice($table,$id){
    $this->db->where('id', $id); 
    return $this->db->get($table)->row(); 
}
function get_all_myorder($id){
    $this->db->select('orders_items.id, orders_items.product_name,orders_items.qty,orders_items.price, orders_items.total_amt, orders_items.delivarystua,orders_items.paystatus,orders_items.invoice_no,images.img_name');
    $this->db->from('orders_items');
    $this->db->join('images', 'orders_items.p_id = images.product_id', 'left'); 
    $this->db->where('orders_items.seller_id', $id); 
    $this->db->group_by('orders_items.id');
    //$query = $this->db->get();
    //echo $this->db->last_query();die;
    return $this->db->get()->result(); 
}
function get_all_myborder($id){
    $this->db->select('*');
    $this->db->from('appointment_order');
    $this->db->where('sid', $id); 
    return $this->db->get()->result(); 
}
public function getRows($postData){
    $this->_get_datatables_query($postData);
    if($postData['length'] != -1){
        $this->db->limit($postData['length'], $postData['start']);
    }
    $query = $this->db->get();
   // echo $this->db->last_query();exit();
    return $query->result();
}
private function _get_datatables_query($postData){
   
    $id = $this->session->userdata('user')->id;
    $this->db->select('orders_items.*,images.img_name');
    $this->db->from('orders_items');
    $this->db->join('images', 'orders_items.p_id = images.product_id', 'left'); 
    $this->db->where('orders_items.seller_id',$id);
    $this->db->group_by('orders_items.id');
     
    $i = 0;
    // loop searchable columns 
    foreach($this->column_search as $item){
        // if datatable send POST for search
        if($postData['search']['value']){
            // first loop
            if($i===0){
                // open bracket
                $this->db->group_start();
                $this->db->like($item, $postData['search']['value']);
            }else{
                $this->db->or_like($item, $postData['search']['value']);
            }
            
            // last loop
            if(count($this->column_search) - 1 == $i){
                // close bracket
                $this->db->group_end();
            }
        }
        $i++;
    }
     
    if(isset($postData['order'])){
        $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
    }else if(isset($this->order1)){
        $order = $this->order1;
        $this->db->order_by(key($order), $order[key($order)]);
    }
   
}
public function countAll(){
   
    $id = $this->session->userdata('user')->id;
    $this->db->select('orders_items.*,images.img_name');
    $this->db->from('orders_items');
    $this->db->join('images', 'orders_items.p_id = images.product_id', 'left'); 
    $this->db->where('orders_items.seller_id',$id);
    $this->db->group_by('orders_items.id');
     return $this->db->count_all_results();

}

public function countFiltered($postData){
    $this->_get_datatables_query($postData);
    $query = $this->db->get();
    return $query->num_rows();
}
public function get_all_business_module($tabel,$id){
    $this->db->select('business.*');
    $this->db->from('business');
    $this->db->join('business_module', 'business_module.bussines_id = business.id', 'left'); 
    $this->db->where('business_module.sevice_id',$id);
    return $this->db->get()->result(); 
}
public function get_event($tabel,$id){
    $this->db->where('s_id',$id);
    return $this->db->get($tabel)->result(); 
}


public function getRows11($postData){
    $this->_get_datatables_query11($postData);
    if($postData['length'] != -1){
        $this->db->limit($postData['length'], $postData['start']);
    }
    $query = $this->db->get();
   // echo $this->db->last_query();exit();
    return $query->result();
}
private function _get_datatables_query11($postData){
   
    $id = $this->session->userdata('user')->id;
    $this->db->select('*');
    $this->db->from('appointment_order');
   
    $this->db->where('sid',$id);
    
     
    $i = 0;
    // loop searchable columns 
    foreach($this->column_search11 as $item){
        // if datatable send POST for search
        if($postData['search']['value']){
            // first loop
            if($i===0){
                // open bracket
                $this->db->group_start();
                $this->db->like($item, $postData['search']['value']);
            }else{
                $this->db->or_like($item, $postData['search']['value']);
            }
            
            // last loop
            if(count($this->column_search11) - 1 == $i){
                // close bracket
                $this->db->group_end();
            }
        }
        $i++;
    }
     
    if(isset($postData['order'])){
        $this->db->order_by($this->column_order11[$postData['order']['0']['column']], $postData['order']['0']['dir']);
    }else if(isset($this->order11)){
        $order = $this->order11;
        $this->db->order_by(key($order), $order[key($order)]);
    }
   
}
public function countAll11(){
   
    $id = $this->session->userdata('user')->id;
    $this->db->select('*');
    $this->db->from('appointment_order');
    $this->db->where('sid',$id);
     return $this->db->count_all_results();

}

public function countFiltered11($postData){
    $this->_get_datatables_query11($postData);
    $query = $this->db->get();
    return $query->num_rows();
}

public function getRows22($postData){
    $this->_get_datatables_query22($postData);
    if($postData['length'] != -1){
        $this->db->limit($postData['length'], $postData['start']);
    }
    $query = $this->db->get();
   // echo $this->db->last_query();exit();
    return $query->result();
}
private function _get_datatables_query22($postData){
   
    $id = $this->session->userdata('user')->id;
    $this->db->select('*');
    $this->db->from('service_contract_order');
   
    $this->db->where('sid',$id);
    
     
    $i = 0;
    // loop searchable columns 
    foreach($this->column_search22 as $item){
        // if datatable send POST for search
        if($postData['search']['value']){
            // first loop
            if($i===0){
                // open bracket
                $this->db->group_start();
                $this->db->like($item, $postData['search']['value']);
            }else{
                $this->db->or_like($item, $postData['search']['value']);
            }
            
            // last loop
            if(count($this->column_search22) - 1 == $i){
                // close bracket
                $this->db->group_end();
            }
        }
        $i++;
    }
     
    if(isset($postData['order'])){
        $this->db->order_by($this->column_order22[$postData['order']['0']['column']], $postData['order']['0']['dir']);
    }else if(isset($this->order11)){
        $order = $this->order11;
        $this->db->order_by(key($order), $order[key($order)]);
    }
   
}
public function countAll22(){
   
    $id = $this->session->userdata('user')->id;
    $this->db->select('*');
    $this->db->from('service_contract_order');
    $this->db->where('sid',$id);
     return $this->db->count_all_results();

}

public function countFiltered22($postData){
    $this->_get_datatables_query22($postData);
    $query = $this->db->get();
    return $query->num_rows();
}
function saverecords($state,$city,$pin)
	{
		$query="insert into states values('$state','$city','$pin')";
		$this->db->query($query);
    }
    
function city_get_by_state($table,$id) { 
    $this->db->distinct();
    $this->db->select('city');
    $this->db->where('state', $id); 
    return $this->db->get($table)->result(); 
}  

function delivery_aria($id,$data){
    if(!empty($data)){
        $this->db->select('*');
        $this->db->where('uid', $id); 
        $query = $this->db->get("delivery_aria"); 
        $num = $query->num_rows();
        if($num == 0){
            if(count($data) > 4){
                $this->db->insert_batch('delivery_aria', $data); 
        // $this->db->insert("delivery_aria", $data); 
            return true;
            }else{
                $this->db->insert("delivery_aria", $data);
                return true;
            }
            
        }else{
            $this->db->where('uid', $id); 
            $this->db->delete('delivery_aria');
            if(count($data) > 4){
                $this->db->insert_batch('delivery_aria', $data); 
                return true;
            }else{
                $this->db->insert("delivery_aria", $data);
                return true;
            }
        }
    }
  
}

}