<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<style>
.error{
	color: red;
}
body{
	
    background: navajowhite;

}
</style>
  <div class="jumbotron " style="
    background-color: rosybrown;
">
  			<div class="widget-header mr-3">
				<a href="https://quests.co.in/" class="brand-wrap">
					<img class="logo" src="https://quests.co.in/assets/images/logo.png" height = "47" width = "80"> 
				</a> <!-- brand-wrap.// -->
				</div>
    <h1 class = "text-center" style="
    margin-top: -61px;
">Business Partner</h1>      
  </div>

<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<!-- ============================ COMPONENT REGISTER   ================================= -->
<div class="container">
	<div class="row">
	<div class="col-md-6">
	<div class="card mx-auto">
	<header class=" card-header mb-4 text-center"><h4 class="card-title">Demo video</h4></header>
      <article class="card-body text-center">
	  <iframe width="460" height="315" src="https://www.youtube.com/embed/qGIhl4pwMeM" frameborder="0"  ></iframe>
		
		</article>
    </div> 
	</div>
	<div class="col-md-6">
	<div class="card mx-auto">
        <?php if($this->session->flashdata('message')){?>
        <div class="alert alert-success">
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_r');?>.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('message_r')){?>
        <div class="alert alert-warning">
            <strong>Error!</strong> <?php echo $this->session->flashdata('message_r');?>.
        </div>
        <?php } ?>
      <article class="card-body">
		<header class="mb-4"><h4 class="card-title">Sign up</h4></header>
		<form id = "registerform" method = "post" action = "<?php echo base_url('Business_partner/register_action');?>">
				<div class="form-row">
					<div class="col form-group">
						<label>First name</label>
					  	<input type="text" class="form-control" placeholder="" name = "name">
					</div> <!-- form-group end.// -->
					<div class="col form-group">
						<label>Last name</label>
					  	<input type="text" class="form-control" placeholder="" name ="lname">
					</div> <!-- form-group end.// -->
				</div> <!-- form-row end.// -->
				<div class="form-group">
					<label>Email</label>
					<input type="email" class="form-control" placeholder="" name ="email">
					<small class="form-text text-muted">We'll never share your email with anyone else.</small>
				</div> <!-- form-group end.// -->
                <div class="form-group">
					<label>Mobile No</label>
					<input type="number" class="form-control" placeholder="" name = "phone">
					<small class="form-text text-muted">We'll never share your Mobile No. with anyone else.</small>
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label class="custom-control custom-radio custom-control-inline">
					  <input class="custom-control-input" checked="" type="radio" name="gender" value="Male">
					  <span class="custom-control-label"> Male </span>
					</label>
					<label class="custom-control custom-radio custom-control-inline">
					  <input class="custom-control-input" type="radio" name="gender" value="Female">
					  <span class="custom-control-label"> Female </span>
					</label>
				</div> <!-- form-group end.// -->
				<div class="form-row">
					<div class="form-group col-md-6">
					  <label>City</label>
					  <input type="text" class="form-control" name ="city">
					</div> <!-- form-group end.// -->
					<div class="form-group col-md-6">
					  <label>Country</label>
					  <select id="inputState" class="form-control" name="country">
					    <option> Choose...</option>
					      <option selected="">INDIA</option>
					  </select>
					</div> <!-- form-group end.// -->
				</div> <!-- form-row.// -->
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>Create password</label>
					    <input class="form-control" type="password" name = "password" id = "password">
					</div> <!-- form-group end.// --> 
					<div class="form-group col-md-6">
						<label>Repeat password</label>
					    <input class="form-control" type="password" name = "cpassword">
					</div> <!-- form-group end.// -->  
				</div>
				<div class="form-group">
					<label>Businss Type</label>
					<select name="bussines" class="form-control bussines" style = "height: 34px"> 
                    <option value="">Select</option>
                    <?php
                    foreach($services as $value)
                    {
                        echo "<option value='".$value->id."'>".$value->name."</option>";
                    }
                    ?>
                </select> 
				</div> <!-- form-group end.// -->
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>Shop Name</label>
					    <input class="form-control" type="text" name = "shopname">
					</div> <!-- form-group end.// --> 
					<div class="form-group col-md-6">
						<label>Shop No.</label>
					    <input class="form-control" type="text" name = "shopno">
					</div> <!-- form-group end.// -->  
				</div>
				<div class="form-group">
					<label>Shop Address</label>
					<input type="text" class="form-control" placeholder="" name = "shopadd">
				</div> <!-- form-group end.// -->
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>Pin code</label>
						<input type="text" class="form-control" placeholder="" name = "pin" minlength="6"
					maxlength="6">
					</div> <!-- form-group end.// --> 
					<div class="form-group col-md-6">
						<label>Referral No.</label>
					    <input class="form-control" type="text" name = "referral_code" readonly>
					</div> <!-- form-group end.// -->  
				</div>
				<input type="hidden" id="latitude" name="latitude">
            	<input type="hidden" id="longitude" name="longitude">
			    <div class="form-group">
			        <button type="submit" class="btn btn-primary btn-block"> Register  </button>
			    </div> <!-- form-group// -->      
			    <div class="form-group"> 
		            <label class="custom-control custom-checkbox"> <input type="checkbox" class="custom-control-input" checked="" name  = "term"> <div class="custom-control-label"> I am agree with <a href="#">terms and contitions</a>  </div> </label>
		        </div> <!-- form-group end.// -->           
			</form>
		</article><!-- card-body.// -->
    </div> <!-- card .// -->
    <p class="text-center mt-4">Have an account? <a class = "sheck" href="<?php echo base_url('Business_partner')?>">Log In</a></p>
    <br><br>
	</div>
	</div>
</div>

	
<!-- ============================ COMPONENT REGISTER  END.// ================================= -->


</section>
<!-- ========================= SECTION CONTENT END// ========================= -->
<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function() {
    $j("#registerform").validate({
        rules: {
            name: "required",
            lname: "required",
            phone: "required",
            city: "required",
            country: "required",
            shopname: "required",
           
            shopadd: "required",
            businss: "required",
            term: "required",
			pin: {
				required: true,
				number: true
			},
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            },
            cpassword: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            }
        },
        messages: {
          
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long",
                equalTo: "Please enter the same password as above"
            }
        }
    });
});
</script>
<script>
navigator.geolocation.getCurrentPosition(showPosition);
function showPosition(position){
            let lat = position.coords.latitude;
            let long = position.coords.longitude;
            console.log(lat, long);
            document.getElementById("latitude").value = lat;
            document.getElementById("longitude").value = long;           
}
</script>
<style>
.sheck{
	border: 0;
	font-size: 25px;
	padding: 10px 20px;
	position: relative;
	animation-name: shake;
	animation-duration: 5s;
	animation-iteration-count: infinite;
	animation-timing-function: ease-in;
	cursor: pointer;
}
@keyframes shake {
  0% {left: 0}
  1% {left: -3px}
  2% {left: 5px}
  3% {left: -8px}
  4% {left: 8px}
  5% {left: -5px}
  6% {left: 3px}
  7% {left: 0}
}
</style>