<script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>
<?php if($this->session->flashdata('message')){?>
    <div class="alert alert-success">
        <strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
    </div>
<?php } ?>
<div class="row">
    <div class ="col-md-6 card mx-auto ">

        <div class=" card-body">
            <h2>Add Advertise</h2>
            <div style = "text-align: right";>
                <a href="<?= base_url().'Business_partner/advertise';?>" class="btn btn-primary">List Of Advertise</a>
            </div>
            <form action="<?= base_url('Business_partner/add_advertise_action')?>" method = "POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="email">Advertise Name:</label>
                    <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="<?php echo set_value('name'); ?>">
                    <?php echo form_error('name'); ?>
                </div>
                <div class="form-group">
                    <label for="pwd">Description:</label>
                    <textarea name="description" class="form-control" id="description" cols="30" rows="5"><?php echo set_value('description'); ?></textarea>
                    
                    <?php echo form_error('description'); ?>
                </div>
                <div class="form-group">
					<div class="custom-file">
                        <input name="fname" type="file" class="custom-file-input" aria-describedby="">
                        <label class="custom-file-label" for="">Add Image</label>
                    </div>
				</div>
                <div class="form-group">
			        <button type="submit" class="btn btn-primary btn-block"> Save  </button>
			    </div> <!-- form-group// -->  
            </form>
        </div>
    </div>
</div>
<script>
CKEDITOR.replace('description', {
      fullPage: true,
      extraPlugins: 'docprops',
      // Disable content filtering because if you use full page mode, you probably
      // want to  freely enter any HTML content in source mode without any limitations.
      allowedContent: true,
      height: 320
    });
</script>