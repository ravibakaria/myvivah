<script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>
<style>
#prev{
    /* height:500px ! important;
    width:480px ! important; */
}
</style>
<?php if($this->session->flashdata('message')){?>
    <div class="alert alert-success">
        <strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
    </div>
<?php } ?>
<div class="row">
    <div class ="col-md-6 card mx-auto ">
    <div class=" card-body">
        <h2>Add Services</h2>
        <div style = "text-align: right";>
            <a href="<?= base_url().'Business_partner/service_contract';?>" class="btn btn-primary">List Of Services</a>
        </div>
        <form action="<?= base_url('Business_partner/add_services_action')?>" method = "POST" enctype="multipart/form-data">
            <div class="form-group">
                <label for="email">Services Name:</label>
                <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="<?php echo set_value('name'); ?>">
                <?php echo form_error('name'); ?>
            </div>
            <div class="form-row">
                <div class="col form-group">
                    <label>Actual Price:</label>
                    <input type="text" class="form-control" id="pries" placeholder="Enter Price" name="pries" value="<?php echo set_value('pries'); ?>">
                    <?php echo form_error('pries'); ?>
                </div> 
                <div class="col form-group">
                    <label>Your Share [Price - (4% + GST)]</label>
                    <input type="text" class="form-control" id="share"  name="share" value ="" readonly>
                </div> 
            </div> 
            <div class="form-row">
                <div class="col form-group">
                    <label>Show Price:</label>
                    <input type="text" class="form-control" id="showpries" placeholder="Enter Show Price" name="showpries" value="<?php echo set_value('showpries'); ?>">
                </div> 
                <div class="col form-group">
                    <label>Pack Type:</label>
                    <select name="packtype" class="form-control packtype" style = "height: 34px"> 
                        <option value="">Select</option>
                        <option>1 Month </option>
                        <option>3 Month </option>
                        <option>6 Month </option>
                        <option> Yearly </option>
                    </select> 
                
                </div> 
            </div> 
           
            <div class="form-group">
                <label for="pwd">How Many Times Service:</label>
                <select name="timesservice" class="form-control timesservice" style = "height: 34px"> 
                    <option value="">Select</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                    <option>11</option>
                    <option>12</option>
                    </select> 
                <?php echo form_error('timesservice'); ?>
            </div>

            <div class="form-group">
                <label for="pwd">Description:</label>
                <textarea name="description" class="form-control" id="description" cols="30" rows="5"><?php echo set_value('description'); ?></textarea>
                
                <?php echo form_error('description'); ?>
            </div>
           
            <div class="form-group"> 
                <label for="varchar">Service </label>
                <?php echo form_error('service') ?> 
                <select name="service" class="form-control service" style = "height: 34px"> 
                    <option value="">Select</option>
                    <?php
                        foreach($services as $value){
                            if($value->id == $this->session->userdata('user')->bussines_type) {
                                echo "<option selected='selected' value='".$value->id."'>".$value->name."</option>";
                            }else{
                                if($value->id == $this->session->userdata('user')->bussines_type1){
                                    echo "<option  value='".$value->id."'>".$value->name."</option>";
                                }
                                if($value->id == $this->session->userdata('user')->bussines_type2){
                                    echo "<option  value='".$value->id."'>".$value->name."</option>";
                                }
                            }
                        }
                    ?>
                </select> 
            </div> 
           
            <div class="form-group">
                <label for="">Image:</label> 
                <input type="file" class="form-control" id="fname" name="fname[]">
            </div> 
            <div class="form-group input_fields_wrap">

            </div>
                <button class="add_field_button">Add More</button>
                <?php echo form_error('fname'); ?>
            

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    </div>
    <div class ="col-md-6">
    <div class="row ">
        <aside class="col-md-12">
            <div class="card">
                <article class="gallery-wrap"> 
                    <div class="img-big-wrap">
                        <div class = "showimg"> <img id = "prev" src="<?php echo base_url('assets/items/demo.png')?>" height = "500px" width="480px"></div>
                    </div> <!-- slider-product.// -->
                    
                </article> <!-- gallery-wrap .end// -->
            </div> <!-- card.// -->
        </aside>
        <main class="col-md-12">
            <article class="product-info-aside">
                <h2 class="title mt-3 itemname" ></h2>
                <div class="mb-3"> 
                <var class="price h4"></var>
                <del>   <span class="text-muted price1"></span> 
                    <span class="badge badge-danger"></span>
                </div> <!-- price-detail-wrap .// -->
                <p class ="description"></p>
                <dl class="row">
                    <dt class="col-sm-3">Pack Type</dt>
                    <dd class="col-sm-9 din"></dd>
                    <dt class="col-sm-3 stock"></dt>
                    <dt class="col-sm-9 ">Times Service</dt>
                </dl>
            </article> <!-- product-info-aside .// -->
        </main> <!-- col.// -->
    </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var max_fields      = 5; //maximum input boxes allowed
        var wrapper       = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><input type="file" class="form-control" id="fname'+x+'" name="fname[]"> <a href="#" class="remove_field">   X </a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });



$("#pries").keyup(function(){
    var p = $("#pries").val();
    var m = 4/100;
    var g = 18/100;
    var tatal = p * m;
    var tatal1 = tatal *g;
    var main = tatal1 +tatal;
    var main1 = (p - main).toFixed(2);
  
  $("#share").val(main1);
});

$("#name").blur(function(){
    var name = $("#name").val();
    $(".itemname").html(name);
});
$("#pries").blur(function(){
    var name = $("#pries").val()
    var pries = '&#x20B9;' + name;;
    $(".price").html(pries);
   
});
$("#showpries").blur(function(){
    var name = $("#showpries").val();
    var name1 = $("#pries").val();
    var pries = '&#x20B9;' + name;
    var d = name - name1;
    var dis = "&#x20B9;"+d+" -/OFF";
   
    $(".badge").html(dis);
    $(".price1").html(pries);
});
$('.timesservice').on('change', function() {
    var name =  this.value;
    $(".stock").html(name);
});
$("#description").blur(function(){
    var name = $("#description").val();
    $(".description").html(name);
});
$('.packtype').on('change', function() {
   
    var din = this.value;
    $(".din").html(din);
});
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#prev').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#fname").change(function() {
  readURL(this);
});

CKEDITOR.replace('description', {
      fullPage: true,
      extraPlugins: 'docprops',
      // Disable content filtering because if you use full page mode, you probably
      // want to  freely enter any HTML content in source mode without any limitations.
      allowedContent: true,
      height: 320
    });
</script>