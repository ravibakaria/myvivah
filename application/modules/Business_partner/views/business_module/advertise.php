    <div class="row" style="margin-bottom: 10px"> 
        <div class="col-md-4"> 
            <h2 style="margin-top:0px">Advertise List</h2> 
        </div> 
        <div class="col-md-4 text-center"> 
            <div style="margin-top: 4px" id="message"> 
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?> 
            </div> 
        </div> 
        <div class="col-md-4 text-right"> 
            <?php echo anchor(base_url('Business_partner/add_advertise'), 'Create', 'class="btn btn-primary"'); ?> 
        </div> 
    </div> 
    <table class="table table-bordered table-striped" id="mytable"> 
        <thead> 
            <tr> 
                <th width="80px">No</th> 
                <th>Name</th> 
                <th>Description</th> 
                <th>image</th> 
                <th>Status</th> 
                <th>Action</th> 
            </tr> 
        </thead> 
        <tbody> 
            <?php 
            $start = 0; 
            foreach ($services as $service) 
            { 
                ?> 
                <tr> 
                    <td> 
                        <?php echo ++$start ?> 
                    </td> 
                    <td> 
                        <?php echo $service->name ?>
                    </td>  
                    <td> 
                        <?php echo $service->discription ?> 
                    </td> 
                    <td> 
                        <img src="<?php echo base_url('assets/images/').$service->img_name ?>" alt="" srcset="" class= "img img-thumbnail" width="120px" height = "160px"> 
                    </td> 
                    <td> 
                        <?php if($service->status == 0){
                        echo "Approval Pending";
                       
                        }else{
                            echo "Active";
                        }    
                        ?> 
                    </td>
                    <td style="text-align:center" width="200px"> 
                        <?php  if($service->status == 0){
            echo anchor(base_url('Business_partner/edit_advertise/'.$service->id),'Update');  
            echo ' | ';}  
            echo anchor(base_url('Vendors/Vendors/delete_advertise/'.$service->id),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"');  
            ?> 
                    </td> 
                </tr> 
                <?php 
            } 
            ?> 
        </tbody> 
    </table> 
    <script type="text/javascript"> 
        $(document).ready(function() { 
            $("#mytable").dataTable(); 
        }); 
    </script> 
</body> 
</html>