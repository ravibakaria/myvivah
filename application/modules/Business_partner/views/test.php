
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
    crossorigin="anonymous">
    <header id="main-header" class="py-2 bg-danger text-white">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h1 style="text-transform: capitalize">
          <i class="fas fa-users"></i>  Dashboard</h1>
        </div>
            <div class="col-md-6 mt-2">
            <?php if($this->session->userdata('user')->status == 0){?>
          <a href="<?php echo base_url('Business_partner/profileactivate')?>" class="btn btn-warning">
          <i class="fas fa-toggle-on"></i> Activate Account
          </a>
          <?php } ?>
        </div>
              </div>
    </div>
  </header>

  <!-- ACTIONS -->
  <section id="actions" class="py-4 mb-4">
    <div class="container">
      
    </div>
  </section>

  <!-- POSTS -->
  <section id="posts">
<div class="container">
    <div class="row">

        <div class="col-md-12">
          <div class="card text-center bg-warning text-white mb-3">
            <div class="card-body">
              <h3>Shopname</h3>
              <h4 class="display-4"><i class="fas fa-store"></i>
                <?php echo $this->session->userdata('user')->shopname?>
              </h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>