<section class="section-content padding-y">
    <div class="card mx-auto" style="max-width:720px; margin-top:40px;">
        <article class="card-body">
		    <header class="mb-4"><h4 class="card-title">My Business Modules</h4></header>
            <?php if($this->session->flashdata('message')){?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('message_r');?>.
                </div>
                <?php } ?>
                <?php if($this->session->flashdata('message_r')){?>
                <div class="alert alert-warning">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('message_r');?>.
                </div>
                <?php } ?>
            <form id = "registerform" method = "post" action = "<?php echo base_url('Business_partner/busniess_update_action');?>">
                <p class="font-weight-bold">Business Modules</p>
                    <?php echo form_error('access') ?> </label>
                <ul class="list-group list-group-flush">
                    <?php $i = 1;
                        foreach($module as $b ){ ?>
                            <li class="list-group-item">
                            <div class="custom-control custom-checkbox">
                                <input type="radio" class="custom-control-input" id="check<?php echo $i;?>"  name = 'bmodules' value="<?php echo $b->id?>"  >
                                <label class="custom-control-label" for="check<?php echo $i;?>"><?php echo $b->name?></label>
                                <p><?php echo $b->description?></p>
                            </div>
                            </li>
                        <?php $i++;
                        }
                    ?>
                </ul>
                <div class="form-group">
			        <button type="submit" class="btn btn-primary btn-block"> Update  </button>
			    </div> <!-- form-group// --> 
            </form>
        </article>
    </div>
</section>