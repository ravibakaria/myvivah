
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>

<h2>List Of Orders</h2>
<div class="table-responsive-sm">
  <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
              <th>Sr. No</th>
              <th>Customer Name</th>
              <th>Product Name</th>
             
              <th>Phome</th>
              <th>Total Amount</th>
              <th>Image</th>
              <th>Date</th>
              
            </tr>
        </thead>
        <tfoot>
            <tr>
            <th>Sr. No</th>
              <th>Customer Name</th>
              <th>Product Name</th>
             
              <th>Phome</th>
              <th>Total Amount</th>
              <th>Image</th>
              <th>Date</th>
              
            </tr>
        </tfoot>
    </table>
  </div>
<script>
var $j = jQuery.noConflict();
$j(document).ready(function(){
    $j('#example').DataTable({
        
        "processing": true,
        
        "serverSide": true,
        "order": [[ 2, "desc" ]],
      
        // "order": [],
      
        "ajax": {
            "url": "<?php echo base_url('Business_partner/getssOrders/'); ?>",
            "type": "POST"
        },
        "aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [0] }
       ],
        "columnDefs": [{ 
            "targets": [0],
            "orderable": false  
        }],
        serverSide: true,
        dom: 'Bfrtip',   
        buttons: [
            'pageLength',
            {extend: 'excelHtml5', title: 'Data export' },
            {extend: 'csvHtml5', title: 'Data export' },
            {extend: 'pdfHtml5', title: 'Data export' }
        ]
    });
    table.buttons().container()
        .appendTo( '#example .col-sm-6:eq(0)' );

});

// $(document).ready(function() {
//     $('#example').DataTable( {
//         dom: 'Bfrtip',
//         buttons: [
//             { extend: 'copyHtml5', footer: true },
//             { extend: 'excelHtml5', footer: true },
//             { extend: 'csvHtml5', footer: true },
//             { extend: 'pdfHtml5', footer: true }
//         ]
//     } );
// } );
</script>