
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">


<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap.min.css">

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>


<h2>List Of Products</h2>

<div style = "text-align: right";>
	<a href="<?= base_url().'Business_partner/add_product';?>" class="btn btn-primary">Add Product</a>
</div>
<?php if($this->session->flashdata('message')){?>
  	<div class="alert alert-success">
		<strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
	</div>
	<?php } ?>
<?php 
//print_r($products);?>
<div class="table-responsive-sm">
  <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
              <th>Sr. No</th>
              <th>Name</th>
              <th>Price</th>
             
              <th>Description</th>
              <th>Catgory</th>
              <th>Image</th>
              <th>Remark</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
              <th>Sr. No</th>
              <th>Name</th>
              <th>Pries</th>
             
              <th>Description</th>
              <th>Catgory</th>
              <th>Image</th>
              <th>Remark</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
        </tfoot>
    </table>
  </div>
<script>
var $j = jQuery.noConflict();
// $j(document).ready(function(){
//     $j('#example').DataTable({
        
//         "processing": true,
        
//         "serverSide": true,
//         "order": [[ 2, "desc" ]],
      
//         // "order": [],
      
//         "ajax": {
//             "url": "<?php echo base_url('Business_partner/prodgetLists/'); ?>",
//             "type": "POST"
//         },
//         "aoColumnDefs": [
//           { 'bSortable': false, 'aTargets': [0, 5 ] }
//        ],
//         // "columnDefs": [{ 
//         //     "targets": [0],
//         //     "orderable": false  
//         // }],
//         serverSide: true,
//         // dom: 'Lfrtip',   
//         // buttons: [
//         //     'pageLength',
//         //     {extend: 'excelHtml5', title: 'Data export' },
//         //     {extend: 'csvHtml5', title: 'Data export' },
//         //     {extend: 'pdfHtml5', title: 'Data export' }
//         // ]
//     });
//     table.buttons().container()
//         .appendTo( '#example .col-sm-6:eq(0)' );

// });

$j(document).ready(function(){
    $j('#example').DataTable({
        // Processing indicator
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
            "url": "<?php echo base_url('Business_partner/prodgetLists/'); ?>",
            "type": "POST"
        },
        "aoColumnDefs": [
          { 'bSortable': false, 'aTargets': [0, 5 ,6,7] }
       ],
        //Set column definition initialisation properties
        "columnDefs": [{ 
            "targets": [0],
            "orderable": false
        }]
    });
});
</script>