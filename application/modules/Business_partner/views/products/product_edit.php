<script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>
<div class="row">
	<div class ="col-md-8 card mx-auto">
		<div class=" card-body">
			<form method="post" action="<?php echo base_url('Business_partner/update_product/'.$product->id);?>" enctype="multipart/form-data">
				<h3>Update Product </h3>
					<div class="form-group">
						<label for="email">Name:</label>
						<input type="text" class="form-control" id="name" value="<?= $product->name;?>" name="name">
						<?php echo form_error('name'); ?>
					</div>
					<div class="form-row">
						<div class="col form-group">
							<label>Your Selling Price:</label>
							<input type="text" class="form-control" id="pries" value="<?= $product->pries;?>" name="pries">
					<?php echo form_error('pries'); ?>
						</div> 
						<div class="col form-group">
							<label>Your Share [Price - (4% + GST)]</label>
							<input type="text" class="form-control" id="share"  name="share"  value="<?= $product->share;?>" readonly>
						</div> 
            		</div> 
					
					<div class="form-row">
						<div class="col form-group">
							<label>Actual Price/MRP:</label>
							<input type="text" class="form-control" id="showpries" placeholder="Enter Show Price" name="showpries" value="<?php echo $product->showpries ?>">
						</div> 
						<div class="col form-group">
							<label>Quantity:</label>
							<input type="text" class="form-control" id="pries" placeholder="Enter pries" name="quantity" value="<?= $product->quantity;?>">
								<?php echo form_error('quantity'); ?>
						</div> 
					</div> 
					<?php 
					
						// if($product->size ==''){
						// 	$size = [];
						// }else{
						// 	$size = explode(",",$product->size,true);
						// 	$size = explode((','),$size[0]);
							
						// }
						
					?>
					<!-- <div class="form-row">
						<label>Available In Size:</label>
					<div class="inner">
						<label class="checkbox-btn">
							<input type="checkbox" name = "size[]" value = "XS" <?php if(in_array("XS", $size)){ echo "checked";}?>>
							<span class="btn btn-light"> XS </span>
						</label>

						<label class="checkbox-btn">
							<input type="checkbox" name = "size[]" value = "SM" <?php if(in_array("SM", $size)){ echo "checked";}?>>
							<span class="btn btn-light"> SM </span>
						</label>

						<label class="checkbox-btn">
							<input type="checkbox" name = "size[]" value = "LG" <?php if(in_array("LG", $size)){ echo "checked";}?>>
							<span class="btn btn-light"> LG </span>
						</label>

						<label class="checkbox-btn">
							<input type="checkbox" name = "size[]" value = "XXL" <?php if(in_array("XXL", $size)){ echo "checked";}?>>
							<span class="btn btn-light"> XXL </span>
						</label>
						<label class="checkbox-btn">
							<input type="checkbox" name = "size[]" value = "4" <?php if(in_array("4", $size)){ echo "checked";}?>>
							<span class="btn btn-light"> 4 </span>
						</label>
						<label class="checkbox-btn">
							<input type="checkbox" name = "size[]" value = "5" <?php if(in_array("4", $size)){ echo "checked";}?>>
							<span class="btn btn-light"> 5 </span>
						</label>
						<label class="checkbox-btn">
							<input type="checkbox" name = "size[]" value = "6" <?php if(in_array("6", $size)){ echo "checked";}?>>
							<span class="btn btn-light"> 6 </span>
						</label>
						<label class="checkbox-btn">
							<input type="checkbox" name = "size[]" value = "7" <?php if(in_array("7", $size)){ echo "checked";}?>>
							<span class="btn btn-light"> 7 </span>
						</label>
						<label class="checkbox-btn">
							<input type="checkbox" name = "size[]" value = "8" <?php if(in_array("8", $size)){ echo "checked";}?>>
							<span class="btn btn-light"> 8 </span>
						</label>
						<label class="checkbox-btn">
							<input type="checkbox" name = "size[]" value = "9" <?php if(in_array("9", $size)){ echo "checked";}?>>
							<span class="btn btn-light"> 9 </span>
						</label>
					</div>
					</div> 
					<?php  if($product->color ==''){
								$color = [];
							}else{
								$color = explode(" ",$product->color);
								$color = explode((','),$color[0]);
							}
					?>		
					<div class="form-row">
						<label>Available In Colours:</label>
					<div class="inner">
						<label class="checkbox-btn">
							<input type="checkbox" name = "color[]" value = "red" <?php if(in_array("red", $color)){ echo "checked";}?>>
							<span class="btn btn-light"> Red </span>
						</label>

						<label class="checkbox-btn">
							<input type="checkbox" name = "color[]" value = "black" <?php if(in_array("black", $color)){ echo "checked";}?>>
							<span class="btn btn-light"> Black </span>
						</label>

						<label class="checkbox-btn">
							<input type="checkbox" name = "color[]" value = "blue" <?php if(in_array("blue", $color)){ echo "checked";}?>>
							<span class="btn btn-light"> Blue </span>
						</label>

						<label class="checkbox-btn">
							<input type="checkbox" name = "color[]" value = "white" <?php if(in_array("white", $color)){ echo "checked";}?>>
							<span class="btn btn-light"> White </span>
						</label>
						<label class="checkbox-btn">
							<input type="checkbox" name = "color[]" value = "orange" <?php if(in_array("orange", $color)){ echo "checked";}?>>
							<span class="btn btn-light"> Orange </span>
						</label>
						<label class="checkbox-btn">
							<input type="checkbox" name = "color[]" value = "SlateBlue" <?php if(in_array("SlateBlue", $color)){ echo "checked";}?>>
							<span class="btn btn-light"> SlateBlue </span>
						</label>
						<label class="checkbox-btn">
							<input type="checkbox" name = "color[]" value = "Violet" <?php if(in_array("Violet", $color)){ echo "checked";}?>>
							<span class="btn btn-light"> Violet </span>
						</label>
						<label class="checkbox-btn">
							<input type="checkbox" name = "color[]" value = "LightGray" <?php if(in_array("LightGray", $color)){ echo "checked";}?>>
							<span class="btn btn-light"> LightGray </span>
						</label>
						<label class="checkbox-btn">
							<input type="checkbox" name = "color[]" value = "Gray" <?php if(in_array("Gray", $color)){ echo "checked";}?>>
							<span class="btn btn-light"> Gray </span>
						</label>
						<label class="checkbox-btn">
							<input type="checkbox" name = "color[]" value = "DodgerBlue" <?php if(in_array("DodgerBlue", $color)){ echo "checked";}?>>
							<span class="btn btn-light"> DodgerBlue </span>
						</label>
						<label class="checkbox-btn">
							<input type="checkbox" name = "color[]" value = "Yellow" <?php if(in_array("Yellow", $color)){ echo "checked";}?>>
							<span class="btn btn-light"> Yellow </span>
						</label>
					</div>
					</div>  -->
					<div class="form-row">
					<div class="col form-group">
						<label>Available In Size:</label>
						<input type="text" class="form-control" id="size" placeholder="XXL Or 9-10 Years" name="size" value="<?php echo $product->size; ?>">
					</div> 
					<div class="col form-group">
						<label>Available In Colour:</label>
						<input type="text" class="form-control" id="color" placeholder="Enter Colour" name="color" value="<?php echo $product->color; ?>">
					</div> 
				</div> 
					<div class="form-group">
						<label for="pwd">Description:</label>
						<textarea class="form-control" id="description"  name="description" cols="30" rows="5"><?= $product->description;?></textarea>
						<?php echo form_error('description'); ?>
					</div>
					<div class="form-row">
						<div class="col form-group">
							<label>Delivery :</label>
							<input type="text" class="form-control" id="delivery" placeholder="Enter Show Delivery" name="delivery" value="<?php echo $product->delivery; ?>">
						</div> 
						<div class="col form-group">
							<label>Delivery In:</label>
							<select name="deliveryin" class="form-control deliveryin" style = "height: 34px"> 
								<option value="">Select</option>
								<option <?php if($product->deliveryin =='Minutes'){echo "selected='selected'"; }?>>Minutes</option>
								<option <?php if($product->deliveryin =='Hours'){echo "selected='selected'"; }?>>Hours</option>
								<option <?php if($product->deliveryin =='Days'){echo "selected='selected'"; }?>>Days</option>
								<option <?php if($product->deliveryin =='Working Days'){echo "selected='selected'"; }?>>Working Days</option>
							</select> 
						</div> 
					</div> 
					<div class="form-group"> 
						<label for="varchar">Service</label>  
						<?php echo form_error('service') ?> 
						<select name="service" class="form-control service" style = "height: 34px"> 
							<option value="">Select</option>
							<?php
								foreach($services as $value){
									if($value->id == $product->service){
										echo "<option selected='selected' value='".$value->id."'>".$value->name."</option>";
									}
								}
							?>
						</select> 
					</div> 

					<?php $images = $this->db->get_where('images',['product_id'=>$product->id])->result();
						if(count($images)>=5){

						}else{?>
						<div class="form-group">
							<label for="pwd">Image:</label> 
								<input type="file" class="form-control" id="fname" name="fname[]"> 
							<div class="input_fields_wrap">

							</div>
							<button class="add_field_button">Add More</button>

						</div>
					<?php } ?>
				<div class="row">

					<?php foreach ($images as $key => $image) {?>
						<div class="col-md-3">
							<button type="button" class="close" value="<?= $image->id?>">&times;</button>
							<img src="<?= base_url().'assets/images/thumbnails/'.$image->img_name?>">
						</div>
					<?php } ?>
				</div>



				<div class="form-group">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
$(".close").click(function(){
if(confirm("Are you sure you want to delete this?")){

}
else{
return false;
}
var id = $(this).val();
//alert(id);
$.ajax({ 
url: "<?= base_url('Business_partner/image_delete')?>",
data: {'id':id},
type: 'post',
success: function(data){
//alert(data);
location.reload();
}
});
});
});

$(document).ready(function() {
var max_fields      = <?php echo 5 - count($images);?>; //maximum input boxes allowed
var wrapper       = $(".input_fields_wrap"); //Fields wrapper
var add_button      = $(".add_field_button"); //Add button ID

var x = 1; //initlal text box count
$(add_button).click(function(e){ //on add input button click
e.preventDefault();
if(x < max_fields){ //max input box allowed
x++; //text box increment
$(wrapper).append('<div><input type="file" class="form-control" id="fname" name="fname[]"> <a href="#" class="remove_field">   X </a></div>'); //add input box
}
});

$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
e.preventDefault(); $(this).parent('div').remove(); x--;
})
});

// $(document).ready(function(){
//   $(".service").change(function(){
//     var service = $(".service").val();

//     $.ajax({
//             url: "<?php echo base_url('Product/getsubservice');?>",
//             method: "POST",
//             data: {service: service},
//             cache: true
//         }).success(function(data){

//             data = $.parseJSON(data);
//             var i;
//             var html='';
//             for (i = 0; i < data.length; i++) {
//             html += "<option value = '"+data[i].id+"'>"+data[i].name+"</option>"
//            }

//            $(".service1").html(html);

//         }).fail(function(data){
//             console.log(data);
//         });
//   });
// });
$("#pries").keyup(function(){
var p = $("#pries").val();
var m = 4/100;
var g = 18/100;
var tatal = p * m;
var tatal1 = tatal *g;
var main = tatal1 +tatal;
var main1 = (p - main).toFixed(2);

$("#share").val(main1);
});

CKEDITOR.replace('description', {
      fullPage: true,
      extraPlugins: 'docprops',
      // Disable content filtering because if you use full page mode, you probably
      // want to  freely enter any HTML content in source mode without any limitations.
      allowedContent: true,
      height: 320
    });
</script>