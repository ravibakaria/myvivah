<style>
.red-star {
    color: red;
}
.error{
    color: red;
}
</style>
<script src="https://cdn.ckeditor.com/4.14.0/standard-all/ckeditor.js"></script>
<?php if($this->session->flashdata('message')){?>
    <div class="alert alert-success">
        <strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
    </div>
<?php } ?> 
<div class="row">
    <div class ="col-md-6 card mx-auto ">
    <div class=" card-body">
        <h2>Add Product</h2>
        <div style = "text-align: right";>
            <a href="<?= base_url().'Business_partner/Products';?>" class="btn btn-primary">List Of Product</a>
        </div>
        <form action="<?= base_url('Business_partner/add_product_action')?>" method = "POST" enctype="multipart/form-data">
            <div class="form-group">
                <label for="email">Product Name:<span class="red-star">*</span></label> 
                <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="<?php echo set_value('name'); ?>" required >
                <?php echo form_error('name'); ?>
            </div>
            <div class="form-row">
                <div class="col form-group">
                    <label>Your Selling Price:<span class="red-star">*</span></label>
                    <input type="text" class="form-control" id="pries" placeholder="Enter Price" name="pries" value="<?php echo set_value('pries'); ?>"  required >
                    <?php echo form_error('pries'); ?>
                </div> 
                <div class="col form-group">
                    <label>Your Share [Price - (4.72%)]</label>
                    <input type="text" class="form-control" id="share"  name="share" value ="" readonly>
                </div> 
            </div> 
            <div class="form-row">
                <div class="col form-group">
                    <label>Actual Price/MRP:</label>
                    <input type="text" class="form-control" id="showpries" placeholder="Enter Show Price" name="showpries" value="<?php echo set_value('showpries'); ?>">
                </div> 
                <div class="col form-group">
                    <label>Quantity:<span class="red-star">*</span></label>
                    <input type="text" class="form-control" id="quantity" placeholder="Enter quantity" name="quantity" value="<?php echo set_value('quantity'); ?>" required >
                <?php echo form_error('quantity'); ?>
                </div> 
            </div> 
            <!-- <div class="form-row">
                <label>Available In Size:</label>
			  <div class="inner">
			  	<label class="checkbox-btn">
				    <input type="checkbox" name = "size[]" value = "XS">
				    <span class="btn btn-light"> XS </span>
				  </label>

				  <label class="checkbox-btn">
				    <input type="checkbox" name = "size[]" value = "SM">
				    <span class="btn btn-light"> SM </span>
				  </label>

				  <label class="checkbox-btn">
				    <input type="checkbox" name = "size[]" value = "LG">
				    <span class="btn btn-light"> LG </span>
				  </label>

				  <label class="checkbox-btn">
				    <input type="checkbox" name = "size[]" value = "XXL">
				    <span class="btn btn-light"> XXL </span>
				  </label>
				  <label class="checkbox-btn">
				    <input type="checkbox" name = "size[]" value = "4">
				    <span class="btn btn-light"> 4 </span>
				  </label>
				  <label class="checkbox-btn">
				    <input type="checkbox" name = "size[]" value = "5">
				    <span class="btn btn-light"> 5 </span>
				  </label>
				  <label class="checkbox-btn">
				    <input type="checkbox" name = "size[]" value = "6">
				    <span class="btn btn-light"> 6 </span>
				  </label>
				  <label class="checkbox-btn">
				    <input type="checkbox" name = "size[]" value = "7">
				    <span class="btn btn-light"> 7 </span>
				  </label>
				  <label class="checkbox-btn">
				    <input type="checkbox" name = "size[]" value = "8">
				    <span class="btn btn-light"> 8 </span>
				  </label>
				  <label class="checkbox-btn">
				    <input type="checkbox" name = "size[]" value = "9">
				    <span class="btn btn-light"> 9 </span>
				  </label>
			 </div>
            </div> 
            <div class="form-row">
                <label>Available In Colours:</label>
			  <div class="inner">
			  	<label class="checkbox-btn">
				    <input type="checkbox" name = "color[]" value = "red">
				    <span class="btn btn-light"> Red </span>
				  </label>

				  <label class="checkbox-btn">
				    <input type="checkbox" name = "color[]" value = "black">
				    <span class="btn btn-light"> Black </span>
				  </label>

				  <label class="checkbox-btn">
				    <input type="checkbox" name = "color[]" value = "blue">
				    <span class="btn btn-light"> Blue </span>
				  </label>

				  <label class="checkbox-btn">
				    <input type="checkbox" name = "color[]" value = "white">
				    <span class="btn btn-light"> White </span>
				  </label>
				  <label class="checkbox-btn">
				    <input type="checkbox" name = "color[]" value = "orange">
				    <span class="btn btn-light"> Orange </span>
				  </label>
				  <label class="checkbox-btn">
				    <input type="checkbox" name = "color[]" value = "SlateBlue">
				    <span class="btn btn-light"> SlateBlue </span>
				  </label>
				  <label class="checkbox-btn">
				    <input type="checkbox" name = "color[]" value = "Violet">
				    <span class="btn btn-light"> Violet </span>
				  </label>
				  <label class="checkbox-btn">
				    <input type="checkbox" name = "color[]" value = "LightGray">
				    <span class="btn btn-light"> LightGray </span>
				  </label>
				  <label class="checkbox-btn">
				    <input type="checkbox" name = "color[]" value = "Gray">
				    <span class="btn btn-light"> Gray </span>
				  </label>
				  <label class="checkbox-btn">
				    <input type="checkbox" name = "color[]" value = "DodgerBlue">
				    <span class="btn btn-light"> DodgerBlue </span>
				  </label>
				  <label class="checkbox-btn">
				    <input type="checkbox" name = "color[]" value = "Yellow">
				    <span class="btn btn-light"> Yellow </span>
				  </label>
			 </div>
            </div>  -->
            <div class="form-row">
                <div class="col form-group">
                    <label>Available In Size:</label>
                    <input type="text" class="form-control" id="size" placeholder="XXL Or 9-10 Years" name="size" value="<?php echo set_value('size'); ?>">
                </div> 
                <div class="col form-group">
                    <label>Available In Colour:</label>
                    <input type="text" class="form-control" id="color" placeholder="Enter Colour" name="color" value="<?php echo set_value('color'); ?>">
                </div> 
            </div> 

            <div class="form-group">
                <label for="pwd">Description:<span class="red-star">*</span></label>
                <textarea class="form-control" id="description" placeholder="Enter description" name="description" value="<?php echo set_value('description'); ?>" cols="30" rows="5" required ></textarea>
                <?php echo form_error('description'); ?>
            </div>
            <div class="form-row">
                <div class="col form-group">
                    <label>Delivery :<span class="red-star">*</span></label>
                    <input type="text" class="form-control" id="delivery" placeholder="Enter Delivery" name="delivery" value="<?php echo set_value('showpries'); ?>" required >
                </div> 
                <div class="col form-group">
                    <label>Delivery In:<span class="red-star">*</span></label>
                    <select name="deliveryin" class="form-control deliveryin" style = "height: 34px" required > 
                        <option value="">Select</option>
                        <option>Minutes</option>
                        <option>Hours</option>
                        <option>Days</option>
                        <option>Working Days</option>
                    </select> 
                </div> 
            </div> 
            <div class="form-group"> 
                <label for="varchar">Service </label>
                <?php echo form_error('service') ?> 
                <select name="service" class="form-control service" style = "height: 34px"> 
                    <option value="">Select</option>
                    <?php
                        foreach($services as $value){
                            if($value->id == $this->session->userdata('user')->bussines_type) {
                                echo "<option selected='selected' value='".$value->id."'>".$value->name."</option>";
                            }else{
                                if($value->id == $this->session->userdata('user')->bussines_type1){
                                    echo "<option  value='".$value->id."'>".$value->name."</option>";
                                }
                                if($value->id == $this->session->userdata('user')->bussines_type2){
                                    echo "<option  value='".$value->id."'>".$value->name."</option>";
                                }
                            }
                        }
                    ?>
                </select> 
            </div> 
           
            <div class="form-group">
                <label for="">Image:<span class="red-star">*</span></label> 
                <input type="file" class="form-control" id="fname" name="fname[]">
            </div> 
            <div class="form-group input_fields_wrap">

            </div>
                <button class="add_field_button">Add More</button>
                <?php echo form_error('fname'); ?>
            

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    </div>
    <div class ="col-md-6">
    <div class="row ">
        <aside class="col-md-12">
            <div class="card">
                <article class="gallery-wrap"> 
                    <div class="img-big-wrap">
                        <div class = "showimg"> <img id = "prev" src="<?php echo base_url('assets/items/demo.png')?>"></div>
                    </div> <!-- slider-product.// -->
                    
                </article> <!-- gallery-wrap .end// -->
            </div> <!-- card.// -->
        </aside>
        <main class="col-md-12">
            <article class="product-info-aside">
                <h2 class="title mt-3 itemname" ></h2>
                <div class="mb-3"> 
                <var class="price h4"></var>
                <del>   <span class="text-muted price1"></span> 
                    <span class="badge badge-danger"></span>
                </div> <!-- price-detail-wrap .// -->
                <p class ="description"></p>
                <dl class="row">
                
                    <dt class="col-sm-3">Available</dt>
                    <dd class="col-sm-9 stock"></dd>
                    <dt class="col-sm-3">Delivery time</dt>
                    <dd class="col-sm-9 din"></dd>
                    <dt class="col-sm-3 size1"></dt>
                    <dd class="col-sm-9 size"></dd>
                    <dt class="col-sm-3 color1"></dt>
                    <dd class="col-sm-9 color"></dd>
                    
                </dl>
            </article> <!-- product-info-aside .// -->
        </main> <!-- col.// -->
    </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var max_fields      = 5; //maximum input boxes allowed
        var wrapper       = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><input type="file" class="form-control" id="fname'+x+'" name="fname[]"> <a href="#" class="remove_field">   X </a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });


// $(document).ready(function(){
//   $(".service").change(function(){
//     var service = $(".service").val();
    
//     $.ajax({
//             url: "<?php //echo base_url('Product/getsubservice');?>",
//             method: "POST",
//             data: {service: service},
//             cache: true
//         }).success(function(data){
           
//             data = $.parseJSON(data);
//             var i;
//             var html='';
//             for (i = 0; i < data.length; i++) {
//             html += "<option value = '"+data[i].id+"'>"+data[i].name+"</option>"
//            }
          
//            $(".service1").html(html);
         
//         }).fail(function(data){
//             console.log(data);
//         });
//   });
// });

$("#pries").keyup(function(){
    var p = $("#pries").val();
    var m = 4/100;
    var g = 18/100;
    var tatal = p * m;
    var tatal1 = tatal *g;
    var main = tatal1 +tatal;
    var main1 = (p - main).toFixed(2);
  
  $("#share").val(main1);
});

$("#name").blur(function(){
    var name = $("#name").val();
    $(".itemname").html(name);
});
$("#pries").blur(function(){
    var name = $("#pries").val();
    var pries = '&#x20B9;' + name;
    $(".price").html(pries);
   
});
$("#showpries").blur(function(){
    var name = $("#showpries").val();
    var name1 = $("#pries").val();
    var pries = '&#x20B9;' + name;
    var d = parseInt(name) - parseInt(name1);
   
    if(parseInt(name) > parseInt(name1)){
      var dis = "&#x20B9;"+d+" -/OFF";
    
      $(".badge").html(dis);
      $(".price1").html(pries);
    }else{
      alert("Actual Price Must Be Greater Thane Your Selling Price");
      $("#showpries").val('');
      $('#pries').focus();
    }
});
$("#quantity").blur(function(){
    var name = $("#quantity").val();
    $(".stock").html(name);
});
$("#description").blur(function(){
    var name = $("#description").val();
    $(".description").html(name);
});
$("#size").blur(function(){
    var name = $("#size").val();
   
    $(".size").html(name);
    $(".size1").html('Available In Size:');
});
$("#color").blur(function(){
    var name = $("#color").val();
  
    $(".color").html(name);
    $(".color1").html("Available In Colour:");
});
$('.deliveryin').on('change', function() {
    var name = $("#delivery").val();
    var din = this.value;
    $(".din").html(name+' '+ din);
});
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#prev').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#fname").change(function() {
  readURL(this);
});

CKEDITOR.replace('description', {
      fullPage: true,
      extraPlugins: 'docprops',
      // Disable content filtering because if you use full page mode, you probably
      // want to  freely enter any HTML content in source mode without any limitations.
      allowedContent: true,
      height: 320
    });
</script>