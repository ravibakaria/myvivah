<style>
.rating-stars i {
    font-size: 27px ! important;
    padding: 5px;
}
.info-main{
    margin-left: 10%;
}
</style>
<h2>My Portfolio</h2>
<div class="row no-gutters">
		<div class="col-md-6">
        <a href="<?= base_url().'Business_partner/edit_myportfolio';?>" class="btn btn-primary">Edit Portfolio</a>
        </div>
       
</div>
<?php if($this->session->flashdata('message')){?>
  	<div class="alert alert-success">
		<strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
	</div>
	<?php } ?>

<article class="card card-product-list">
	<div class="row no-gutters">
		<aside class="col-md-3">
        <p class="img-wrap">
		<?php if($portpolio->img_name == ''){ ?>
					<img src="<?= base_url().'assets/items/demo.png'?>">
				<?php } else { ?>
					<img src="<?= base_url().'assets/images/'.$portpolio->img_name?>">
				<?php } ?>
			</p>
		</aside> 
		<div class="col-md-5">
			<div class="info-main">
				<p class="h5 title"> <?php echo ucfirst($this->session->userdata('user')->shopname)?></p>
                <div class="rating-wrap mb-2">
					<ul class="rating-stars">
						<li style="width:100%" class="stars-active"> 
                        <?php if($portpolio->insta != '') {?>
							<a href="<?php echo $portpolio->insta ?>" target="_blank"><i class="fa fa-instagram"></i> </a>
                        <?php } ?>
                        <?php if($portpolio->fb != '') { ?>
                            <a href="<?php echo $portpolio->fb ?>" target="_blank"><i class="fa fa-facebook"></i> </a>
                        <?php } ?> 
                        <?php if($portpolio->youtube != ''){?>
                            <a href="<?php echo $portpolio->youtube?>" target="_blank"><i class="fa fa-youtube"></i></a>
                        <?php } ?> 
							
						</li>
						<li>
							<i class="fa fa-instagram"></i> 
                            <i class="fa fa-facebook"></i> 
							<i class="fa fa-youtube"></i> 
							 
						</li>
					</ul>
					
				</div> <!-- rating-wrap.// -->
				<p> <?php echo $portpolio->discription ?></p>

			</div> 
		</div> 
        <div class="col-md-4" >
        <p class="h5 title"> Shop Open Day </p> <?php echo $portpolio->opendays ?>
        <p class="h5 title">  Open Time </p> 
        <?php
            if($portpolio->morning_time !=''){
                if($portpolio->morning_time == 'm4'){
                    echo "Morning :- 8:00 Am To 12:00 PM" ;
                }
                if($portpolio->morning_time == 'm3'){
                    echo "Morning :- 9:00 Am To 12:00 PM" ;
                }
                if($portpolio->morning_time == 'm2'){
                    echo "Morning :- 10:00 Am To 12:00 PM" ;
                }
                if($portpolio->morning_time == 'm1'){
                    echo "Morning :- 11:00 Am To 12:00 PM" ;
                }
                echo "<br>";
            }
            
            if($portpolio->after_time !=''){
                if($portpolio->after_time == 'a4'){
                    echo "After Noon :- 1:00 PM To 4:00 PM" ;
                }
                if($portpolio->after_time == 'a5'){
                    echo "After Noon :- 1:00 PM To 5:00 PM" ;
                }
                if($portpolio->after_time == 'a2'){
                    echo "After Noon :- 2:00 PM To 4:00 PM" ;
                }
                if($portpolio->after_time == 'a3'){
                    echo "After Noon :- 2:00 Am To 5:00 PM" ;
                }
                echo "<br>";
            }

            if($portpolio->morning_time !=''){
                if($portpolio->evning_time == 'e4'){
                    echo "Evening :- 4:00 PM To 8:00 PM" ;
                }
                if($portpolio->evning_time == 'e5'){
                    echo "Evening :- 4:00 PM To 9:00 PM" ;
                }
                if($portpolio->evning_time == 'e6'){
                    echo "Evening :- 4:00 PM To 10:00 PM" ;
                }
                if($portpolio->evning_time == 'e3'){
                    echo "Evening :- 5:00 PM To 8:00 PM" ;
                }
                if($portpolio->evning_time == 'ee4'){
                    echo "Evening :- 5:00 PM To 9:00 PM" ;
                }
                if($portpolio->evning_time == 'ee5'){
                    echo "Evening :- 5:00 PM To 10:00 PM" ;
                }
                if($portpolio->evning_time == 'e2'){
                    echo "Evening :- 6:00 PM To 8:00 PM" ;
                }
                if($portpolio->evning_time == 'ee3'){
                    echo "Evening :- 6:00 PM To 9:00 PM" ;
                }
                if($portpolio->evning_time == 'ee5'){
                    echo "Evening :- 5:00 PM To 10:00 PM" ;
                }
               
            }
                
                ?>
        <p class="h5 title">  Booking Allow </p> 
        <?php
            if($portpolio->morning_booking !=''){
                echo "Morning :- ".$portpolio->morning_booking ;
                echo "<br>";
            }
            if($portpolio->after_booking !=''){  
                echo "After Noon :- ".$portpolio->after_booking ;
                echo "<br>";
            }
            if($portpolio->evning_booking !=''){          
                echo "Evening :- ".$portpolio->evning_booking ;   
            }
                
        ?>
		</div>
	</div> 
</article>


<link rel="stylesheet" href="<?php echo base_url('assets/fullcalendar/fullcalendar.min.css')?>" />

<script src="<?php echo base_url('assets/fullcalendar/lib/moment.min.js')?>"></script>
<script src="<?php echo base_url('assets/fullcalendar/fullcalendar.min.js')?>"></script>

<script>
var $j = jQuery.noConflict();
$j(document).ready(function () {
    var calendar = $j('#calendar').fullCalendar({
        editable: true,
        events: "<?php echo base_url('Business_partner/fetch_event')?>",
        displayEventTime: false,
        eventRender: function (event, element, view) {
            if (event.allDay === 'true') {
                event.allDay = true;
            } else {
                event.allDay = false;
            }
        },
        selectable: true,
        selectHelper: true,
        select: function (start, end, allDay) {
            var title = prompt('Event Title:');

            if (title) {
                var start = $j.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                var end = $j.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");

                $.ajax({
                    url: "<?php echo base_url('Business_partner/add_event')?>",
                    data: 'title=' + title + '&start=' + start + '&end=' + end,
                    type: "POST",
                    success: function (data) {
                        displayMessage("Added Successfully");
                    }
                });
                calendar.fullCalendar('renderEvent',
                        {
                            title: title,
                            start: start,
                            end: end,
                            allDay: allDay
                        },
                true
                        );
            }
            calendar.fullCalendar('unselect');
        },
        
        editable: true,
        eventDrop: function (event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url: 'edit-event.php',
                        data: 'title=' + event.title + '&start=' + start + '&end=' + end + '&id=' + event.id,
                        type: "POST",
                        success: function (response) {
                            displayMessage("Updated Successfully");
                        }
                    });
                },
        eventClick: function (event) {
            var deleteMsg = confirm("Do you really want to delete?");
            if (deleteMsg) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('Business_partner/delete_event')?>",
                    data: "&id=" + event.id,
                    success: function (response) {
                        if(parseInt(response) > 0) {
                            $j('#calendar').fullCalendar('removeEvents', event.id);
                            displayMessage("Deleted Successfully");
                        }
                    }
                });
            }
        }

    });
});

function displayMessage(message) {
	    $(".response").html("<div class='success'>"+message+"</div>");
    setInterval(function() { $(".success").fadeOut(); }, 1000);
}
</script>

<style>


#calendar {
    width: 700px;
    margin: 0 auto;
}

.response {
    height: 60px;
}

.success {
    background: #cdf3cd;
    padding: 10px 60px;
    border: #c3e6c3 1px solid;
    display: inline-block;
}
</style>
<div class="card">
    <div class="card-head">
        <h2>
        My Booking / Scheduling
        </h2>
    </div>
    <div class="card-body">
        <div class="response"></div>
        <div id='calendar'></div>
    </div>
</div>



