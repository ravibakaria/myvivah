<style>
.rating-stars i {
    font-size: 27px ! important;
    padding: 5px;
}
.info-main{
    margin-left: 10%;
}
</style>
<h2>My Portfolio</h2>
<div class="row no-gutters">
		<div class="col-md-6">
        <a href="<?= base_url().'Business_partner/edit_portfolio';?>" class="btn btn-primary">Edit Portfolio</a>
        </div>
        <div class="col-md-6" style = "text-align: right; margin-bottom:10px;">
            <a href="<?= base_url().'Business_partner/add_appointment_services';?>" class="btn btn-primary">Add Services</a>
        </div>
</div>
<?php if($this->session->flashdata('message')){?>
  	<div class="alert alert-success">
		<strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
	</div>
	<?php } ?>

	<article class="card card-product-list">
	<div class="row no-gutters">
		<aside class="col-md-3">
        <p class="img-wrap">
		<?php if($portpolio->img_name == ''){ ?>
					<img src="<?= base_url().'assets/items/demo.png'?>">
				<?php } else { ?>
					<img src="<?= base_url().'assets/images/'.$portpolio->img_name?>">
				<?php } ?>
			</p>
		</aside> 
		<div class="col-md-5">
			<div class="info-main">
				<p class="h5 title"> <?php echo ucfirst($this->session->userdata('user')->shopname)?></p>
                <div class="rating-wrap mb-2">
					<ul class="rating-stars">
						<li style="width:100%" class="stars-active"> 
                        <?php if($portpolio->insta != '') {?>
							<a href="<?php echo $portpolio->insta ?>" target="_blank"><i class="fa fa-instagram"></i> </a>
                        <?php } ?>
                        <?php if($portpolio->fb != '') { ?>
                            <a href="<?php echo $portpolio->fb ?>" target="_blank"><i class="fa fa-facebook"></i> </a>
                        <?php } ?> 
                        <?php if($portpolio->youtube != ''){?>
                            <a href="<?php echo $portpolio->youtube?>" target="_blank"><i class="fa fa-youtube"></i></a>
                        <?php } ?> 
							
						</li>
						<li>
							<i class="fa fa-instagram"></i> 
                            <i class="fa fa-facebook"></i> 
							<i class="fa fa-youtube"></i> 
							 
						</li>
					</ul>
					
				</div> <!-- rating-wrap.// -->
				<p> <?php echo $portpolio->discription ?></p>

			</div> 
		</div> 
        <div class="col-md-4" >
        <p class="h5 title"> Shop Open Day </p> <?php echo $portpolio->opendays ?>
        <p class="h5 title">  Open Time </p> 
        <?php
            if($portpolio->morning_time !=''){
                if($portpolio->morning_time == 'm4'){
                    echo "Morning :- 8:00 Am To 12:00 PM" ;
                }
                if($portpolio->morning_time == 'm3'){
                    echo "Morning :- 9:00 Am To 12:00 PM" ;
                }
                if($portpolio->morning_time == 'm2'){
                    echo "Morning :- 10:00 Am To 12:00 PM" ;
                }
                if($portpolio->morning_time == 'm1'){
                    echo "Morning :- 11:00 Am To 12:00 PM" ;
                }
                echo "<br>";
            }
            
            if($portpolio->after_time !=''){
                if($portpolio->after_time == 'a4'){
                    echo "After Noon :- 1:00 PM To 4:00 PM" ;
                }
                if($portpolio->after_time == 'a5'){
                    echo "After Noon :- 1:00 PM To 5:00 PM" ;
                }
                if($portpolio->after_time == 'a2'){
                    echo "After Noon :- 2:00 PM To 4:00 PM" ;
                }
                if($portpolio->after_time == 'a3'){
                    echo "After Noon :- 2:00 Am To 5:00 PM" ;
                }
                echo "<br>";
            }

            if($portpolio->morning_time !=''){
                if($portpolio->evning_time == 'e4'){
                    echo "Evening :- 4:00 PM To 8:00 PM" ;
                }
                if($portpolio->evning_time == 'e5'){
                    echo "Evening :- 4:00 PM To 9:00 PM" ;
                }
                if($portpolio->evning_time == 'e6'){
                    echo "Evening :- 4:00 PM To 10:00 PM" ;
                }
                if($portpolio->evning_time == 'e3'){
                    echo "Evening :- 5:00 PM To 8:00 PM" ;
                }
                if($portpolio->evning_time == 'ee4'){
                    echo "Evening :- 5:00 PM To 9:00 PM" ;
                }
                if($portpolio->evning_time == 'ee5'){
                    echo "Evening :- 5:00 PM To 10:00 PM" ;
                }
                if($portpolio->evning_time == 'e2'){
                    echo "Evening :- 6:00 PM To 8:00 PM" ;
                }
                if($portpolio->evning_time == 'ee3'){
                    echo "Evening :- 6:00 PM To 9:00 PM" ;
                }
                if($portpolio->evning_time == 'ee5'){
                    echo "Evening :- 5:00 PM To 10:00 PM" ;
                }
               
            }
                
                ?>
        <p class="h5 title">  Booking Allow </p> 
        <?php
            if($portpolio->morning_booking !=''){
                echo "Morning :- ".$portpolio->morning_booking ;
                echo "<br>";
            }
            if($portpolio->after_booking !=''){  
                echo "After Noon :- ".$portpolio->after_booking ;
                echo "<br>";
            }
            if($portpolio->evning_booking !=''){          
                echo "Evening :- ".$portpolio->evning_booking ;   
            }
                
        ?>
		</div>
	</div> 
</article>
		<div class="row" style = "margin-top:15px;">
		<?php foreach($products as $p){?>
			<div class="col-md-6 card">
				<div class="card-body">
					<p class="img-wrap">
						<img src="<?php echo base_url('assets/images/').$p->img_name?>"  class="img img-thumbnail">
					</p>
				
					<p class="h5 title"><?php echo $p->name?></p>
					<p><?php echo $p->discription?></p>
						<var class="price h4">MRP : &#x20B9;<?php echo $p->pries ?></var>
                		<del><span class="text-muted">&#x20B9; <?php echo $p->showpries ?></span> 
                    	<span class="badge badge-danger">&#x20B9; <?php echo $p->showpries - $p->pries ?> -/OFF</span>
					
					<div class="row no-gutters">
						<div class="col-md-6">
							<a href="<?= base_url().'Business_partner/edit_appointment_services/'.$p->id;?>" class="btn btn-primary">Edit Service</a>
						</div>
						<div class="col-md-6" style = "text-align: right; margin-bottom:10px;">
							<a href="<?= base_url().'Business_partner/delete_ap_Services/'.$p->id;?>" class="btn btn-danger">Delete</a>
						</div>
					</div>
				</div>		
			</div>
		<?php } ?>
		</div>
	
