<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.multiselect.css')?>">
<style>
.error{
	color: red;
}
fieldset {
    display: block;
    margin-inline-start: 2px;
    margin-inline-end: 2px;
    padding-block-start: 0.35em;
    padding-inline-start: 0.75em;
    padding-inline-end: 0.75em;
    padding-block-end: 0.625em;
    min-inline-size: min-content;
    border-width: 2px;
    border-style: groove;
    border-color: threedface;
    border-image: initial;
}
</style>
<div class="row text-center">
    <div class="col-md-8 text-center">
        <?php if($this->session->userdata('user')->status == 0){?>
         <h3>Your profile not active. To activate <a href="<?php echo base_url('Business_partner/status')?>" class="btn btn-primary"> click here</a></h3>
      
       <?php } ?>
    </div>
    
  </div>

<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<!-- ============================ COMPONENT REGISTER   ================================= -->
	<div class="card mx-auto" style="max-width:720px; margin-top:40px;">
        <?php if($this->session->flashdata('message')){?>
        <div class="alert alert-success">
            <strong>Success!</strong> <?php echo $this->session->flashdata('message_r');?>.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata('message_r')){?>
        <div class="alert alert-warning">
            <strong>Error!</strong> <?php echo $this->session->flashdata('message_r');?>.
        </div>
        <?php } ?>
      <article class="card-body">
		<header class="mb-4"><h4 class="card-title">My Profile</h4></header>
		<form id = "registerform" method = "post" action = "<?php echo base_url('Business_partner/profile_action');?>" enctype="multipart/form-data">
				<div class="form-row">
					<div class="col form-group">
						<label>First name</label>
					  	<input type="text" class="form-control" placeholder="" name = "name" value = "<?php echo $prifile->name;?>">
					</div> <!-- form-group end.// -->
					<div class="col form-group">
						<label>Last name</label>
					  	<input type="text" class="form-control" placeholder="" name ="lname"  value = "<?php echo $prifile->lname;?>">
					</div> <!-- form-group end.// -->
				</div> <!-- form-row end.// -->
				<div class="form-group">
					<label>Email</label>
					<input type="email" class="form-control" placeholder="" name ="email"  value = "<?php echo $prifile->email;?>" readonly>
					<small class="form-text text-muted">We'll never share your email with anyone else.</small>
				</div> <!-- form-group end.// -->
                <div class="form-group">
					<label>Mobile No</label>
					<input type="number" class="form-control" placeholder="" name = "phone"  value = "<?php echo $prifile->phone;?>">
					<small class="form-text text-muted">We'll never share your Mobile No. with anyone else.</small>
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label class="custom-control custom-radio custom-control-inline">
					  <input class="custom-control-input" <?php if($prifile->gender =="Male"){echo "checked";}?> type="radio" name="gender" value="Male">
					  <span class="custom-control-label"> Male </span>
					</label>
					<label class="custom-control custom-radio custom-control-inline">
					  <input class="custom-control-input" type="radio" name="gender" value="Female" <?php if($prifile->gender =="Female"){echo "checked";}?>>
					  <span class="custom-control-label"> Female </span>
					</label>
				</div> <!-- form-group end.// -->
				<div class="form-row">
					<div class="form-group col-md-6">
					  <label>City</label>
					  <input type="text" class="form-control" name ="city" value = "<?php echo $prifile->city;?>">
					</div> <!-- form-group end.// -->
					<div class="form-group col-md-6">
					  <label>Country</label>
					  <select id="inputState" class="form-control" name="country">
					    <option> Choose...</option>
					      <option selected="">INDIA</option>
					  </select>
					</div> <!-- form-group end.// -->
				</div> <!-- form-row.// -->
				
				<div class="form-group">
					<label>Businss Type</label>
					<select name="bussines" class="form-control bussines" style = "height: 34px"> 
                    
                    <?php
                   
                        echo "<option selected ='' value='".$myservice->id."'>".$myservice->name."</option>";
                   
                    ?>
                </select> 
				</div> <!-- form-group end.// -->
				
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>Shop Name</label>
					    <input class="form-control" type="text" name = "shopname" value = "<?php echo $prifile->shopname;?>">
					</div> <!-- form-group end.// --> 
					<div class="form-group col-md-6">
						<label>Shop No.</label>
					    <input class="form-control" type="text" name = "shopno" value = "<?php echo $prifile->shopno;?>">
					</div> <!-- form-group end.// -->  
				</div>
				<div class="form-group">
					<label>Shop Address</label>
					<input type="text" class="form-control" placeholder="" name = "shopadd" value = "<?php echo $prifile->shopadd;?>">
				</div> <!-- form-group end.// -->
				
					<div class="form-group">
					<label>Pin code</label>
					<input type="text" class="form-control" placeholder="" name = "pin" minlength="6"
					maxlength="6" value = "<?php echo $prifile->pin;?>">
					</div> <!-- form-group end.// -->
					<!-- <div class="col form-group">
						<label>Delivery Area Radius In K.M</label>
					  	<input type="number" class="form-control" placeholder="" name ="aria"  value = "<?php echo $prifile->aria;?>"  minlength="2" maxlength="2">
					</div>  -->
				
				<fieldset>
  					<legend>Delivery Area:</legend>
				
					<!-- <div class="form-row">
						<div class="form-group col-md-6">
						<label>Businss Type 2</label>
						<select name="bussines2" class="form-control bussines" style = "height: 34px"> 
						<option value="">Select</option>
						<?php
						// foreach($services as $value)
						// {
						// 	if($value->id == $prifile->bussines_type2){
						// 		echo "<option selected = '' value='".$value->id."'>".$value->name."</option>";
						// 	}else{
						// 		echo "<option value='".$value->id."'>".$value->name."</option>";
						// 	}
						// }
						?>
					</select>
						</div> 
						<div class="form-group col-md-6">
						<label>Businss Type 2</label>
						<select name="bussines2" class="form-control bussines" style = "height: 34px"> 
						<option value="">Select</option>
						<?php
						// foreach($services as $value)
						// {
						// 	if($value->id == $prifile->bussines_type2){
						// 		echo "<option selected = '' value='".$value->id."'>".$value->name."</option>";
						// 	}else{
						// 		echo "<option value='".$value->id."'>".$value->name."</option>";
						// 	}
						// }
						?>
					</select>
						</div> 
					</div> -->
					<div class="form-group">
						<label class="custom-control custom-radio custom-control-inline">
						<input class="custom-control-input" <?php if($prifile->aria =="1"){echo "checked";}?> type="radio" name="delivery" value="1">
						<span class="custom-control-label"> Delivery Area Radius In K.M </span>
						</label>
						<label class="custom-control custom-radio custom-control-inline">
						<input class="custom-control-input"<?php if($prifile->aria =="2"){echo "checked";}?> type="radio" name="delivery" value="2">
						<span class="custom-control-label"> Delivery In All India </span>
						</label>
						<label class="custom-control custom-radio custom-control-inline">
						<input class="custom-control-input" <?php if($prifile->aria =="3"){echo "checked";}?> type="radio" name="delivery" value="3">
						<span class="custom-control-label"> Delivery In 3 States </span>
						</label>
						<div class = "myaria">
							<?php if($prifile->aria =='1'){
							
								$id = $this->session->userdata('user')->id;
								$this->db->distinct();
								$this->db->select('aria');
								$this->db->where('uid', $id); 
								$query = $this->db->get("delivery_aria")->row();
							
								?>
							<div class="col form-group">
								<label>Delivery Area Radius In K.M</label>
								<input type="number" class="form-control" placeholder="" name ="aria"  value = "<?php echo $query->aria;?>"  minlength="2" maxlength="2" required>
							</div>
							<?php } else if($prifile->aria =='2'){?>
								<div class="form-row">
								<div class="form-group col-md-6">
								<label>Country</label>
								<select id="inputState" class="form-control" name="country">
									<option selected="">INDIA</option>
								</select>
								</div>
								
								</div>
							<?php }else{ ?>
								<div class="form-row">
								<div class="form-group col-md-6">
								<fieldset>
    							<legend>States:</legend>
								<?php if($prifile->aria = 3){
									$id = $this->session->userdata('user')->id;
									$this->db->distinct();
									$this->db->select('state');
									$this->db->where('uid', $id); 
									$query = $this->db->get("delivery_aria")->result();
								}?>
								<?php foreach($query as $q){?>
								<label><?php echo $q->state; ?></label><br>
								<?php } ?>
								</fieldset>
								</div>
								<div class="form-group col-md-6">
								<fieldset>
								<legend>City:</legend>
								<?php
									$id = $this->session->userdata('user')->id;
									$this->db->distinct();
									$this->db->select('city');
									$this->db->where('uid', $id); 
									$query = $this->db->get("delivery_aria")->result();
								?>
    							
								<?php foreach($query as $q){?>
								<label><?php echo $q->city.","; ?></label>
								<?php } ?>
								</fieldset>
								</div>
								</div>
							<?php } ?>
						</div> 
						<div class = "myaria1">
						</div>
					</div>
				</fieldset>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>latitude</label>
					    <input class="form-control" type="text" id="latitude" name="latitude" value = "<?php echo $prifile->latitude;?>" readonly >
					</div> <!-- form-group end.// --> 
					<div class="form-group col-md-6">
						<label>longitude</label>
					    <input class="form-control" type="text" id="longitude" name="longitude" value = "<?php echo $prifile->longitude;?>" readonly >
					</div> <!-- form-group end.// -->  
				</div>
				<fieldset>
  					<legend>My Bank Details:</legend>
				

				<div class="form-group">
					<label>Bank Account Holder Name</label>
					<input type="text" class="form-control" placeholder="" name = "holdername"  value = "<?php echo $prifile->holdername;?>">
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>Bank Name</label>
					<input type="text" class="form-control" placeholder="" name = "bankname" value = "<?php echo $prifile->bankname;?>">
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>Account No.</label>
					<input type="text" class="form-control" placeholder="" name = "accountno" value = "<?php echo $prifile->accountno;?>">
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>IFSC Code</label>
					<input type="text" class="form-control" placeholder="" name = "ifsc"  value = "<?php echo $prifile->ifsc;?>">
				</div> <!-- form-group end.// -->
				<div class="form-group">
					<label>Google Pay Number Or UPI</label>
					<input type="text" class="form-control" placeholder="" name = "upi" value = "<?php echo $prifile->upi;?>">
				</div> <!-- form-group end.// -->
				</fieldset>
				<fieldset>
  					<legend>KYC:</legend>
				<?php if($prifile->img1 ==''){?>
				<div class="form-group">
					<div class="custom-file">
                        <input name="aadhar" type="file" class="custom-file-input" aria-describedby="">
                        <label class="custom-file-label" for="">Aadhar Card</label>
                    </div>
				</div> <!-- form-group end.// -->
				<?php } ?>
				<?php if($prifile->img2 ==''){?>
				<div class="form-group">
					<div class="custom-file">
                        <input name="voter" type="file" class="custom-file-input" aria-describedby="">
                        <label class="custom-file-label" for="">VoterId</label>
                    </div>
				</div> <!-- form-group end.// -->
				<?php } ?>
				<?php if($prifile->img3 ==''){?>
				<div class="form-group">
					<div class="custom-file">
                        <input name="dl" type="file" class="custom-file-input" aria-describedby="">
                        <label class="custom-file-label" for="">Driving Licence</label>
                    </div>
				</div> <!-- form-group end.// -->
				<?php } ?>
				<?php if($prifile->img4 ==''){?>
				<div class="form-group">
					<div class="custom-file">
                        <input name="pan" type="file" class="custom-file-input" aria-describedby="">
                        <label class="custom-file-label" for="">Pancard</label>
                    </div>
				</div> <!-- form-group end.// -->
				<?php } ?>
				</fieldset>
			    <div class="form-group">
			        <button type="submit" class="btn btn-primary btn-block"> Update  </button>
			    </div> <!-- form-group// -->      
			              
			</form>
		</article><!-- card-body.// -->
    </div> <!-- card .// -->
   
    <br><br>
<!-- ============================ COMPONENT REGISTER  END.// ================================= -->
<?php $this->db->distinct();
$this->db->select('state');
$query = $this->db->get('states');
$result = $query->result();
?>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->
<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function() {
    $j("#registerform").validate({
        rules: {
            name: "required",
            lname: "required",
            phone: "required",
            city: "required",
            country: "required",
            shopname: "required",
            shopno: "required",
            shopadd: "required",
            businss: "required",
			delivery: "required",
			pin: {
				required: true,
				number: true
			},
            email: {
                required: true,
                email: true
            }
        },
        messages: {
          
           
        }
    });
});
</script>

<script>
navigator.geolocation.getCurrentPosition(showPosition);
function showPosition(position){
            let lat = position.coords.latitude;
            let long = position.coords.longitude;
			console.log(lat, long);
			<?php if($prifile->longitude == 0 || ''){?>
            document.getElementById("latitude").value = lat;
			document.getElementById("longitude").value = long;  
			<?php }?>         
}

$j('input[type=radio][name=delivery]').change(function() {
	var html ='';
    if (this.value == '1') {
		$j(".myaria").css("display","none");
        html = '<div class="col form-group"><label>Delivery Area Radius In K.M</label>	<input type="number" class="form-control" placeholder="" name ="aria"   minlength="2" maxlength="2" size"2" required></div>';
		$j(".myaria1").html(html);
    }
    else if (this.value == '2') {
		$j(".myaria").css("display","none");
		html = '<div class="form-row"><div class="form-group col-md-6">								<label>Country</label><select id="" class="form-control" name="country"><option> Choose...</option><option selected="">INDIA</option></select></div></div>';
		$j(".myaria1").html(html);
        
    } else{
		
		$j(".myaria").css("display","none");
		html = '<div class="form-row"><div class="form-group col-md-6">								<label>States</label><select id="" class="form-control inputState" name="dstate"><option> Choose...</option><?php foreach($result as $r){echo "<option>$r->state</option>";}?></select></div><div class="form-group col-md-6"><label>City</label><select multiple="multiple" size=1 class="form-control service" name="dcity[]"><option> Choose...</option></select></div></div><div class="form-row"><div class="form-group col-md-6">								<label>States</label><select id="" class="form-control inputState1" name="dstate1"><option> Choose...</option><?php foreach($result as $r){echo "<option>$r->state</option>";}?></select></div><div class="form-group col-md-6"><label>City</label><select multiple="multiple" size=1 class="form-control service1" name="dcity1[]"><option> Choose...</option></select></div></div><div class="form-row"><div class="form-group col-md-6">								<label>States</label><select id="" class="form-control inputState2" name="dstate2"><option> Choose...</option><?php foreach($result as $r){echo "<option>$r->state</option>";}?></select></div><div class="form-group col-md-6"><label>City</label><select multiple="multiple" size=1 class="form-control service2" name="dcity2[]"><option> Choose...</option></select></div></div>';
		$j(".myaria1").html(html);
		
	}
});



$j(document).on('change', '.inputState', function() {
  var state = this.value;
  	$j.ajax({
		url: "<?php echo base_url('Business_partner/getcity');?>",
		method: "POST",
		data: {state: state},
		cache: true,
		success:function(data){
		
		data = $j.parseJSON(data);
		var i;
		var html='';
		html+='<option value="all">all</option>';
		for (i = 0; i < data.length; i++) {
		html += "<option>"+data[i].city+"</option>"
		}
		$j(".service").html(html);
		$j(".service").select2();
		$j('.service').on("select2:select", function (e) { 
           var data = e.params.data.text;
           if(data=='all'){
            $j(".service > option").prop("selected","selected");
            $j(".service").trigger("change");
           }
      });
		
	}
		

	}).fail(function(data){
		console.log(data);
	});

});

$j(document).on('change', '.inputState1', function() {
  var state = this.value;
  	$j.ajax({
		url: "<?php echo base_url('Business_partner/getcity');?>",
		method: "POST",
		data: {state: state},
		cache: true,
		success:function(data){
		
		data = $j.parseJSON(data);
		var i;
		var html='';
		html+='<option value="all">all</option>';
		for (i = 0; i < data.length; i++) {
		html += "<option>"+data[i].city+"</option>"
		}
		$j(".service1").html(html);
		$j(".service1").select2();
		$j('.service1').on("select2:select", function (e) { 
           var data = e.params.data.text;
           if(data=='all'){
            $j(".service1 > option").prop("selected","selected");
            $j(".service1").trigger("change");
           }
      });
		
	}
		

	}).fail(function(data){
		console.log(data);
	});

});


$j(document).on('change', '.inputState2', function() {
  var state = this.value;
  	$j.ajax({
		url: "<?php echo base_url('Business_partner/getcity');?>",
		method: "POST",
		data: {state: state},
		cache: true,
		success:function(data){
		
		data = $j.parseJSON(data);
		var i;
		var html='';
		html+='<option value="all">all</option>';
		for (i = 0; i < data.length; i++) {
		html += "<option>"+data[i].city+"</option>";

		}
		$j(".service2").html(html);
		$j(".service2").select2();
		$j('.service2').on("select2:select", function (e) { 
           var data = e.params.data.text;
           if(data=='all'){
            $j(".service2 > option").prop("selected","selected");
            $j(".service2").trigger("change");
           }
      });
		
	}
		

	}).fail(function(data){
		console.log(data);
	});

});



</script>